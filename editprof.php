<html>
<head>
<title>Edit Profile</title>
<link rel="stylesheet" type="text/css" href="ucsdcki.css">
</head>
<body>
<?php
require('utils.php');
$your_ip_address=GetHostByName($_SERVER['REMOTE_ADDR']);
amanda_session_garbage_collect();
$session_existence=amanda_session_exists($your_ip_address);
if ($session_existence){
	$username=amanda_session_get_username($your_ip_address);
	$password=amanda_session_get_password($your_ip_address);
	$loginity=validateLogin($username,$password);
}
if (!$loginity||$_POST['submit']!='Change Profile'){
	die('<span class="error">You should not be here.</span>');
}
/*if (htmlentities(strtolower(trim($username)))!=htmlentities(strtolower(trim($_GET['humanoid']))))
	die('<span class="error">Error: Information does not match.</span>');*/
$editables=explode(' ','password email phone sex dob aim icq msn yahoo website interests music tv movies books pic');
if (htmlentities(strtolower($_POST['password']))!=htmlentities(strtolower($_POST['confpassword'])))
	die('<span class="error">Error: New password and confirmation of new password do not match.  <a href="javascript:history.back(1)">Please go back and fix this</a></span>');
if (!(trim($_POST['password'])==='')&&htmlentities($_POST['opassword'])!=decrypt($password))
	die('<span class="error">Error: Old password supplied is incorrect.  <a href="javascript:history.back(1)">Please go back and fix this</a></span>');
$temp=explode('@',$_POST['email']);
//print_r($temp); echo '<br>';
if (sizeOf($temp)!=2) 
	die('<span class="error">Error: Invalid email address.  <a href="javascript:history.back(1)">Please go back and fix this</a></span>');
$temp='';
for ($x=0; $x<10; $x++){
	$temp.=$_POST['phone'.$x];
	if ($x==2||$x==5) $temp.='-';
}
$_POST['phone']=$temp;
if (strlen($_POST['phone'])!=12) 
	die('<span class="error">Error: Invalid phone number.  <a href="javascript:history.back(1)">Please go back and fix this</a></span>');
for ($x=10; $x<=14; $x++){
	$_POST[$editables[$x]]=htmlentities($_POST[$editables[$x]]);
	$temp='';
	//echo $_POST[$editables[$x]].'<br>';
	$temp2=explode(chr(13),$_POST[$editables[$x]]);
	for ($y=0; $y<sizeOf($temp2); $y++){
		$temp.=$temp2[$y];
		if ($y<sizeOf($temp2)-1) $temp.='<br>';
	}
	$_POST[$editables[$x]]=$temp;
	$_POST[$editables[$x]]=trim($_POST[$editables[$x]]);
}
$months=explode(' ','Placeholder January February March April May June July August September October November December');
$_POST['dob']=mktime(0,0,0,$_POST['dobm'],$_POST['dobd'],$_POST['doby']);

//print_r($_POST); echo '<br>';

$changedpw=true;

mysql_connect('localhost','circlek1_cki','cki') or die('<span class="error">Unable to connect to MySQL Server</span>');
mysql_select_db('circlek1_members') or die ('<span class="error">Unable to find database</span>');
$query='UPDATE `profiles` SET ';
for ($x=0; $x<sizeOf($editables); $x++){
	if ($editables[$x]=='password'){
		if (!$_POST['password']){
			$changedpw=false;
			continue;
		}
		else{
			$_POST['password']=encrypt($_POST['password']);
		}
	}

	$query.='`'.$editables[$x].'`=\''.$_POST[$editables[$x]].'\'';
	if ($x<sizeOf($editables)-1) $query.=',';
}
$query.=' WHERE `username`=\''.$username.'\'';
//echo $query.'<br>';
$data=mysql_query($query);
if (mysql_affected_rows()>0) $msg='Profile information changed.';
else $msg='No change in profile information.';
if ($changedpw){
	mysql_select_db('circlek1_misc') or die ('<span class="error">Unable to find database</span>');
	$query='UPDATE `sessions` SET `password`=\''.$_POST['password'].'\' WHERE `username`=\''.$username.'\' AND `ip`=\''.$your_ip_address.'\'';
	mysql_query($query);
	if (mysql_affected_rows()<1) die('<span class="error">Error: Your session data could not be changed.  You\'ll have to contact someone about this, as there\'s nothing that can be done from here.  Many, many apologies for this inconvenience.</span>');
}
echo '<meta http-equiv="refresh" content="0;editprofile.php?humanoid='.$username.'&msg='.$msg.'">';
mysql_close();
?>
</body>
</html>