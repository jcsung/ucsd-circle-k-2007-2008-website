<html>
<head>
<title>UCSD Circle K Member Search</title>
<link rel="stylesheet" type="text/css" href="ucsdcki.css">
</head>
<body>
<?php
require('utils.php');
$your_ip_address=GetHostByName($_SERVER['REMOTE_ADDR']);
amanda_session_garbage_collect();
$session_existence=amanda_session_exists($your_ip_address);
if ($session_existence){
	$username=amanda_session_get_username($your_ip_address);
	$password=amanda_session_get_password($your_ip_address);
	$loginity=validateLogin($username,$password);
	$secLevel=getInfo($username,$password,'secLevel');
	if ($loginity){
		$append='humanoid='.$username;
	}
	else
		$append='';
}
else
	$secLevel=150;
echo '<p class="title"><img src="./images/memberaccountsearch.png" alt="Member Account Search" border="0"><!--Member Account Search//--></p>';
if ($_POST['submit']!='Search'){
	echo "<p>Here you can search for AMANDA accounts.  This search will examine all public fields of an account's profile.  Results will be sorted alphabetically by name.  If you put nothing in the search box, you will get a list of every member account.</p>";
	echo '<form action="members.php?'.$append.'" method="post">';
	echo '<p align="center"><table class="matt" align="center"><tr><td colspan="2"><input type="text" name="query" size="55"></td></tr>';
	echo '<tr><td><input type="checkbox" name="trim" checked="checked"></td><td>Ignore extra spaces at the beginning and ending of query</td></tr>';
	echo '<tr><td><input type="checkbox" name="case"></td><td>Match Case</td></tr>';
	echo '<tr><td colspan="2" align="center"><input type="submit" name="submit" value="Search"></td></tr>';
	echo '</table></p>';
	echo '</form>';
}
else{
	$match=$_POST['case']=='on'?true:false;
	$trim=$_POST['trim']=='on'?true:false;
	$needle=stripslashes($_POST['query']);
	$multiple=false;
	if(substr($needle,0,1)=='"') $multiple=true;
	if (!$match) $needle=strtolower($needle);
	if ($trim) $needle=trim($needle);
//	echo $needle.'<br>';
	if ($multiple){
		//echo 'in here<br>';
		$needle=substr($needle,1,strlen($needle));
		if (substr($needle,strlen($needle)-1,1)=='"') $needle=substr($needle,0,strlen($needle)-1);
	}
	$needle=htmlentities($needle);
//	echo $needle.'<br>';
	if (!$multiple){
		$needles=explode(' ',$needle);
	}
	else{
		$needles=array();
		array_push($needles,$needle);
	}
	mysql_connect('localhost','circlek1_cki','cki') or die('<span class="error">Error: Unable to connect to server</span>');
	mysql_select_db('circlek1_members') or die('<span class="error">Error: Unable to connect to database</span>');
	$query='SELECT * FROM `profiles` ORDER BY `fname`,`lname`';
	$data=mysql_query($query);
	$results=array();
	$where=array();
	$links=array();
	$i=0;
	$searchables=explode(' ','0 2 3 4 5 6 9 10 11 12 13 14 15 16 17 18 19 20 21 22');
	$where_pos=explode(',','Username,First Name,Middle Name,Last Name,Email,Phone Number,College,Year,Sex,Birthday,AIM,ICQ,MSN,Yahoo!,Website,Interests,Favorite Music,TV Shows,Favorite Movies,Favorite Books');
	while ($info=mysql_fetch_row($data)){
		$temp_query='SELECT * FROM `profiles` WHERE `username`=\''.$info[0].'\'';
		$temp_data=mysql_query($temp_query);
		$temp_info=mysql_fetch_row($temp_data);
		if ($secLevel<651){
			if ($temp_info[1]=='Yes') $info[5]=null;
			if ($temp_info[2]=='Yes') $info[6]=null;
			if ($temp_info[3]=='Yes') $info[11]=null;
			if ($temp_info[4]=='Yes') $info[12]=null;
		}
		$where[$i]='';
		$flag=false;
		for ($x=0; $x<sizeOf($searchables); $x++){
			$haystack=$info[$searchables[$x]];
			if (!$match) $haystack=strtolower($haystack);
			$haystack=htmlentities($haystack);
			if($trim) $haystack=trim($haystack);
			if ($needle=='') $whattocheck=true;
			else{
				$whattocheck=false;
				for ($y=0; $y<sizeOf($needles); $y++){
					$whattocheck=strstr($haystack,$needles[$y]); 
					if ($whattocheck) break;
				}
			}
			if ($whattocheck){
				$results[$i]=$info[2].' '.$info[3].' '.$info[4];
				$links[$i]=$info[0];
				$where[$i].=$x.',';//$where_pos[$x].',';
				$flag=true;
			}
		}
		if ($flag)
			$i++;
	}
	if ($i==0){
		echo '<p>Sorry, your search yielded no results.</p>';
	}
	else{
		//print_r($where); echo '<br>';
		echo '<p align="center"><a href="members.php?'.$append.'">Search Again</a></p>';
		echo "<p>Your search yielded the following $i results (click to view profile):</p>".'<p><table class="matt">';
		$x=0;
		for ($x=0; $x<$i; $x++){
			$temp=explode(',',$where[$x]);
			$which='';
			for ($y=0; $y<sizeOf($temp)-1; $y++){
				$which.=$where_pos[$temp[$y]];
				if ($y<sizeOf($temp)-2) $which.=', ';
			}
			if ($which=='Username, First Name, Middle Name, Last Name, Email, Phone Number, College, Year, Sex, Birthday, AIM, ICQ, MSN, Yahoo!, Website, Interests, Favorite Music, TV Shows, Favorite Movies, Favorite Books') $which='All Fields';
			echo '<tr><td><table class="matt"><tr><td><a href="viewprofile.php?who='.$links[$x].'&'.$append.'">Name: </a></td><td><a href="viewprofile.php?who='.$links[$x].'&'.$append.'">'.$results[$x].'</a></td></tr><tr><td valign="top"><a href="viewprofile.php?who='.$links[$x].'&'.$append.'">Matches:</a></td><td><a href="viewprofile.php?who='.$links[$x].'&'.$append.'">'.$which.'</a></td></tr></table></td></tr>';
		}
		echo '</table></p>';
	}
	echo '<p align="center"><a href="members.php?'.$append.'">Search Again</a></p>';
	mysql_close();
}

?>
</body>
</html>