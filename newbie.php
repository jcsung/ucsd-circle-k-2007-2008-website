<?php
require('utils.php');
?>
<html>
<head>
<title>The New Person's Guide to UCSD Circle K</title>
<link rel="stylesheet" type="text/css" href="ucsdcki.css">
</head>
<body>
<?php
$your_ip_address=GetHostByName($_SERVER['REMOTE_ADDR']);
amanda_session_garbage_collect();
$session_existence=amanda_session_exists($your_ip_address);
if ($session_existence){
	//echo 'session exists<br>';	
	$username=amanda_session_get_username($your_ip_address);
	$password=amanda_session_get_password($your_ip_address);
	$loginity=validateLogin($username,$password);
}
?>
<p class="title">New to Circle K?</p>
<p>This page is still under construction at this time.  However, here are some useful resources:<br>
<a href="./files/other/Membership_Application.pdf">Membership Application</a><br>
<a href="cki_in_brief.php" target="_blank">UCSD Circle K In Brief</a><br>
<span class="bold">Unable to attend meetings?  Attend Circle K Office Hours!  </span><br>
Tuesday: 7-8pm @ Price Center Food Court w/ President Alan Nam<br>
Wednesday: 7-8pm @ Sierra Summit Dining Hall (Muir College) w/ VP Angela Yen<br>
</body>
</html>