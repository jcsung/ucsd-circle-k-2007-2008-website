<html>
<head>
<title>MRP Status Recognition Script</title>
<link rel="stylesheet" type="text/css" href="ucsdcki.css">
</head>
<body>
<?php
$loc='./mrp/';
require('utils.php');
if ($handle = opendir($loc)) {
	echo '<p><table class="matt" width="100%">';
	echo '<tr><td rowspan="2" align="center" valign="top"><img src="./images/congratulations.gif"></td>';
	echo '<td colspan="4" width="100%" class="title left" align="left">Congratulations to the following members who have obtained MRP status: </td></tr><tr>';	
	$bronze=array();
	$silver=array();
	$gold=array();
	$platinum=array();

	while (($file = readdir($handle))!==false) {
		if (!strstr($file,'.csv')) continue;
		$data=explode('"',file_get_contents($loc.$file));
		for ($x=0; $x<sizeOf($data); $x++){
			if ($x==0||$x==sizeOf($data)-1){
				continue;
			}
			$tempdump=explode(',',$data[$x]);
			$temp='';
			for ($y=0; $y<sizeOf($tempdump)-1; $y++){
				$temp.=$tempdump[$y].'&#44;';
			}
			$temp.=$tempdump[$y];
			$data[$x]=$temp;
		}
		$temp='';
		for ($x=0; $x<sizeOf($data); $x++){
			$temp.=$data[$x];
		}
//		echo $temp;
		$data=explode(chr(13),$temp);
		$line=explode(',',$data[0]);
//		print_r($line);
		$your_status=strtolower($line[15]);
		//echo $file.' '.$your_status.'<br>';
		$file=explode('.csv',$file);
		$file=$file[0];
		$name_split=explode('_',$file);
		$name=$name_split[1].' '.$name_split[0];
		if (strpos($your_status,'bronze')!==false){
			array_push($bronze,$name);
		}
		else if (strpos($your_status,'silver')!==false){
			array_push($silver,$name);
		}
		else if (strpos($your_status,'gold')!==false){
			array_push($gold,$name);
		}
		else if (strpos($your_status,'platinum')!==false){
			array_push($platinum,$name);
		}
	}

	closedir($handle);
	echo '<td align="center" valign="top">';
	if (sizeOf($bronze)!=0){
		echo '<p><img src="./images/bronze.gif" border="0"><br>';
		for ($x=0; $x<sizeOf($bronze); $x++){
			echo $bronze[$x].'<br>';
		}
	}
	echo '</td><td align="center" valign="top">';
	if (sizeOf($silver)!=0){
		echo '<p><img src="./images/silver.gif" border="0"><br>';
		for ($x=0; $x<sizeOf($silver	); $x++){
			echo $silver[$x].'<br>';
		}
	}
	echo '</td><td align="center" valign="top">';
	if (sizeOf($gold)!=0){
		echo '<p><img src="./images/gold.gif" border="0"><br>';
		for ($x=0; $x<sizeOf($gold); $x++){
			echo $gold[$x].'<br>';
		}
	}
	echo '</td><td align="center" valign="top">';
	if (sizeOf($platinum)!=0){
		echo '<p><img src="./images/platinum.png" border="0"><br>';
		for ($x=0; $x<sizeOf($platinum); $x++){
			echo $platinum[$x].'<br>';
		}
	}
	echo '</td></tr></table></p>';
}
else{
	echo '<span class="error">Error: Could not read from directory</span>';
}
?>
</body>
</html>
