<?php
require('utils.php');
$VERSION="1.7d Service Pack 9";

$show=trim($_GET['show']);
if(!$show) $show='home.php';

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=utf-8">
<title>UCSD Circle K</title>
<META name="description" content="UCSD Circle K Website">
<META name="keywords" content="0, 1, UC University of California San Diego Circle K CNH California Nevada Hawaii Cal Nev Ha Kiwanis amanda awesome">

<link rel="shortcut icon" href="http://www.ucsdcirclek.org/ucsdckifavicon.ico" type="image/x-icon">
<link rel="icon" href="http://www.ucsdcirclek.org/ucsdckifavicon.ico" type="image/x-icon">

<link rel="stylesheet" type="text/css" href="ucsdcki.css">
</head>
<body background="./images/blue.jpg">
<table height="100%" border="0" frame$="void" cellpadding="0" cellspacing="0" width="100%" class="matt">
<tr>
<td width="21%" height="98%"><table border="0" width="100%" height="100%"><tr><td align="center" valign="middle" width="100%" height="37%"><iframe frameborder="0" src="login.php" name="login" height="100%" width="100%"></iframe></td></tr><tr><td align="center" valign="middle" width="100%" height="63%"><iframe frameborder="0" src="chatterbox.php" name="chatterbox" height="100%" width="100%"></iframe></td></tr></table></td>
<td width="59%" height="98%"><table border="0" width="100%" height="100%"><tr><td align="center" valign="middle" width="100%" height="200"><iframe frameborder="0" src="navbar.php" name="navbar" height="100%" width="100%"></iframe></td></tr><tr><td align="center" valign="middle" width="100%"><iframe frameborder="0" src="<?php echo $show; ?>" name="home" height="100%" width="100%"></iframe></td></tr></table></td>
<td width="20%" height="98%"><table border="0" width="100%" height="100%"><tr><td align="center" valign="middle" width="100%" height="37%"><iframe frameborder="0" src="nextmeeting.php" name="nextmeeting" height="100%" width="100%"></iframe></td></tr><tr><td align="center" valign="middle" width="100%" height="63%"><iframe frameborder="0" src="upcoming.php" name="upcoming" height="100%" width="100%"></iframe></td></tr></table></td>
</tr>
<tr><td width="100%" colspan="3" height="2%"><table class="matt" width="100%"><tr><td width="25%" align="left"><a href="aboutamanda.php" target="home">About</a> | <a href="devteam.php" target="home">Dev Team</a> | <a href="changelog.php" target="home">Changelog</a></td><td width="50%" align="center">Powered by AMANDA Mark II Build <?php echo $VERSION; ?></td><td width="25%" align="right">&copy; 2006<?php echo (getYear(currentTime())!=2006)?'-'.getYear(currentTime()):''; ?> UCSD Circle K</td></tr></table></td></tr>
</table>
</body>
</html>