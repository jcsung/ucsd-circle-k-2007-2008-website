<html>
<head>
<title>Files</title>
<link rel="stylesheet" type="text/css" href="../ucsdcki.css">
</head>
<body>
<p class="title">Files</p>
<p>Here are UCSD Circle K's electronically-available resources.  You can get meeting agendas, meeting 
minutes, newsletters, member applications, and in general, basically any printable files we have.</p>
<p><table class="matt" width="100%">
<tr><td class="subtitle" align="center" width="50%">Latest Meeting Agenda</td><td class="subtitle" align="center" width="50%">Latest Meeting Minutes</td></tr>
<tr>
<?php
$x=0;
$meeting_day=array();
if ($dh=opendir('./agendas')){
	while(($thefilename=readdir($dh))!=false){
		if ($thefilename!='.'&&$thefilename!='..'&&$thefilename!='index.php'){
		$agendas[$x]=$thefilename;
		$meeting_day[$x]=mktime(0,0,0,substr($thefilename,0,2),substr($thefilename,2,2),'20'.substr($thefilename,4,2));
		$agenda_date[$x]=date('M j, Y',$meeting_day[$x]);
		$x++;
		}
	}
	closedir($dh);
	for ($x=0; $x<sizeOf($agendas); $x++){
		for ($y=$x+1; $y<sizeOf($agendas); $y++){
			if ($meeting_day[$x]<$meeting_day[$y]){
				$temp=$agendas[$x];
				$agendas[$x]=$agendas[$y];
				$agendas[$y]=$temp;
				$temp=$agenda_date[$x];
				$agenda_date[$x]=$agenda_date[$y];
				$agenda_date[$y]=$temp;
				$temp=$meeting_day[$x];
				$meeting_day[$x]=$meeting_day[$y];
				$meeting_day[$y]=$temp;
			}
		}
	}
	echo '<td align="center" width="50%">';
	if (stristr($agendas[0],'.pdf')) $icon='pdf';
	else if (stristr($agendas[0],'.doc')) $icon='word';
	else if (stristr($agendas[0],'.rtf')) $icon='word';
	else if (stristr($agendas[0],'.txt')) $icon='txt';
	else $icon='unknown';
	echo '<table class="matt"><tr><td><a href="./agendas/'.$agendas[0].'" target="_blank"><img src="'.$icon.'icon.png" border="0"></a></td><td><a href="./agendas/'.$agendas[0].'" target="_blank">'.$agenda_date[0].'</a></td></tr></table>';
	echo '</td>';
}
else{
	echo '<td>No agendas were found.</td>';
}
$x=0;
$meeting_day=array();
if ($dh=opendir('./minutes')){
	while(($thefilename=readdir($dh))!=false){
		if ($thefilename!='.'&&$thefilename!='..'&&$thefilename!='index.php'){
		$minutes[$x]=$thefilename;
		$meeting_day[$x]=mktime(0,0,0,substr($thefilename,0,2),substr($thefilename,2,2),'20'.substr($thefilename,4,2));
		$minute_date[$x]=date('M j, Y',$meeting_day[$x]);
		$x++;
		}
	}
	closedir($dh);
	for ($x=0; $x<sizeOf($minutes); $x++){
		for ($y=$x+1; $y<sizeOf($minutes); $y++){
			if ($meeting_day[$x]<$meeting_day[$y]){
				$temp=$minutes[$x];
				$minutes[$x]=$minutes[$y];
				$minutes[$y]=$temp;
				$temp=$minute_date[$x];
				$minute_date[$x]=$minute_date[$y];
				$minute_date[$y]=$temp;
				$temp=$meeting_day[$x];
				$meeting_day[$x]=$meeting_day[$y];
				$meeting_day[$y]=$temp;
			}
		}
	}
	echo '<td align="center" width="50%">';
	if (stristr($minutes[0],'.pdf')) $icon='pdf';
	else if (stristr($minutes[0],'.doc')) $icon='word';
	else if (stristr($minutes[0],'.rtf')) $icon='word';
	else if (stristr($minutes[0],'.txt')) $icon='txt';
	else $icon='unknown';
	echo '<table class="matt"><tr><td><a href="./minutes/'.$minutes[0].'" target="_blank"><img src="'.$icon.'icon.png" border="0"></a></td><td><a href="./minutes/'.$minutes[0].'" target="_blank">'.$minute_date[0].'</a></td></tr></table>';
	echo '</td>';
}
else{
	echo '<td>No minutes were found.</td>';
}
?>
</tr>
<tr><td width="50%" align="center"><a href="./agendas">Click here for older agendas</a></td><td width="50%" align="center"><a href="./minutes">Click here for older minutes</a></td></tr>
</table></p>
<p><table class="matt" width="100%">
<tr><td class="subtitle" align="center" width="50%">Latest Newsletter</td><td class="subtitle" align="center" width="50%">Other Files</td></tr>
<tr>
<?php
$x=0;
if ($dh=opendir('./newsletters')){
	while(($thefilename=readdir($dh))!=false){
		if ($thefilename!='.'&&$thefilename!='..'&&$thefilename!='index.php'){
		$newsletters[$x]=$thefilename;
		$temp='';
		for($y=0; $y<strlen($thefilename); $y++){
			if (substr($thefilename,$y,1)=='.') break;
			$temp.=substr($thefilename,$y,1);
		}
		$temp=explode('_',$temp);
		$possible_months=explode(' ','January February March April May June July August September October November December');
		for ($y=0; $y<12; $y++){
			if ($temp[0]==$possible_months[$y]){
				$temp[0]=$y+1;
				break;
			}
		}
		$newsletter_date[$x]=mktime(0,0,0,$temp[0],1,$temp[1]);//date('M Y',);
		$x++;
		}
	}
	closedir($dh);
	for ($x=0; $x<sizeOf($newsletters); $x++){
		for ($y=$x+1; $y<sizeOf($newsletters); $y++){
			if ($newsletter_date[$x]<$newsletter_date[$y]){
				$temp=$newsletters[$x];
				$newsletters[$x]=$newsletters[$y];
				$newsletters[$y]=$temp;
				$temp=$newsletter_date[$x];
				$newsletter_date[$x]=$newsletter_date[$y];
				$newsletter_date[$y]=$temp;
			}
		}
	}
	echo '<td valign="top" align="center" width="50%">';
	if (stristr($newsletters[0],'.pdf')) $icon='pdf';
	else if (stristr($newsletters[0],'.doc')) $icon='word';
	else if (stristr($newsletters[0],'.rtf')) $icon='word';
	else if (stristr($newsletters[0],'.txt')) $icon='txt';
	else $icon='unknown';
	echo '<table class="matt"><tr><td><a href="./newsletters/'.$newsletters[0].'" target="_blank"><img src="'.$icon.'icon.png" border="0"></a></td><td><a target="_blank" href="./newsletters/'.$newsletters[0].'">'.date('M Y',$newsletter_date[0]).'</a></td></tr><tr><td align="center" colspan="2"><a href="./newsletters">Click here for older newsletters</a></td></table>';
	echo '</td>';
}
else{
	echo '<td valign="top">No newsletters were found.</td>';
}
?>
<td align="center" width="50%"><table class="matt"><tr><td><a href="./other/Membership_Application.pdf" target="_blank"><img src="pdficon.png" border="0"></a></td><td><a href="./other/Membership_Application.pdf" target="_blank">Membership<br>Application</a></td></tr><tr><td><a href="./other/Sib_Application.pdf" target="_blank"><img src="pdficon.png" border="0"></a></td><td><a target="_blank" href="./other/Sib_Application.pdf">Big/Lil Sib<br>Application</a></td></tr><!--<tr><td><a href="./other/Service_Committee_Application.pdf" target="_blank"><img src="pdficon.png" border="0"></a></td><td><a target="_blank" href="./other/Service_Committee_Application.pdf">Service<br>Committee<br>Application</a></td></tr>//--></table></td>
</tr>
</table></p>
<p class="mini">Note: The files hosted on this site are in either PDF or DOC format.  To read PDF files, 
you will need to download a PDF reader like 
<a href="http://www.adobe.com/products/acrobat/readstep2.html" target="_blank">Adobe Reader</a>.  For DOC 
files, you will need a productivity suite like <a href="http://office.microsoft.com/en-us/default.aspx" target="_blank">Microsoft Office</a>. 
Or, consider a free alternative, <a href="http://www.openoffice.org" target="_blank">OpenOffice.org</a>, which is fully capable of reading and writing DOC files.

</body>
</html>