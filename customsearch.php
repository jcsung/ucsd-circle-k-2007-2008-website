<html>
<head>
<title>Custom Event Search</title>
<link rel="stylesheet" type="text/css" href="ucsdcki.css">
</head>
<body>
<?php
require('utils.php');
$your_ip_address=GetHostByName($_SERVER['REMOTE_ADDR']);
amanda_session_garbage_collect();
$session_existence=amanda_session_exists($your_ip_address);
$secLevel=150;
if ($session_existence){
	$username=amanda_session_get_username($your_ip_address);
	$password=amanda_session_get_password($your_ip_address);
	$loginity=validateLogin($username,$password);
	if ($loginity){
		$secLevel=getInfo($username,$password,'secLevel');
	}

}
$types=explode(',',' ,Service,Social,Fundraiser,Kiwanis Family,MD&E,Interclub,District,Divisional,International,Administrative');
$months=explode(' ','January February March April May June July August September October November December');
$year=getYear(currentTime());
if ($_GET['submit']!='Show'){
?>
	<p class="title"><img src="./images/customeventsearch.png" border="0" alt="Custom Event Search"><!--Custom Event Search//--></p>
	<p>Fill in the criteria for your search below.  
	Anything left blank will be ignored in the search.  
	The search is NOT case-sensitive.</p>
	<form action="customsearch.php" method="GET">
	<table border="0" class="matt" align="center">
	<tr><td valign="top">&nbsp;</td><td valign="top">Event Name: </td><td valign="top"><input type="text" name="name"></td></tr><tr><td valign="top" colspan="3">&nbsp;</td></tr>
	<tr><td valign="top"><select name="bool1"><option>AND</option><option>OR</option></select></td><td valign="top">Type: </td><td valign="top"><select name="type"><?php for ($x=0; $x<sizeOf($types); $x++){ echo '<option>'.$types[$x].'</option>'; } ?></select></td></tr><tr><td valign="top" colspan="3">&nbsp;</td></tr>
	<tr><td valign="top"><select name="bool2"><option>AND</option><option>OR</option></select></td><td valign="top">Date: </td><td valign="top"><table class="matt"><tr><td valign="top" colspan="3">Between</td></tr><tr><td valign="top"><select name="start_month"><option> </option><?php for ($x=0; $x<sizeOf($months); $x++){ echo '<option value="'.($x+1).'">'.$months[$x].'</option>'; } ?></select></td><td valign="top"><select name="start_day"><option> </option><?php for ($x=1; $x<=31; $x++){ echo '<option>'.$x.'</option>'; } ?></select>,</td><td valign="top"><select name="start_year"><option> </option><?php for ($x=$year-10; $x<=$year+10; $x++){ echo '<option>'.$x.'</option>'; } ?></select></td></tr><tr valign="top"><td colspan="3">AND</td></tr><tr><td valign="top" colspan="3"><input type="checkbox" name="topresentstart"> Check me to set the start date to the present</td></tr><tr><td valign="top" colspan="3"><span class="mini">(Leave below alone if you're not looking for a range of days)</span></td></tr><tr><td valign="top"><select name="end_month"><option> </option><?php for ($x=0; $x<sizeOf($months); $x++){ echo '<option value="'.($x+1).'">'.$months[$x].'</option>'; } ?></select></td><td valign="top"><select name="end_day"><option> </option><?php for ($x=1; $x<=31; $x++){ echo '<option>'.$x.'</option>'; } ?></select>,</td><td valign="top"><select name="end_year"><option> </option><?php for ($x=$year-10; $x<=$year+10; $x++){ echo '<option>'.$x.'</option>'; } ?></select></td></tr></table></td></tr><tr><td colspan="2">&nbsp;</td><td valign="top"><input type="checkbox" name="topresentend"> Check me to set the end date to the present</td></tr><tr><td valign="top" colspan="3">&nbsp;</td></tr>
	<tr><td valign="top"><select name="bool3"><option>AND</option><option>OR</option></select></td><td valign="top">Chair: </td><td valign="top"><input type="text" name="chair"></td></tr><tr><td valign="top" colspan="3">&nbsp;</td></tr>
	<tr><td valign="top"><select name="bool4"><option>AND</option><option>OR</option></select></td><td valign="top">Event ID#: </td><td valign="top"><input type="text" name="serial"></td></tr><tr><td valign="top" colspan="3">&nbsp;</td></tr>
	<tr><td valign="top"><select name="bool5"><option>AND</option><option>OR</option></select></td><td valign="top">Keyword: </td><td valign="top"><input type="text" name="keyword"></td></tr><tr><td valign="top" colspan="3" class="mini" align="right">&nbsp;</td></tr>
	<tr><td valign="top" colspan="3" align="center"><input type="submit" name="submit" value="Show"></td></tr>
	</table>
	</form>
<?php
}
else{
	$name=htmlentities(trim(strtolower($_GET['name'])));
	$type=trim(strtolower($_GET['type']));
	$start_date=0;
	$present=currentTime();
	if ($_GET['topresentstart']=='on'){
		$_GET['start_month']=getMonth($present,'num');
		$_GET['start_day']=getDay($present);
		$_GET['start_year']=getYear($present);
	}
	$_GET['start_month']=trim($_GET['start_month']);
	$_GET['start_day']=trim($_GET['start_day']);
	$_GET['start_year']=trim($_GET['start_year']);
	if ($_GET['start_month']&&$_GET['start_day']&&$_GET['start_year']) $start_date=mktime(0,0,0,$_GET['start_month'],$_GET['start_day'],$_GET['start_year']);
	if ($_GET['topresentend']=='on'){
		$_GET['end_month']=getMonth($present,'num');
		$_GET['end_day']=getDay($present);
		$_GET['end_year']=getYear($present);
	}
	$_GET['end_month']=trim($_GET['end_month']);
	$_GET['end_day']=trim($_GET['end_day']);
	$_GET['end_year']=trim($_GET['end_year']);
	$end_date=0;
	if ($_GET['end_month']&&$_GET['end_day']&&$_GET['end_year']) $end_date=mktime(0,0,0,$_GET['end_month'],$_GET['end_day'],$_GET['end_year']);
	$chair=htmlentities(trim(strtolower($_GET['chair'])));
	$id=htmlentities(trim(strtolower($_GET['serial'])));
	$keyword=htmlentities(trim(strtolower($_GET['keyword'])));

	$bool1=$_GET['bool1'];
	$bool2=$_GET['bool2'];
	$bool3=$_GET['bool3'];
	$bool4=$_GET['bool4'];
	$bool5=$_GET['bool5'];

	$test1=false;
	$test2=false;
	$test3=false;
	$test4=false;
	$date_range=false;
	$test5=false;
	$test6=false;

	if ($name) $test1=true;
	if ($type) $test2=true;
	if ($start_date!='0') $test3=true;
	if ($chair) $test4=true;
	if ($test3&&$end_date!='0') $date_range=true;
	if ($id) $test5=true;
	if ($keyword) $test6=true;

	//echo '<p>$test1='.$test1.' | $test2='.$test2.' | $test3='.$test3.' | $test4='.$test4.' | $date_range='.$date_range.'</p>';

	mysql_connect('localhost','circlek1_cki','cki') or die('<span class="error">Error: Unable to connect to server</span>');
	mysql_select_db('circlek1_events') or die('<span class="error">Error: Unable to connect to database</span>');	
	$query='SELECT * FROM `eventlist`';
	if ($secLevel<=650) $query.=' WHERE `deleted`=\'No\'';
	$query.=' ORDER BY `date` ASC';
	$data=mysql_query($query);
	echo '<p class="title"><img src="./images/customeventsearch.png" border="0" alt="Custom Event Search"><!--Custom Event Search//--></p><p class="subtitle" align="center">Search Results</p><p>';
	$num_results=0;
	$results='';
	$current_event_num=1;
	while ($info=mysql_fetch_row($data)){
		$event_is_deleted=($info[12]=='Yes');
		$cur_name=htmlentities(trim(strtolower($info[1])));
		$temp='';
		for ($x=0; $x<strlen($cur_name); $x++){
			if (substr($cur_name,$x,1)=='\''||substr($cur_name,$x,1)=='\\'||substr($cur_name,$x,1)=='\"') $temp.='\\';
			$temp.=substr($cur_name,$x,1);
		}
		$cur_name=$temp;
		$cur_type=htmlentities(trim(strtolower($info[2])));
		$cur_date=htmlentities(trim(strtolower($info[3])));
		$cur_chair=htmlentities(trim(strtolower($info[7])));
		$cur_id=htmlentities(trim(strtolower('a'.$info[0].'a')));
		$cur_keyword=htmlentities(trim(strtolower($info[5]))).' '.htmlentities(trim(strtolower($info[6]))).' '.htmlentities(trim(strtolower($info[8])));
		$temp='';
		for ($x=0; $x<strlen($cur_chair); $x++){
			if (substr($cur_chair,$x,1)=='\''||substr($cur_chair,$x,1)=='\\'||substr($cur_chair,$x,1)=='\"') $temp.='\\';
			$temp.=substr($cur_chair,$x,1);
		}
		$cur_chair=$temp;
		$displayme='&';
		if ($test1){
			if ($displayme=='&'){ //if statement unneccessary - kept for clarity of algorithm structure
				$displayme=strstr($cur_name,$name);				
			}
			else{
				//no else in this case - kept for clarity of algorithm structure	
			}
		}
		if ($test2){
			if ($displayme=='&'){
				$displayme=strstr($cur_type,$type);
			}
			else{
				if ($bool1=='AND') 
					$displayme=$displayme&&strstr($cur_type,$type);
				else //if ($bool1=='OR')
					$displayme=$displayme||strstr($cur_type,$type);
			}
		}
		if ($test4){
			if ($displayme=='&'){
				$displayme=strstr($cur_chair,$chair);
			}
			else{
				if ($bool3=='AND') 
					$displayme=$displayme&&strstr($cur_chair,$chair);
				else //if ($bool3=='OR')
					$displayme=$displayme||strstr($cur_chair,$chair);
			}
		}
		if ($test5){
			if ($displayme=='&'){
				$displayme=strstr($cur_id,$id);
			}
			else{
				if ($bool4=='AND') 
					$displayme=$displayme&&strstr($cur_id,$id);
				else //if ($bool4=='OR')
					$displayme=$displayme||strstr($cur_id,$id);
			}
		}
		if ($test6){
			if ($displayme=='&'){
				$displayme=strstr($cur_keyword,$keyword);
			}
			else{
				if ($bool5=='AND') 
					$displayme=$displayme&&strstr($cur_keyword,$keyword);
				else //if ($bool5=='OR')
					$displayme=$displayme||strstr($cur_keyword,$keyword);
			}
		}
		if ($test3){
			if ($displayme=='&'){
				if (!$date_range)
					$displayme=($cur_date==$start_date);
				else
					$displayme=($cur_date>$start_date&&$cur_date<$end_date);
			}
			else{
				if ($bool2=='AND'){ 
					if (!$date_range)
						$displayme=$displayme&&($cur_date==$start_date);
					else
						$displayme=$displayme&&($cur_date>$start_date&&$cur_date<$end_date);
				}
				else{ //if ($bool2=='OR')
					if (!$date_range)
						$displayme=$displayme||($cur_date==$start_date);
					else
						$displayme=$displayme||($cur_date>$start_date&&$cur_date<$end_date);
				}
			}
		}

		if ($displayme=='&') $displayme=true; //Blank search displays everything
		
		if ($displayme){
			$temp_display_num=$current_event_num.'.';
			if ($event_is_deleted) $temp_display_num='&nbsp;';
			$results.='<tr><td align="right">'.$temp_display_num.'&nbsp;</td><td>&nbsp;<a href="./events/view.php?id='.$info[0].'" target="home">';
			if ($event_is_deleted) $results.='<strike>';
			$results.=$info[1];
			if ($event_is_deleted) $results.='</strike>';
			$results.='</a></td></tr>';
			$num_results++;
			if (!$event_is_deleted) $current_event_num++;
		}
	}
	$static_link='http://www.ucsdcirclek.org'.$_SERVER['REQUEST_URI'];
	$static_size=strlen($static_link);
	if ($static_size>50) $static_size=50;
	echo 'Link to these results: <input size="'.$static_size.'" readonly="readonly" maxlength="'.$static_size.'" type=text onClick=select() value="'.$static_link.'"><br><span class="mini">(Click on the box to select the text)</span></p><p align="center"><a href="customsearch.php" class="mini">[Search Again]</a></p><p>';
	if ($num_results>0) $results='Your search yielded the following '.$num_results.' results:<br>Click on an event to view it.<br><table class="matt">'.$results.'</table>';
	else $results='Sorry, no events could be found that matched your search criteria.';
	echo $results.'</p><p align="center"><a href="customsearch.php" class="mini">[Search Again]</a></p>';
	mysql_close();	
}
?>
</body>
</html>
