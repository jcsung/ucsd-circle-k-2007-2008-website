<?php
$debug=$_GET['debug'];
if(!$debug) $debug=0;
?>
<html>
<head>
<title>Next Meeting</title>
<link rel="stylesheet" type="text/css" href="ucsdcki.css">
<style>
body{
	color: black;
	background-image: url(./images/water_right.jpg);
}
</style>
</head>
<body>
<?php
require('utils.php');
$your_ip_address=GetHostByName($_SERVER['REMOTE_ADDR']);
$session_existence=amanda_session_exists($your_ip_address);
$editable=false;
$secLevel=350;
if ($session_existence){
	$username=amanda_session_get_username($your_ip_address);
	$password=amanda_session_get_password($your_ip_address);
	$loginity=validateLogin($username,$password);
	if ($loginity){
		$secLevel=getInfo($username,$password,'secLevel');
		if ($secLevel>650) $editable=true;
	}

}
if ($editable) $editlink='<a href="./admin/editnextmeeting.php" target="home">';
else $editlink='';
$str='<table border="'.$debug.'" class="matt" width="100%" height="100%">';
$str.='<tr><td width="100%" align="center" valign="middle" class="title underline">'.$editlink.'Next Time '.($editable?'</a>':'').'</td></tr>';
mysql_connect('localhost','circlek1_cki','cki') or die('<span class="error">Error: Unable to connect to server</span>');
mysql_select_db('circlek1_misc') or die('<span class="error">Error: Unable to connect to database</span>');
$query='SELECT * FROM `havenextmeeting`';
$data=mysql_query($query);
$info=mysql_fetch_assoc($data);
$break=$info['break'];
//echo $break.'<br>';
if ($break!='No'){
	$next=array();
	$next['Winter']='in January';
	$next['Spring']='spring quarter';
	$next['Summer']='next fall';
	if ($break!='Cancelled'){
		$str.='<tr><td width="100%" align="center" valign="middle"><h3>After '.$break.' break!'.'</h3></td></tr>';
		$str.='<tr><td width="100%" align="center" valign="middle"><h3>See you '.$next[$break].'!</h3></td></tr>';
	}
	else{
		if(strstr($_SERVER['HTTP_USER_AGENT'],'MSIE')) $num=5;
		else $num=4;
		$str.='<tr><td width="100%" align="center" valign="middle"><h'.($num-1).'>Upcoming meeting is cancelled.</h'.($num-1).'></td></tr>';
		$str.='<tr><td width="100%" align="center" valign="middle"><h'.$num.'>More information is forthcoming.</h'.$num.'></td></tr>';
	}
}
else{
	$now=currentTime();
	if(strstr($_SERVER['HTTP_USER_AGENT'],'MSIE')) $num=5;
	else $num=4;
	$query='SELECT * FROM `meetinglocation` ORDER BY `date` DESC';
	$data=mysql_query($query);
	while($info=mysql_fetch_assoc($data)){
		$timestamp=$info['date'];
		$type=$info['type'];
		$pre_day=date('n/j',$timestamp);
		$pre_time=date('g:i A',$timestamp);
		$pre_location=$info['location'];
		$pre_note=$info['note'];
		$temp=explode(chr(13),$pre_note);
		$pre_note='';
		for ($x=0; $x<sizeOf($temp); $x++){
			$pre_note.=$temp[$x];
			if ($x<sizeOf($temp)-1) $pre_note.=' <br> ';
		}
		$temp=explode('"',$pre_note);
		$pre_note='';
		for ($x=0; $x<sizeOf($temp); $x++){
			$pre_note.=$temp[$x];
			if ($x<sizeOf($temp)-1) $pre_note.='%22';
		}
		$has_map=($info['hasMap']=='Yes');
		$map=$info['map'];
		if ($info['have']=='Yes'){
			$str.='<tr><td width="100%" align="center" valign="middle" class="bold">'.$type.'</td></tr>';
			if ($now<$timestamp+5400){
				$str.='<tr><td width="100%" align="center" valign="middle" class="bold">'.$pre_day.' at '.$pre_time.'</td></tr>';
				$str.='<tr><td width="100%" align="center" valign="middle" class="bold">'.$pre_location.'</td></tr>';
				$flag=$has_map&&trim($map)||trim($pre_note);
				if ($flag)
					$str.='<tr><td width="100%" align="center" valign="middle">';
				if ($has_map&&trim($map))
					$str.='<a href="'.$map.'" target="home">Map</a>&nbsp;';
				if ($has_map&&trim($map)&&trim($pre_note))
					$str.='&nbsp;|&nbsp;';
				if (trim($pre_note))
					$str.='&nbsp;<a href="note.php?text='.$pre_note.'" target="home">Note</a>';
				if ($flag)
					$str.='</td></tr>';
			}
			else{
				$str.='<tr><td width="100%" align="center" valign="middle"><h'.$num.'>TBA</h'.$num.'></td></tr>';
			}
		}
	}
	
}


$str.='</table>';
mysql_close();
echo $str;
?>
</body>
</html>

