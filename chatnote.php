<html>
<head>
<title>Note</title>
<link rel="stylesheet" type="text/css" href="ucsdcki.css">
</head>
<body>
<p class="title" align="center">Note</p>
<p>Aside from what is noted in the <a href="termsofservice.php" target="welcome">Terms of Service</a>, 
please note the following:
<p>HTML, JavaScript, AJAX, PHP, etc. are not accepted in this chatterbox.</p>
<p>The chatterbox will detect and automatically link to any links typed in the chatterbox that have the 
prefix http://.</p>
<p>Links typed in the chatterbox without the prefix http:// will be linked to if they end in .gov, .com, 
.org, or .net.</p>
<p align="center"><A href="javascript:window.close()">[Close Window]</a></p>
</body>
</html>