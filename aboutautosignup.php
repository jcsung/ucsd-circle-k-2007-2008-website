<html>
<head>
<title>What's "Auto-Signup?"</title>
<link rel="stylesheet" type="text/css" href="ucsdcki.css">
</head>
<body>
<p class="subtitle">What is "Auto-Signup?"</p>
<p>If you turn ON Auto-Signup, when you click [Sign up] or [Waitlist] on the Upcoming Events page while 
logged in, you will be automatically signed up for the event with your preferred comment, driving status, 
and stalk event choice as specified in the Account Preferences.  Of course, your signup information can 
be manually edited later on the signup page itself.</p>
<p align="center"><a href="javascript:window.close()">Close Window</a></p>
</body>
</html>