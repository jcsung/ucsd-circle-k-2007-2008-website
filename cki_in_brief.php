<?php
require('utils.php');
?>
<html>
<head>
<title>The New Person's Guide to UCSD Circle K</title>
<link rel="stylesheet" type="text/css" href="ucsdcki.css">
<style>
.matt{
	border-spacing: 15px;
}
.close{
	border-spacing: 0px;
}
td.topoff{
	border-top: thin solid transparent;
}
td.bord{
	border: thin solid white;
}
.square{
	list-style-type: square;
}
</style>
</head>
<body>
<?php
$your_ip_address=GetHostByName($_SERVER['REMOTE_ADDR']);
amanda_session_garbage_collect();
$session_existence=amanda_session_exists($your_ip_address);
if ($session_existence){
	//echo 'session exists<br>';	
	$username=amanda_session_get_username($your_ip_address);
	$password=amanda_session_get_password($your_ip_address);
	$loginity=validateLogin($username,$password);
}
?>
<table class="matt" width="100%">
<tr><td width="100%" align="center" class="title">UCSD Circle K In Brief</td></tr>
<tr><td width="100%" align="center"><a href=".">ucsdcirclek.org</a> / <A href="contact.php">circlek@ucsd.edu</a> / aim: <a href="aim:goim?screenname=circlekucsd">circle k ucsd</a></td></tr>
</table>
<table class="matt" width="100%" border="0">
<tr>
<td width="50%" valign="top"> 
<table class="matt" width="100%">
<tr>
<td>
<table class="matt" width="100%">
<tr><td class="title">CIRCLE K?</td></tr>
<tr><td class="bord"><span class="bold">Circle K</span> is an international community service organization that promotes service, leadership, and fellowship.</td></tr>
<tr><td class="bord"><span class="bold">UCSD Circle K</span> is part of the Paradise Division, CNH District, and is sponsored by the Kiwanis Club of La Jolla</td></tr>
</table>
</td>
</tr>
<tr>
<td>
<table class="matt" width="100%">
<tr><td class="title">COMMON ACRONYMS</td></tr>
<tr>
<td>
<table class="matt close" width="100%">
<tr><td class="bord bold">EVENTS</td></tr>
<tr><td class="bord topoff"><span class="bold">CKI*</span> � Crazy Kompetition for Infants<br><span class="bold">DLSSP</span> � District Large-Scale Service Project<br><span class="bold">DCON</span> � District Convention<br><span class="bold">FTC</span> � Fall Training Conference<br><span class="bold">ICON</span> � International Convention<br></td></tr>
</table>
</td>
</tr>
<tr>
<td class="mini">* CKI stands for �Circle K International� in most references</td>
</tr>
<tr>
<td>
<table class="matt close" width="100%">
<tr><td class="bord bold">ADMINISTRATIVE</td></tr>
<tr><td class="bord topoff"><span class="bold">CNH</span> � California-Nevada-Hawaii<br><span class="bold">LTG</span> � Lieutenant Governor<br><span class="bold">DCM</span> � Divisional Council Meeting<br><span class="bold">CERF</span> � Club Event Report Form<br><span class="bold">MRF</span> � Monthly Report Form<br><span class="bold">MRP</span> � Member Recognition Program<br><span class="bold">MD&E</span> � Membership Development/Education<br><span class="bold">MOM</span> � Member of the Month<br></td></tr>
</table>
</td>
</tr>
<tr>
<td>
<table class="matt close" width="100%">
<tr><td class="bord bold">PROGRAMS</td></tr>
<tr><td class="bord topoff"><span class="bold">DSI</span> � District Service Initiative<br><span class="bold">DFI</span> � District Fundraising Initiative<br><span class="bold">PTP</span> � Pediatric Trauma Program<br><span class="bold">K-Fam</span> � Kiwanis Family<br></td></tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td>
<table class="matt" width="100%">
<tr><td class="title">INTERESTED?</td></tr>
<tr>
<td>
<table class="matt close" width="100%">
<tr><td class="bord bold">MEMBER BENEFITS</td></tr>
<tr><td class="bord topoff"><ul class="square"><li>Campus and Community Service</li><li>Leadership Development</li><li>Professional Development</li><li>Lifetime Friendships</li><li>Scholarship Opportunities (up to $1,000!)</li><li>Kiwanis Meetings (Free Lunches!)</li></ul></td></tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td>
<table class="matt" width="100%">
<tr><td class="title">LIST SERVE!</td></tr>
<tr><td class="bord"><a href="http://groups.yahoo.com/subscribe/ucsdcirclek">http://groups.yahoo.com/subscribe/ucsdcirclek</a><br>Receive weekly emails about meetings and events!</td></tr>
</table>
</td>
</tr>
</table>
</td>

<td width="50%" valign="top"> 
<table class="matt" width="100%">
<tr>
<td>
<table class="matt" width="100%">
<tr>
<td>
<table class="matt" width="100%">
<tr><td class="title">CLUB TRADITIONS</td></tr>
<tr>
<td>
<table class="matt close" width="100%">
<tr><td class="bord bold">MEETINGS</td></tr>
<tr><td class="bord topoff"><ul class="square"><li><span class="bold">Bell and Gavel</span> � The President�s prized possessions. Snag them and you can make him/her do anything you want!</li><li><span class="bold">Fines and Finks</span> � On-going fundraiser. Drop in a Happy/Sad Dollar (or any loose change) to share whatever you want at the end of the meeting!</li>Money must also be put in if fined (ie. cell phone ringing in the middle of the meeting).</li><li><span class="bold">Crobe</span> � Our great mascot, representative of our �Circle K Disease� theme.</li></ul></td></tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td>
<table class="matt" width="100%">
<tr><td class="title">AMANDA</td></tr>
<tr><td class="bord"><span class="bold">AMANDA</span> refers to our automated member database system. With an AMANDA account, you can sign upfor events, un-sign up for events, and check your monthly MRP status� all online!<br>Go to the club website and click on the AMANDA tab and follow the account registration instructions. If you need help, just give us a holler!<br></td></tr>
<tr>
<td>
<table class="matt close" width="100%">
<tr><td class="bord bold">SIGNING UP FOR EVENTS</td></tr>
<tr><td class="bord topoff"><ol><li>Go to <a href=".">ucsdcirclek.org</a> and log in with your account <br><span class="mini">(You may also sign up for events as a guest. Just go to Step 2)</span></li><li>Check out the calendar. Click on an event to find out more about it.</li><li> Sign up or un-sign up for an event by clicking the sign-up/un-sign up button at the bottom of the event�s info page.</li><li>Sign ups must be made 24 hrs prior to the event.</li><li>All events meet at <a href="./events/defaultmeetinglocation.php" target="_blank">PCU</a> unless otherwise noted.</li></ol></td></tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td>
<table class="matt" width="100%">
<tr><td class="title">HOW TO BECOME A MEMBER</td></tr>
<tr><td class="bord"><ul class="square"><li>Attend at least 1 service event*</li><li>Get a member application (from meeting/website)</li><li>Pay yearly dues ($35) with application</li><li>Turn application and dues to a board member!</li></ul></td></tr>
<tr><td>* Effective starting Fall 2007</td></tr>
</table>
</td>
</tr>
<tr>
<td>
<table class="matt" width="100%">
<tr>
<td class="bord">
<table class="matt" width="100%">
<tr><td class="title">QUESTIONS?  CONCERNS?</TD></TR>
<tr><td>Feel free to ask any of us! We�re here to help!</td></tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</body>
</html>