<html>
<head>
<title>AMANDA Registration</title>
<link rel="stylesheet" type="text/css" href="ucsdcki.css">
</head>
<body>
<p class="subtitle" align="center">UCSD Circle K AMANDA Registration</p>
<form action="process_registration.php" method="post">
<p><table class="matt" border="0">
<tr><td colspan="2" align="left" class="bold">Registration Information</td></tr>
<tr><td colspan="2" class="mini">Items marked with a * are required unless stated otherwise</td></tr>
<tr><td valign="top">Username: *<br><span class="mini">(Max 30 characters)</span></td><td valign="top"><input type="text" name="username" maxlength="30"></td></tr>
<tr><td valign="top">Password: *<br><span class="mini">(Max 30 characters)</span></td><td valign="top"><input type="password" name="password"></td></tr>
<tr><td valign="top">Confirm Password: *</td><td valign="top"><input type="password" name="confpassword"></td></tr>
<tr><td colspan="2" align="left" class="bold">Profile Information</td></tr>
<tr><td colspan="2" align="left" class="mini">This information will be viewable by all logged-in AMANDA users</td></tr>
<tr><td valign="top">First Name: *</td><td valign="top"><input type="text" name="fname"></td></tr>
<tr><td valign="top">Middle Name: </td><td valign="top"><input type="text" name="mname"></td></tr>
<tr><td valign="top">Last Name: *</td><td valign="top"><input type="text" name="lname"></td></tr>
<tr><td valign="top">E-mail: *<br><span class="mini">Please enter an email you use frequently</span></td><td valign="top"><input type="text" name="email"></td></tr>
<tr><td valign="top">Phone: *<br><span class="mini">Please use your primary phone</span></td><td valign="top"><table><tr> 
<?php
	require('utils.php');
	for ($x=0; $x<10; $x++){
		echo '<td valign="middle" align="center"><select name="phone'.$x.'" size="10">';
		for ($y=0; $y<10; $y++){
			echo '<option>'.$y.'</option>';
		}
		echo '</select></td>';
		if ($x==2||$x==5) echo '<td valign="middle" align="center">--</td>';
	}
?>
</tr></table></td></tr>
<tr><td valign="top">College: <br><span class="mini">If N/A, you can leave it as ERC.</span></td><td valign="top"><select name="college">
<?php
	$colleges=explode(' ','ERC Marshall Muir Revelle Sixth Warren');
	for ($x=0; $x<sizeOf($colleges); $x++)
		echo '<option>'.$colleges[$x].'</option>';
?>
</select></td></tr>
<tr><td valign="top">Year: </td><td valign="top"><select name="year">
<?php
	for ($x=1; $x<=12; $x++)
		echo '<option>'.$x.'</option>';
?>
</select></td></tr>
<tr><td valign="top">Sex: </td><td valign="top"><select name="sex">
<?php
	$sexes=explode('.','Male.Female.Other.Yes, Please');
	for ($x=0; $x<sizeOf($sexes); $x++)
		echo '<option>'.$sexes[$x].'</option>';
?>
</select></td></tr>
<tr><td valign="top">Birthday: </td><td valign="top">
<?php
	$months=explode(' ','January February March April May June July August September October November December');
	echo '<select name="dobm">';
	for ($x=0; $x<sizeOf($months); $x++)
		echo '<option value="'.($x+1).'">'.$months[$x].'</option>';
	echo '</select>&nbsp;&nbsp;';
	echo '<select name="dobd">';
	for ($x=1; $x<=31; $x++)
		echo '<option>'.$x.'</option>';
	echo '</select>,&nbsp;&nbsp;';
	echo '<select name="doby">';
	for ($x=getYear(currentTime()); $x>=1970; $x--)
		echo '<option>'.$x.'</option>';
	echo '</select>&nbsp;&nbsp;';
?>
</td></tr>
<tr><td valign="top">AIM Screen Name: </td><td valign="top"><input type="text" name="aim"></td></tr>
<tr><td valign="top">ICQ Number: </td><td valign="top"><input type="text" name="icq"></td></tr>
<tr><td valign="top">MSN Messenger Name: </td><td valign="top"><input type="text" name="msn"></td></tr>
<tr><td valign="top">Y! Messenger Name: </td><td valign="top"><input type="text" name="yahoo"></td></tr>
<tr><td valign="top">Personal Web Page: </td><td valign="top"><input type="text" name="website"></td></tr>
<tr><td valign="top">Interests: </td><td valign="top"><textarea name="interests" rows="4" cols="55"></textarea></td></tr>
<tr><td valign="top">Favorite Music: </td><td valign="top"><textarea name="music" rows="4" cols="55"></textarea></td></tr>
<tr><td valign="top">TV Shows: </td><td valign="top"><textarea name="tv" rows="4" cols="55"></textarea></td></tr>
<tr><td valign="top">Favorite Movies: </td><td valign="top"><textarea name="movies" rows="4" cols="55"></textarea></td></tr>
<tr><td valign="top">Favorite Books: </td><td valign="top"><textarea name="books" rows="4" cols="55"></textarea></td></tr>
<tr><td colspan="2" align="left" class="bold">Account Options</td></tr>
<tr><td colspan="2" class="mini">The Auto-Signup and Auto-Fill Options cannot be set at this time, but can be later on from the Edit Preferences page.</td></tr>
<?php
	$ops=explode(' ','hideEmail hidePhone hideSex hideDOB hideIM notifyIfEventInfoChanged notifyIfSignupsChanged');
	$descs=explode(',','Hide my email on my profile,Hide my phone number on my profile,Hide sex on my profile,Hide my date of birth on my profile,Hide my Screen Names on my profile,Email me if event information is<br>changed on my signed-up events,Email me if signups change on<br>my signed-up events');
	for ($x=0; $x<sizeOf($ops); $x++){
		echo '<tr><td valign="top">'.$descs[$x].':</td><td valign="top"><select name="'.$ops[$x].'"><option>No</option><option>Yes</option></select></td></tr>';
	}
?>
<tr><td colspan="2"><input type="checkbox" name="terms">&nbsp;&nbsp;&nbsp;I have read and agree to the <a href="termsofservice.php" target="_blank">Terms of Service</a>.</td></tr>
<tr><td colspan="2"><input type="submit" name="submit" value="Register"></td></tr>
</table></p></form>
</body>
</html>