<html>
<head>
<title>AMANDAnalytics Prototype</title>
<link rel="stylesheet" type="text/css" href="../ucsdcki.css">
</head>
<body>
<?php
/*
require('utils.php');
$your_ip_address=GetHostByName($_SERVER['REMOTE_ADDR']);
amanda_session_garbage_collect();
$session_existence=amanda_session_exists($your_ip_address);
if ($session_existence){
	$username=amanda_session_get_username($your_ip_address);
	$password=amanda_session_get_password($your_ip_address);
	$loginity=validateLogin($username,$password);
	$secLevel=getInfo($username,$password,'secLevel');
}
else
	$secLevel=150;
*/
$who=htmlentities(trim(strtolower($_GET['who'])));
mysql_connect('localhost','circlek1_cki','cki') or die('<span class="error">Error: Unable to connect to server</span>');
mysql_select_db('circlek1_events') or die('<span class="error">Error: Unable to connect to database</span>');
/*
$query='SELECT * FROM `profiles` WHERE `username`=\''.$who.'\'';
$data=mysql_query($query);
if (mysql_num_rows($data)<1)
	die('<span class="error">Error - User not found: '.$who.'</span>');
*/

$totalSignups=0;
$serialList=array();
$eventList=array();
$freqs=array();
$possibleTypeString='Service,Social,Fundraiser,Kiwanis Family,MD&E,Interclub,District,Divisional,International,Administrative,Testing Purposes';
$possibleTypes=explode(',',$possibleTypeString);
for ($x=0; $x<sizeOf($possibleTypes); $x++)
	$freqs[$possibleTypes[$x]]=0;

$query='SELECT * FROM `eventlist` WHERE 1 ORDER BY `date` ASC';
$data=mysql_query($query);
while ($info=mysql_fetch_row($data)){
	$tq='SELECT * FROM `'.$info[0].'` WHERE `username`=\''.$who.'\'';
	//echo $tq.'<br>';
	$td=mysql_query($tq);
	if (mysql_num_rows($td)==0) continue;
	//echo $info[0].'<br>';
	$totalSignups++;
	array_push($serialList,$info[0]);
	array_push($eventList,$info[1]);
	$temp=explode(',',$info[2]);
	for ($x=0; $x<sizeOf($temp); $x++){
		if(@strpos($possibleTypeString,$temp[$x])===false) echo '<span class="error">***Warning: Nonstandard Type ("'.$temp[$x].'") found!  Click <a href="http://ucsdcirclek.org/?show=http://www.ucsdcirclek.org/?show=http://ucsdcirclek.org/events/view.php?id='.$info[0].'">here</a> to show.***</span><br>';
		$freqs[$temp[$x]]++;
	}
}
//print_r($eventList); echo '<br>';
//print_r($freqs); echo '<br>';
$flag=arsort($freqs,SORT_NUMERIC);
if ($flag===false) die('<span class="Error">Error: Unable to process data.</span>');
echo '<p><h1>AMANDAnalytics data for '.$who.'</h1></p>';
echo "<p>Number of signed-up events: $totalSignups</p>";
echo '<p>Event Type Frequencies: <br>';
$curFreq=-1;
$x=0;
foreach($freqs as $type=>$frequency){
	if ($curFreq!=$frequency){
		$x++;
		$curFreq=$frequency;
	}
	echo "$x. $type: $frequency (".@($frequency/$totalSignups*100)."%)<br>";
	
}
echo '</p>';
echo '<p>List of signed-up events:<br>';
$len=sizeOf($eventList);
for ($numMaxDigits=0; intval($len)>0; $numMaxDigits++)
	$len/=10;
$len=sizeOf($eventList);
for ($x=0; $x<$len; $x++){
	echo ($x+1).'.';
	$temp=$x;
	$numDigits=0;
	for ($numDigits=0; intval($temp)>0; $numDigits++) 
		$temp/=10;
	for ($y=$numDigits; $y<$numMaxDigits; $y++)
		echo'&nbsp;';
	echo '&nbsp;<a href="http://www.ucsdcirclek.org/?show=http://www.ucsdcirclek.org/events/view.php?id='.$serialList[$x].'">'.$eventList[$x].'</a><br>';
}
echo '</p>';
mysql_close();
?>
</body>
</html>