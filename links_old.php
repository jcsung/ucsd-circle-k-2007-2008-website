<table width="600" border="0" cellspacing="10" cellpadding="0">
<tr>
<td colspan="2" align="center"><font face="comic sans ms" color="white"><p><h1><u>Links</u></h1></p></font></td>
</tr>
<tr>
<td colspan="2"><font face="comic sans ms" color="white"><p>Here you can find links to any resources available to people from Circle K!</p></font></td>
</tr>
<tr><td align="center" colspan="2">&nbsp;</td></tr>
<tr><td align="center" colspan="2">&nbsp;</td></tr>
<tr>
<td align="center" valign="top"><img src="ucsd_logo.gif" alt="" border="0"></td>
<td><font face="comic sans ms" color="white">
<p><h2>Us: UCSD</h2></p>
<p>
<ul type="square">
<li><a href="http://groups.yahoo.com/group/ucsdcirclek/" target="_blank">Our Yahoo! Group</a><ul><li>Join this to receive our emails informing you of meeting locations, project times, and anything else we have to tell you!</li></ul></li>
<li><a href="http://www.ljkiwanis.org/" target="_blank">Our Sponsoring Kiwanis' Website</a><ul><li>This is the website of our sponsoring Kiwanis club, containing all of their online resources!</li></ul></li>
</ul>
</p>
</font></td>
</tr>
<tr><td align="center" colspan="2">&nbsp;</td></tr>
<tr>
<td align="center" valign="top"><img src="paradise_logo.gif" alt="" border="0"></td>
<td><font face="comic sans ms" color="white">
<p><h2>The Division: Paradise</h2></p>
<p>
<ul type="square">
<li><a href="http://www.myspace.com/grossmontcirclek" target="_blank">Grossmont College Circle K</a></li>
<li><a href="http://www-rohan.sdsu.edu/~circlek/" target="_blank">San Diego State University Circle K</a></li>
<li><a href="http://www2.hawaii.edu/~cki" target="_blank">University of Hawaii, Manoa Circle K</a></li>
</ul>
</p>
</font></td>
</tr>
<tr><td align="center" colspan="2">&nbsp;</td></tr>
<tr>
<td align="center" valign="top"><img src="calnevha_logo.gif" alt="" border="0"></td>
<td><font face="comic sans ms" color="white">
<p><h2>The District: Cal-Nev-Ha</h2></p>
<p>
<ul type="square">
<li><a href="http://www.cnhcirclek.org/" target="_blank">Circle K Cal-Nev-Ha District Website</a><ul><li>This is the website for our district, California-Nevada-Hawaii (aka Cal-Nev-Ha).  Here you will find many, many resources available to you, including the district newsletter, the <a href="http://www.cnhcirclek.org/pubsunburst.html">Sunburst</a>.  So why don't you check out this site?</li></ul></li>
<li><a href="http://ucdcki.pyden.net/" target="_blank">University of California, Davis Circle K</a></li>
<li><a href="http://geocities.com/UOPCircleK" target="_blank">University of the Pacific Circle K</a></li>
<li><a href="http://www.unrcirclek.bravehost.com/" target="_blank">University of the Nevada, Reno Circle K</a></li>
<li><a href="http://www.csufcirclek.org/" target="_blank">University of California, Fresno Circle K</a></li>
<li><a href="http://www.calpoly.edu/~cirkclub/" target="_blank">California Polytechnic State University, San Luis Obispo Circle K</a></li>
<li><a href="http://orgs.sa.ucsb.edu/cki" target="_blank">University of California, Santa Barbra Circle K</a></li>
<li><a href="http://www.ucrcirclek.org/" target="_blank">University of California, Riverside Circle K</a></li>
<li><a href="http://circlek.unlv.edu/" target="_blank">University of Nevada, Las Vegas Circle K</a></li>
<li><a href="http://www.csupomona.edu/~cki/" target="_blank">California Polytechnic State University, Pomona Circle K</a></li>
<li><a href="http://www.circlekpcc.org/" target="_blank">Pasadena City College Circle K</a></li>
<li><a href="http://www.deanzacki.com/" target="_blank">DeAnza College Circle K</a></li>
<li><a href="http://www.humboldt.edu/~circlek" target="_blank">Humboldt State University Circle K</a></li>
<li><a href="http://www.circleksf.org/" target="_blank">San Francisco State University Circle K</a></li>
<li><a href="http://student.santarosa.edu/circlek/index.html" target="_blank">Santa Rosa Jr. College Circle K</a></li>
<li><a href="http://www.stanford.edu/group/circle-k" target="_blank">Stanford University Circle K</a></li>
<li><a href="http://www.ocf.berkeley.edu/~circlek/" target="_blank">University of California, Berkeley Circle K</a></li>
<li><a href="http://www.geocities.com/ckicsuf/" target="_blank">California State University, Fullerton Circle K</a></li>
<li><a href="http://www.ucicirclek.org/" target="_blank">University of California, Irvine Circle K</a></li>
<li><a href="http://www.csudh.edu/circle_k" target="_blank">California State University, Dominguez Hills Circle K</a></li>
<li><a href="http://www.beachcki.org/" target="_blank">Long Beach State University Circle K</a></li>
<li><a href="http://circlek.bol.ucla.edu/" target="_blank">University of California, Los Angeles Circle K</a></li>
<li><a href="http://www-scf.usc.edu/~circlek/" target="_blank">University of Southern California Circle K</a></li>
</ul>
</p>
</font></td>
</tr>
<tr><td align="center" colspan="2">&nbsp;</td></tr>
<tr>
<td align="center" valign="top"><img src="world_logo.gif" alt="" border="0"></td>
<td><font face="comic sans ms" color="white">
<p><h2>The World: Circle K International</h2></p>
<p>
<ul type="square">
<li><a href="http://www.circlek.org/circlek/" target="_blank">Circle K International Website</a><ul><li>This is the international website for Circle K.  It contains tons of resources for Circle Kers ALL over the world - from information on scholarships to job opportunities, internships, and more!  This site is definitely worth checking out (you'll need to register for an account though).</li></ul></li>
</ul>
</p>
</font></td>
</tr>
</table>