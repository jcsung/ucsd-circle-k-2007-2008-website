Home
About CKI
Member Tools
	Calendar
	Members
	My MRP
Leadership
	Board
	Committees
Resources
	Links+ Files (merged)
	Pictures
Help
	Contact
	AMANDA tutorial (new)
	About AMANDA+Dev Team (merged)
	Changelog
Admin Tools
-------------------------------------------
Confused?
Basically, the menu bar will look like this:

Home  Member Tools  Leadership  Resources  Help  Admin Tools

Putting your mouse above Leadership will lead to this:

Home  Member Tools  Leadership++Resources  Help  Admin Tools
                    |Board     |
	            |Committees|
	            ++++++++++++

(except, you know, better-looking).

Uh, I forgot About CKI.  But you get my gist.