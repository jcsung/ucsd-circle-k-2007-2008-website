<?php
class Database{
	private $dbhost;
	private $username;
	private $password;
	private $dbh;
	
	function __construct(){
		$this->dbhost='localhost';
		$this->username='circlek1_cki';
		$this->password='cki';
		$this->dbh=mysql_connect($this->dbhost,$this->username,$this->password) or die('<span class="error">Error: Could not connect to SQL server because of '.mysql_error().'</span>');
	}

	function select($database){
		$flag=strstr($database,'circlek1_');
		if ($flag) $database=substr($database,strpos($database,'_')+1,strlen($database));
		$flag=@mysql_select_db('circlek1_'.$database,$this->dbh);
		if ($flag===false) die('<span class="error">Error: Could not find specified database &quot;'.$database.'&quot;</span>');
		return true;
	}

	function query($q){
		$data=$this->submitQuery($q) or die('<span class="error">Error: Invalid Query</span>');
		$results=array();
		while($info=mysql_fetch_assoc($data)){
			array_push($results,$info);			
		}
		return $results;
	}

	
	private function submitQuery($q){
		return mysql_query($q,$this->dbh);
	}

	function __destruct(){
		mysql_close($this->dbh);
	}

}
?>