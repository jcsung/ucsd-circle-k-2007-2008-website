<?php
$pseudoframe=trim($_GET['mod']);
if (strstr($pseudoframe,'.php')){
	$pseudoframe=substr($pseudoframe,0,strpos($pseudoframe,'.php'));
}
if (!file_exists($pseudoframe.'.php')){
	$pseudoframe='home';
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=utf-8">
<title>UCSD Circle K</title>
<META name="description" content="UCSD Circle K Website">
<META name="keywords" content="0, 1, UC University of California San Diego Circle K CNH California Nevada Hawaii Cal Nev Ha Kiwanis amanda awesome jeffrey sung upessimist">

<link rel="shortcut icon" href="http://www.ucsdcirclek.org/ucsdckifavicon.ico" type="image/x-icon">
<link rel="icon" href="http://www.ucsdcirclek.org/ucsdckifavicon.ico" type="image/x-icon">

<link rel="stylesheet" type="text/css" href="ucsdcki.css">
</head>
<body>
<table class="matt" width="100%" height="100%" border="1">
<tr>
	<td width="80%" height="25%" align="center">top</td>
	<td width="20%" height="25%">next meeting</td>
</tr>
<tr>
	<td width="80%"><?php require($pseudoframe.'.php'); ?></td>
	<td width="20%"><?php require('chatterbox.php'); ?></td>
</tr>

<!--
<tr>
<td width="100%" height="25%">
	<table class="matt" width="100%" height="100%" border="1">
	<tr>
	<td width="80%" height="100%" align="center">top</td>
	<td width="20%" height="100%">next meeting</td>
	</tr>
	</table>
</td>
</tr>

<tr>
<td width="100%">
	<table class="matt" width="100%" height="100%" border="1">
	<tr>
	<td width="80%" height="100%"><?php require($pseudoframe.'.php'); ?></td>
	<td width="20%" height="100%">chatterbox</td>
	</tr>
	</table>
</td>
</tr>
//-->

</table>
</body>
</html>