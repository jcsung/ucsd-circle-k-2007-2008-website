<?php

require_once 'Database.php'; //The database is required to build a User.

//Class definition for an AMANDA account user
class User{

	private $data=array(); //array that holds the account data
	
/*
	function __construct($username,$password,$fname,$mname,$lname,$email,$phone,$college,$year,$sex,$dob,$aim,$icq,$msn,$yahoo,$website,$interests,$music,$tv,$books,$mrp,$picture,$secLevel){
		//Constructor - creates a User
		//Deprecated: 2/28/08
		//Reason:     Unwieldy to use, unwieldy to update, has no advantage (unlike the list of get*() functions below, 
		//	      which have the advantage of convenience to the end-user)
		$this->data['username']=$username;
		$this->data['password']=$password;
		$this->data['fname']=$fname;
		$this->data['mname']=$mname;
		$this->data['lname']=$lname;
		$this->data['email']=$email;
		$this->data['phone']=$phone;
		$this->data['college']=$college;
		$this->data['year']=$year;
		$this->data['sex']=$sex;
		$this->data['dob']=$dob;
		$this->data['aim']=$aim;
		$this->data['icq']=$icq;
		$this->data['msn']=$msn;
		$this->data['yahoo']=$yahoo;
		$this->data['website']=$website;
		$this->data['interests']=$interests;
		$this->data['music']=$music;
		$this->data['tv']=$tv;
		$this->data['books']=$books;
		$this->data['mrp']=$mrp;
		$this->data['picture']=$picture;
		$this->data['secLevel']=$secLevel;
	}
*/

	function __construct(){
		//Constructor - creates a (null) User
	}

	function buildPossibleParameters(){
		//Builds possible parameters for a User 
	}

	function getPossibleParameters(){
		//Returns all possible User data parameters
		//For convenience purposes
		return array_keys($this->data);
	}

	function setValueOf($parameter,$value){
		//Sets any data parameter
		//For convenience purposes
		//No sanity check!
		$this->data[$parameter]=$value;		
	}

	function getValueOf($parameter){
		//Gets any parameter
		//For convenience purposes
		return $this->data[$parameter];
	}

////////// The functions that follow are the get*() functions.  They should be updated when new user data is added to the database.

	function getName(){
		return $this->data['fname'].' '.$this->data['lname'];
	}

	function getFullName(){
		return $this->data['fname'].' '.$this->data['mname'].' '.$this->data['lname'];
	}

	function getUsername(){
		return $this->data['username'];
	}

	function getPassword(){
		return $this->data['password'];
	}

	function getFname(){
		return $this->data['fname'];
	}

	function getMname(){
		return $this->data['mname'];
	}

	function getLname(){
		return $this->data['lname'];
	}

	function getEmail(){
		return $this->data['email'];
	}

	function getPhone(){
		return $this->data['phone'];
	}

	function getCollege(){
		return $this->data['college'];
	}

	function getYear(){
		return $this->data['year'];
	}

	function getSex(){
		return $this->data['sex'];
	}

	function getDob(){
		return $this->data['dob'];
	}

	function getAim(){
		return $this->data['aim'];
	}

	function getIcq(){
		return $this->data['icq'];
	}

	function getMsn(){
		return $this->data['msn'];
	}

	function getYahoo(){
		return $this->data['yahoo'];
	}

	function getWebsite(){
		return $this->data['website'];
	}

	function getInterests(){
		return $this->data['interests'];
	}

	function getMusic(){
		return $this->data['music'];
	}

	function getTv(){
		return $this->data['tv'];
	}

	function getBooks(){
		return $this->data['books'];
	}

	function getMrp(){
		return $this->data['mrp'];
	}

	function getPicture(){
		return $this->data['picture'];
	}

	function getSecLevel(){
		return $this->data['secLevel'];
	}
////////// End set*() functions

////////// The functions that follow are the set*() functions.  They should be updated when new user data is added to the database.

	function setUsername($username){
		$this->data['username']=$username;
	}

	function setPassword($password){
		$this->data['password']=$password;
	}

	function setFname($fname){
		$this->data['fname']=$fname;
	}

	function setMname($mname){
		$this->data['mname']=$mname;
	}

	function setLname($lname){
		$this->data['lname']=$lname;
	}

	function setEmail($email){
		$this->data['email']=$email;
	}

	function setPhone($phone){
		$this->data['phone']=$phone;
	}

	function setCollege($college){
		$this->data['college']=$college;
	}

	function setYear($year){
		$this->data['year']=$year;
	}

	function setSex($sex){
		$this->data['sex']=$sex;
	}

	function setDob($dob){
		$this->data['dob']=$dob;
	}

	function setAim($aim){
		$this->data['aim']=$aim;
	}

	function setIcq($icq){
		$this->data['icq']=$icq;
	}

	function setMsn($msn){
		$this->data['msn']=$msn;
	}

	function setYahoo($yahoo){
		$this->data['yahoo']=$yahoo;
	}

	function setWebsite($website){
		$this->data['website']=$website;
	}

	function setInterests($interests){
		$this->data['interests']=$interests;
	}

	function setMusic($music){
		$this->data['music']=$music;
	}

	function setTv($tv){
		$this->data['tv']=$tv;
	}

	function setBooks($books){
		$this->data['books']=$books;
	}

	function setMrp($mrp){
		$this->data['mrp']=$mrp;
	}

	function setPicture($picture){
		$this->data['picture']=$picture;
	}

	function setSecLevel($secLevel){
		$this->data['secLevel']=$secLevel;
	}
////////// End set*() functions

	function __toString(){
		//A (simple) text representation of the User
		//For debugging purposes only...

		$str='<table border="1">';
		$keys=array_keys($this->data);
		for ($x=0; $x<sizeOf($keys); $x++){
			$str.='<tr>';
			$str.='<td><b>'.$keys[$x].'</b></td>';
			$data=explode(chr(13),$this->data[$keys[$x]]);
			$temp='';
			for ($y=0; $y<sizeOf($data); $y++){
				$temp.=$data[$y];
				if ($y<sizeOf($data)-1) $temp.=' <br> ';
			}
			if (!trim($temp)) $temp='&nbsp;';
			$str.='<td>'.$temp.'</td>';
			$str.='</tr>';
		}
		$str.='</table>';
		return $str;
	}	


}
?>