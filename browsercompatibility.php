<html>
<head>
<title>UCSD Circle K Website Browser Compatibility</title>
<link rel="stylesheet" type="text/css" href="ucsdcki.css">
</head>
<body>
<p class="title">Browser Compatibility</p>
<p>Minimum browser requirements: ability to display frames, ability to render pages via CSS, and support for JavaScript.</p>
<p><table class="matt" width="100%" border="1">
<tr><td align="center" class="bold">Browser</td><td align="center" class="bold">Version</td><td align="center" class="bold">Compatiblity</td></tr>
<tr>
<td align="center" valign="middle">Internet Explorer</td>
<td align="center" valign="middle">7.0+</td>
<td align="left" valign="middle"><p align="center">Mostly Compatible</p><p>Internet Explorer has some problems with CSS and Javascript.</p></td>
</tr>

<tr>
<td align="center" valign="middle">Internet Explorer</td>
<td align="center" valign="middle">6.0-</td>
<td align="left" valign="middle"><p align="center">Not Compatible</p><p>Some links (for instance, the member search results) are unclickable.  There is currently no workaround for this.</p><p>JavaScript support iffy.  No workaround for this.</p><p>Internet Explorer's built-in CSS engine does not render the page correctly.  Workaround: Update to Internet Explorer 7</p></td>
</tr>

<tr>
<td align="center" valign="middle">Mozilla Firefox</td>
<td align="center" valign="middle">2.0+</td>
<td align="left" valign="middle"><p align="center">Compatible</p><p>Sometimes pages with unupdated data are displayed.  Workaround: right-click on the frame and goto This Frame > Reload Frame.</p></td>
</tr>

<tr>
<td align="center" valign="middle">Mozilla Firefox</td>
<td align="center" valign="middle">1.4 - 2.0</td>
<td align="left" valign="middle"><p align="center">Suspected Compatible (never tested)</p></td>
</tr>


<tr>
<td align="center" valign="middle">Opera</td>
<td align="center" valign="middle">9.2+</td>
<td align="left" valign="middle"><p align="center">Compatible</p><p>Due to Opera's frequent caching of pages, pages with unupdated data may be frequently displayed.  Workaround: left-click on the frame, then right-click on the frame and goto Frame > Reload.</p></td>
</tr>

<tr>
<td align="center" valign="middle">Opera</td>
<td align="center" valign="middle">9.0 - 9.2</td>
<td align="left" valign="middle"><p align="center">Suspected Compatible (never tested)</p></td>
</tr>

<tr>
<td align="center" valign="middle">Opera</td>
<td align="center" valign="middle">7.0 - 9.0</td>
<td align="left" valign="middle"><p align="center">Suspected Partially Compatible (never tested)</p></td>
</tr>

<tr>
<td align="center" valign="middle">Safari (Mac Only)</td>
<td align="center" valign="middle">2.0+</td>
<td align="left" valign="middle"><p align="center">Compatible</p></td>
</tr>

<tr>
<td align="center" valign="middle">Lynx</td>
<td align="center" valign="middle">2.0+</td>
<td align="left" valign="middle"><p align="center">Not Compatible</p><p>Does not support frames or JavaScript</p></td>
</tr>


</table></p>
<p align="center"><a href="javascript:window.close()" class="mini">[Close]</a></p>
</body>
</html>