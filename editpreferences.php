<html>
<head>
<title>Edit Preferences</title>
<link rel="stylesheet" type="text/css" href="ucsdcki.css">
<script language="javascript">
<!--
function autosignup(){
	window.open('aboutautosignup.php','AboutAutoSignup','width=300,height=300,resizable=yes,scrollbars=yes,location=no');
}
function autofill(){
	window.open('aboutautofill.php','AboutAutoFill','width=300,height=300,resizable=yes,scrollbars=yes,location=no');
}
//-->
</script>
</head>
<body>
<?php
require('utils.php');
$your_ip_address=GetHostByName($_SERVER['REMOTE_ADDR']);
amanda_session_garbage_collect();
$session_existence=amanda_session_exists($your_ip_address);
if ($session_existence){
	$username=amanda_session_get_username($your_ip_address);
	$password=amanda_session_get_password($your_ip_address);
	$loginity=validateLogin($username,$password);
}
if (!loginity||!$session_existence) die('<span class="error">You should not be here.</span>');
$humanoid=htmlentities(trim($_GET['humanoid']));
//if (strtolower(htmlentities($username))!=strtolower($humanoid)) die('<span class="error">Error: Information does not match.</span>');
if ($_GET['msg']){
	echo '<p class="bold">'.$msg.'</p>';
}
?>
<p class="title">Account Preferences</p>
<p>Here you can change your preferences for your AMANDA account.</p>
<?php

echo '<form action="editprefs.php?humanoid='.$username.'" method="post">';
mysql_connect('localhost','circlek1_cki','cki') or die('<span class="error">Error: Unable to connect to server</span>');
mysql_select_db('circlek1_members') or die('<span class="error">Error: Unable to connect to database</span>');
$query='SELECT * FROM `options` WHERE `username`=\''.$username.'\'';
$data=mysql_query($query);
if (mysql_num_rows($data)==0) die('<span class="error">Error: Your Account Options Information could not be found!</span>');
$info=mysql_fetch_row($data);
$indicies=explode(' ','1 2 3 4 8 5 6');
$keys=explode(' ','hideEmail hidePhone hideSex hideDOB hideIM notifyIfEventInfoChanged notifyIfSignupsChanged');
$desc=explode(',','Hide my email on my profile,Hide my phone number on my profile,Hide sex on my profile,Hide my date of birth on my profile,Hide my Screen Names on my profile,Email me if event information is<br>changed on my signed-up events,Email me if signups change on<br>my signed-up events');
$yesno=explode(' ','No Yes');
echo '<p><table class="matt">';
for ($x=0; $x<sizeOf($indicies); $x++){
	echo '<tr><td valign="top">'.$desc[$x].'</td><td valign="top">';
	echo '<select name="'.$keys[$x].'">';
	for ($y=0; $y<2; $y++){
		echo '<option';
		if ($yesno[$y]==$info[$indicies[$x]]) echo ' selected="selected"';
		echo '>'.$yesno[$y].'</option>';
	}
	echo '</select>';
	echo '</td></tr>';
	echo '<tr><td colspan="2">&nbsp;</td></tr>';
}
echo '</table></p>';
echo '<p><table class="matt">';
echo '<tr><td>Auto-Fill is: </td><td><select name="autoFill">';
$onoff=explode(' ','on off');
for ($x=0; $x<2; $x++){
	echo '<option';
	if ($onoff[$x]==$info[9]) echo ' selected="selected"';
	echo ' value="'.$onoff[$x].'">'.strtoupper($onoff[$x]).'</option>';
}
echo '</select> <span class="mini"><a href="javascript:autofill()">[What\'s Auto-Fill?]</a></span></td></tr>';
echo '<tr><td>Auto-Signup is: </td><td><select name="autoSignup">';
$onoff=explode(' ','on off');
for ($x=0; $x<2; $x++){
	echo '<option';
	if ($onoff[$x]==$info[7]) echo ' selected="selected"';
	echo ' value="'.$onoff[$x].'">'.strtoupper($onoff[$x]).'</option>';
}
echo '</select> <span class="mini"><a href="javascript:autosignup()">[What\'s Auto-Signup?]</a></span></td></tr>';
echo '<tr><td colspan="2" class="subtitle">Auto-Fill/Auto-Signup Options</td></tr>';
$query='SELECT * FROM `autosignup` WHERE `username`=\''.$username.'\'';
$data=mysql_query($query);
$info=mysql_fetch_row($data);
$temp=explode('<br>',$info[1]);
$tempy='';
for ($x=0; $x<sizeOf($temp); $x++){
	$tempy.=$temp[$x];
	if ($x<sizeOf($temp)-1) $tempy.=chr(13);
}
$info[1]=$tempy;
echo '<tr><td valign="top">Comment:</td><td><textarea name="comment" rows="4" cols="55">'.$info[1].'</textarea></td></tr>';
echo '<tr><td valign="top">Driving?</td><td><select name="driving">';
for ($x=0; $x<2; $x++){
	echo '<option';
	if ($yesno[$x]==$info[2]) echo ' selected="selected"';
	echo '>'.$yesno[$x].'</option>';
}
echo '</select></td></tr>';
echo '<tr><td valign="top">Stalk event?</td><td><select name="stalk">';
for ($x=0; $x<2; $x++){
	echo '<option';
	if ($yesno[$x]==$info[3]) echo ' selected="selected"';
	echo '>'.$yesno[$x].'</option>';
}
echo '</select></td></tr>';

echo '</table></p>';
echo '<p><input type="submit" name="submit" value="Edit Preferences"></p>';
echo '</form>';
mysql_close();
?>
</body>
</html>