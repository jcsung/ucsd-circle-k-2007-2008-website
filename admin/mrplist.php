<html>
<head>
<title>MRP List</title>
<link rel="stylesheet" type="text/css" href="../ucsdcki.css">
</head>
<body>
<?php
require('../utils.php');
$your_ip_address=GetHostByName($_SERVER['REMOTE_ADDR']);
amanda_session_garbage_collect();
$session_existence=amanda_session_exists($your_ip_address);
$editable=false;
if ($session_existence){
	$username=amanda_session_get_username($your_ip_address);
	$password=amanda_session_get_password($your_ip_address);
	$loginity=validateLogin($username,$password);
	if ($loginity){
		$secLevel=getInfo($username,$password,'secLevel');
		if ($secLevel>700) $editable=true;
	}
}
echo '<p class="title">The Uploaded MRP List</p><p><ul>';
$fnames=array();
$lnames=array();
$mrplocs=array();
if ($dh=opendir('../mrp/')){
	while(($file=readdir($dh))!=false){
		if (strstr($file,'.csv')){
			$temp=explode('.csv',$file);
			$file2=$temp[0];
			$temp=explode('_',$file2);
			array_push($fnames,$temp[1]);
			array_push($lnames,$temp[0]);
			array_push($mrplocs,$file);
		}
	}
	closedir($dh);
	for ($x=0; $x<sizeOf($fnames); $x++){
		for ($y=$x+1; $y<sizeOf($fnames); $y++){
			if (strcmp(strtolower($lnames[$x]),strtolower($lnames[$y]))>0){
				$temp=$fnames[$x];
				$fnames[$x]=$fnames[$y];
				$fnames[$y]=$temp;
				$temp=$lnames[$x];
				$lnames[$x]=$lnames[$y];
				$lnames[$y]=$temp;
				$temp=$mrplocs[$x];
				$mrplocs[$x]=$mrplocs[$y];
				$mrplocs[$y]=$temp;
			}
		}
	}
	for ($x=0; $x<sizeOf($fnames); $x++){
		echo '<li>';
		if ($editable) echo '<a href="./mrp.php?n='.encrypt($mrplocs[$x]).'">';
		echo $fnames[$x].' '.$lnames[$x];
		if ($editable) echo '</a>';

		echo '</li>';
	}
}
else{
	echo '<li class="error">Error: Directory not found.</li>';
}
echo '</ul></p>';
?>
</body>
</html>

