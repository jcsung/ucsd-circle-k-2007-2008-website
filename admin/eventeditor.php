<html>
<head>
<title>Event Editor</title>
<link rel="stylesheet" type="text/css" href="../ucsdcki.css">
</head>
<body>
<?php
	$dels=0;
	require('../utils.php');
	$your_ip_address=GetHostByName($_SERVER['REMOTE_ADDR']);
	amanda_session_garbage_collect();
	$session_existence=amanda_session_exists($your_ip_address);
	if ($session_existence){
		$username=amanda_session_get_username($your_ip_address);
		$password=amanda_session_get_password($your_ip_address);
		$secLevel=getInfo($username,$password,'secLevel');
		if ($secLevel>650)
			$editable=true;
		else
			$editable=false;
	}
	if (!$editable)
		die('<span class="error">You should not be here.</span>');

	$serial=trim($_GET['id']);
	while (strlen($serial)<6)
		$serial='0'.$serial;

	mysql_connect('localhost','circlek1_cki','cki') or die('<span class="error">Error: Unable to connect to server</span>');
	mysql_select_db('circlek1_events') or die('<span class="error">Error: Unable to connect to database</span>');
	$query='SELECT * FROM `eventlist` WHERE `serial`=\''.$serial.'\'';
	$data=mysql_query($query);
	if (mysql_num_rows($data)<1)
		die('<span class="error">Error: Event could not be found.<br><a href="javascript:history.back(1)">Back</a>');
	$info=mysql_fetch_row($data);

	$temp=$info[6];
	$cur=explode(' <br>',$temp);
	$temp='';
	for ($x=0; $x<sizeOf($cur); $x++){
		$temp.=$cur[$x];
	}
	$info[6]=$temp;
	$temp=$info[8];
	$cur=explode(' <br>',$temp);
	$temp='';
	for ($x=0; $x<sizeOf($cur); $x++){
		$temp.=$cur[$x];
	}	
	$info[8]=$temp;

	$tanhauser=$info;

	$temp=explode(',','Service,Social,Fundraiser,Kiwanis Family,Interclub,MD&E,District,Divisional,International,Administrative');
	if ($secLevel>900) array_push($temp,'Testing Purposes');
	$types='<table class="matt">';
	for ($x=0; $x<sizeOf($temp); $x++){ 
		$types.='<tr><td valign="middle" align="left"><input type="checkbox" name="types[\''.$temp[$x].'\']"';
		if (strstr($tanhauser[2],$temp[$x])) $types.=' checked="checked"';
		$types.='></td><td align="left" valign="middle">'.$temp[$x].'</td></tr>';
	}
	$types.='</table>';
	$today=$tanhauser[3];
	$this_month=getMonth($today,'num');
	$temp=explode(' ','January February March April May June July August September October November December');
	$cur=getMonth(currentTime(),'num');
	$date='<select name="date_m">';
	for ($x=0; $x<sizeOf($temp); $x++){
		$date.='<option value="'.($x+1).'"';
		if ($x+1==$this_month) $date.=' selected="selected"';
		$date.='>'.$temp[$x].'</option>';
	}
	$date.='</select>&nbsp;&nbsp;<select name="date_d">';
	$this_day=getDay($today);
	for ($x=1; $x<=31; $x++){
		$date.='<option';
		if ($x==$this_day) $date.=' selected="selected"';
		$date.='>'.$x.'</option>';
	}
	$date.='</select>&nbsp;,&nbsp;&nbsp;<select name="date_y">';
	$this_year=getYear($today);
	$is_within_range=false;
	for ($x=0; $x<3; $x++){
		$date.='<option';
		if (getYear(currentTime())+$x==$this_year){
			$date.=' selected="selected"';
			$is_within_range=true;
		}
		$date.='>'.(getYear(currentTime())+$x).'</option>';
	}
	if ($is_within_range===false)
		$date.='<option selected="selected">'.$this_year.'</option>';
	$date.='</select>';
	$time='<input type="text" name="time" value="'.$tanhauser[4].'" size="50">';
	$chairs='<input name="chair" type="text" value="'.$tanhauser[7].'" size="50">';
	$temp=explode(' ','Yes No');
	$allowsignups='<select name="allowsignups">';
	for ($x=0; $x<2; $x++){
		$allowsignups.='<option';
		if ($temp[$x]==$tanhauser[11])
			$allowsignups.=' selected="selected"';
		$allowsignups.='>'.$temp[$x].'</option>';
	}
	$allowsignups.='</select>';
	$pic='<input type="text" name="pic" size="50" value="'.$tanhauser[13].'">';
	$pic.='<br>URL should include "http://" in it.'; 
	$pic.='<br>Optimal picture size is 400x300 or some multiple thereof.';
	$redirect='<input type="text" name="redirect" size="50" value="'.$tanhauser[14].'">';
	$redirect.='<br>Leave this blank if you do not want the event page to redirect.';
	$redirect.='<br>If you do want this event page to redirect, please include http:// in the URL';
	$count=0;
	$signups='<table class="matt" width="100%" border="1">';
	$signups.='<tr><td colspan="6" class="bold" align="center">Signups</td></tr>';
	$signups.='<tr><td class="bold" align="center" width="25%">Name</td><td class="bold" align="center" width="25%">Comment</td><td class="bold" align="center" width="20%">Driving?</td><td class="bold" align="center" width="20%">Phone</td><td align="center" width="5%" class="bold error">X</td><td align="center" width="5%" class="error bold">X<br>2</td></tr>';
	$query='SELECT * FROM `'.$serial.'` WHERE `type`=\'Member Signup\' OR `type`=\'Guest Signup\' ORDER BY `time` ASC';
	$data=mysql_query($query);
	$yesnomaybeso=explode(' ','No Yes');
	for ($x=0; $info=mysql_fetch_row($data); $x++){
		for ($y=0; $y<sizeOf($info); $y++){
			if (trim($info[$y])==='') $info[$y]='&nbsp;';
		}
		$signups.='<input type="hidden" name="name'.$count.'" value="'.$info[0].'">';
		$signups.='<input type="hidden" name="time'.$count.'" value="'.$info[2].'">';
		$signups.='<tr>';
		$name=$info[0];
		if ($info[1]=='Member Signup'){
			mysql_select_db('circlek1_members') or die('<span class="error">Error: Unable to connect to database</span>');
			$tq='SELECT `fname`,`lname`,`phone` FROM `profiles` WHERE `username`=\''.$name.'\'';
			$td=mysql_query($tq);
			$ti=mysql_fetch_row($td);
			$name=$ti[0].' '.$ti[1];
			$info[6]=$ti[2];
			mysql_select_db('circlek1_events') or die('<span class="error">Error: Unable to connect to database</span>');
		}
		$signups.='<td width="30%" align="left" valign="top">'.$name.'</td>';
		$signups.='<td width="30%" align="left" valign="top">'.$info[3].'<br><textarea name="comment'.$count.'" cols="55" rows="4">'.$info[3].'</textarea></td>';
		$signups.='<td width="20%" align="left" valign="top">'.$info[4].'<br><select name="driving'.$count.'">';
		for ($y=0; $y<2; $y++){
			$signups.='<option';
			if($yesnomaybeso[$y]==$info[4]) $signups.=' selected="selected"';
			$signups.='>'.$yesnomaybeso[$y].'</option>';				
		}
		$signups.='</select></td>';
		$signups.='<td width="20%" align="left" valign="top">'.$info[6].'</td>';
		$signups.='<td width="5%"><input type="checkbox" name="delete'.$count.'"></td>';
		$signups.='<td width="5%"><input type="checkbox" name="deleteconf'.$count.'"></td>';
		$count++;
	}
	$query='SELECT * FROM `'.$serial.'` WHERE `type`=\'Member Waitlist\' OR `type`=\'Guest Waitlist\' ORDER BY `time` ASC';
	$data=mysql_query($query);
	if(mysql_num_rows($data)>0){
		$yesnomaybeso=explode(' ','No Yes');
		$signups.='<tr><td colspan="6" class="bold" align="center">Waitlist</td></tr>';
		$signups.='<tr><td class="bold" align="center" width="25%">Name</td><td class="bold" align="center" width="25%">Comment</td><td class="bold" align="center" width="20%">Driving?</td><td class="bold" align="center" width="20%">Phone</td><td align="center" width="5%" class="error bold">X</td><td align="center" width="5%" class="bold error">X<br>2</td></tr>';
		for ($x=0; $info=mysql_fetch_row($data); $x++){
			for ($y=0; $y<sizeOf($info); $y++){
				if (trim($info[$y])==='') $info[$y]='&nbsp;';
			}
			$signups.='<input type="hidden" name="name'.$count.'" value="'.$info[0].'">';
			$signups.='<input type="hidden" name="time'.$count.'" value="'.$info[2].'">';
			$signups.='<tr>';
			$name=$info[0];
			if ($info[1]=='Member Waitlist'){
				mysql_select_db('circlek1_members') or die('<span class="error">Error: Unable to connect to database</span>');
				$tq='SELECT `fname`,`lname`,`phone` FROM `profiles` WHERE `username`=\''.$name.'\'';
				$td=mysql_query($tq);
				$ti=mysql_fetch_row($td);
				$name=$ti[0].' '.$ti[1];
				$info[6]=$ti[2];
				mysql_select_db('circlek1_events') or die('<span class="error">Error: Unable to connect to database</span>');
			}
			$signups.='<td width="30%" align="left" valign="top">'.$name.'</td>';
			$signups.='<td width="30%" align="left" valign="top">'.$info[3].'<br><textarea name="comment'.$count.'" cols="55" rows="4">'.$info[3].'</textarea></td>';
			$signups.='<td width="20%" align="left" valign="top">'.$info[4].'<br><select name="driving'.$count.'">';
			for ($y=0; $y<2; $y++){
				$signups.='<option';
				if($yesnomaybeso[$y]==$info[4]) $signups.=' selected="selected"';
				$signups.='>'.$yesnomaybeso[$y].'</option>';				
			}
			$signups.='</select></td>';
			$signups.='<td width="20%" align="left" valign="top">'.$info[6].'</td>';
			$signups.='<td width="5%"><input type="checkbox" name="delete'.$count.'"></td>';
			$signups.='<td width="5%"><input type="checkbox" name="deleteconf'.$count.'"></td>';
			$count++;
		}
	}
	$signups.='<input type="hidden" name="length" value="'.$count.'">';
	$signups.='</table>';
	mysql_close();
?>
<p class="title" align="center">Event Editor</p>
<p><table class="matt">
<form action="eventeditorbackend.php" method="post">
<tr><td class="bold" align="left" valign="top">Name:</td><td align="left" valign="top"><input type="text" name="name" size="50" value="<?php echo $tanhauser[1]; ?>"></td></tr>
<tr><td class="bold" align="left" valign="top">Type(s):</td><td align="left" valign="top"><?php echo $types; ?></td></tr>
<tr><td class="bold" align="left" valign="top">Description:</td><td align="left" valign="top"><textarea name="info" rows="4" cols="55"><?php echo $tanhauser[6]; ?></textarea></td></tr>
<tr><td class="bold" align="left" valign="top">Date:</td><td align="left" valign="top"><?php echo $date; ?></td></tr>
<tr><td class="bold" align="left" valign="top">Time:</td><td align="left" valign="top"><?php echo $time; ?></td></tr>
<tr><td class="bold" align="left" valign="top">Location:</td><td align="left" valign="top"><input type="text" name="location" size="50" value="<?php echo $tanhauser[5]; ?>"></td></tr>
<tr><td class="bold" align="left" valign="top">Chair(s):</td><td align="left" valign="top"><?php echo $chairs; ?></td></tr>
<tr><td class="bold" align="left" valign="top">Notes:</td><td align="left" valign="top"><textarea name="notes" rows="4" cols="55"><?php echo $tanhauser[8]; ?></textarea></td></tr>
<tr><td class="bold" align="left" valign="top">Signup Cap:</td><td align="left" valign="top"><input type="text" name="signupcap" size="50" value="<?php echo $tanhauser[9]; ?>"> (For uncapped events, use 2147483647)</td></tr>
<tr><td class="bold" align="left" valign="top">Allow Signups?</td><td align="left" valign="top"><?php echo $allowsignups; ?></td></tr>
<tr><td class="bold" align="left" valign="top">Picture URL:</td><td align="left" valign="top"><?php echo $pic; ?></td></tr>
<tr><td class="bold" align="left" valign="top">Redirect?</td><td align="left" valign="top"><?php echo $redirect; ?></td></tr>
<input type="hidden" name="isFirstday" value="Yes">
<input type="hidden" name="deleted" value="<?php echo $tanhauser[12]; ?>">
<input type="hidden" name="serial" value="<?php echo $_GET['id']; ?>">
</table></p>
<p><?php echo $signups; ?></p>
<p><p><input type="hidden" name="verification" value="snarfblat"><input type="submit" value="Edit Event"></p>
<p><a href="../leah.php">Back to Admin Tools</p>
</form>
</body>
</html>