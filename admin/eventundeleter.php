<html>
<head>
<title>Event Undeleter</title>
<link rel="stylesheet" type="text/css" href="../ucsdcki.css">
</head>
<body>
<?php
	require('../utils.php');
	$your_ip_address=GetHostByName($_SERVER['REMOTE_ADDR']);
	amanda_session_garbage_collect();
	$session_existence=amanda_session_exists($your_ip_address);
	if ($session_existence){
		$username=amanda_session_get_username($your_ip_address);
		$password=amanda_session_get_password($your_ip_address);
		$secLevel=getInfo($username,$password,'secLevel');
		if ($secLevel>650)
			$editable=true;
		else
			$editable=false;
	}
	if (!$editable)
		die('<span class="error">You should not be here.</span>');
	$serial=htmlentities(trim($_GET['id']));
	mysql_connect('localhost','circlek1_cki','cki') or die('<span class="error">Error: Unable to connect to server</span>');
	mysql_select_db('circlek1_events') or die('<span class="error">Error: Unable to connect to database</span>');
	$query='UPDATE `eventlist` SET `deleted`=\'No\' WHERE `serial`=\''.$serial.'\'';
	//echo $query.'<br>';
	mysql_query($query);
	echo '<script language="Javascript">parent.upcoming.location.href = \'../upcoming.php\';</script>';

//begin email notification code
		$id=$serial;
		mysql_select_db('circlek1_events') or die('<span class="error">Error: Unable to connect to database</span>');
		$query='SELECT * FROM `eventlist` WHERE `serial`=\''.$id.'\'';
		//echo $query.'<br>';
		$data=mysql_query($query);
		$info=mysql_fetch_row($data);
		$event_name=$info[1];
		$query='SELECT * FROM `'.$id.'`';
		$data=mysql_query($query);
		while($info=mysql_fetch_row($data)){
			mysql_select_db('circlek1_members') or die('<span class="error">Error: Unable to connect to database</span>');
			$temp_name=$info[0];
			$temp_query='SELECT * FROM `profiles` WHERE `username`=\''.$temp_name.'\'';
			$temp_data=mysql_query($temp_query);
			$temp_info=mysql_fetch_row($temp_data);
			$to=$temp_info[5];
			$subj='[UCSD Circle K] Event Undeletion: '.$event_name;
			$msg='Hello '.$temp_info[2].','.chr(13).'Good news!  An administrator has undeleted event "'.$event_name.'!"  Your signup information and spot on the list are as they were before the event\'s deletion.'.chr(13).chr(13).'--UCSD Circle K'.chr(13).chr(13);
			//echo $to.'<br>'.$subj.'<br>'.$msg.'<br>';
			mail($to,$subj,$msg,'From: UCSD Circle K <circlek@ucsd.edu>');
			mysql_select_db('circlek1_events') or die('<span class="error">Error: Unable to connect to database</span>');
		}
//


?>
	<p class="Title">Event Undeleter</p>
	<p>The event has been undeleted.</p>
	<p>Click <a href="eventdeleter.php?id=<?php echo $_GET['id']; ?>">here</a> to undo.</p>
	<p>Click <a href="../events/view.php?id=<?php echo $_GET['id']; ?>">here</a> to go back to the event.</p>
	<p>Click <a href="eventundeletion.php">here</a> to go to the Event Undeleter page.</p>
	<p>Click <a href="../leah.php">here</a> to return to Admin Tools.</p>
<?php
	mysql_select_db('circlek1_members') or die('<span class="error">Error: Unable to connect to database</span>');
	$query='SELECT `fname`,`lname` FROM `profiles` WHERE `username`=\''.$username.'\'';
	$data=mysql_query($query);
	$info=mysql_fetch_row($data);
	$name=$info[0].' '.$info[1].' ('.$username.')';
	mysql_select_db('circlek1_logs') or die('<span class="error">Error: Unable to connect to database</span>');
	$query='INSERT INTO `admin` (`username`,`action`,`detail`,`time`,`ip`) VALUES(\'';
	$query.=$name.'\',\'';
	$query.='UNDELETED EVENT'.'\',\'';
	$query.='<a href="../events/view.php?id='.$serial.'">'.$serial.'</a>\',\'';
	$query.=currentTime().'\',\'';
	$query.=$your_ip_address.'\')';
	mysql_query($query);
	mysql_close();
?>
</body>
</html>