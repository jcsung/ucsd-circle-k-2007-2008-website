<html>
<head>
<title>Event Editor</title>
<link rel="stylesheet" type="text/css" href="../ucsdcki.css">
</head>
<body>
<?php
echo '<p class="title">Event Editor</p>';
echo '<p>Click on an event to edit it.</p>';
require('../utils.php');
function invalidMon($data){
	$possibleMonths=explode(' ','January February March April May June July August September October November December');
	$flag=true;
	for ($x=0; $x<12; $x++){
		//echo '"'.strtolower(htmlentities(trim($data))).'" =?= "'.strtolower($possibleMonths[$x]).'"<br>';
		if (strtolower(htmlentities(trim($data)))==strtolower($possibleMonths[$x])){
			$flag=false;
			break;
		}
	}
	if (!$data) $flag=true;
	return $flag;
}
function invalidYear($data){
	$flag=false;
	for ($x=0; $x<strlen($data); $x++){
		$temp=substr($data,$x,1);
		$flag2=true;
		for ($y=0; $y<10; $y++){
			if ($temp==$y){
				$flag2=false;
				break;
			}
		}
		if ($flag2){
			$flag=true;
			break;
		}
	}
	if (!$data) $flag=true;
	return $flag;
}

$your_ip_address=GetHostByName($_SERVER['REMOTE_ADDR']);
$thecurrenttime=currentTime();
amanda_session_garbage_collect();
$session_existence=amanda_session_exists($your_ip_address);
if ($session_existence){
	$username=amanda_session_get_username($your_ip_address);
	$password=amanda_session_get_password($your_ip_address);
	$loginity=validateLogin($username,$password);
}
$humanoid=htmlentities(trim($_GET['humanoid']));
$view_month=htmlentities(trim($_GET['month']));
$view_year=htmlentities(trim($_GET['year']));
//if (strtolower(htmlentities($username))!=strtolower($humanoid)) die('<span class="error">Error: Information does not match</span>');
//if ($loginity) $url_append='&humanoid='.$username;
//else $url_append='';
$today=currentTime();
$this_month=getMonth($today,'text');
$this_year=getYear($today);
$possibleMonths=explode(' ','January February March April May June July August September October November December');
$daysOfWeek=explode(' ','Sunday Monday Tuesday Wednesday Thursday Friday Saturday');
if (invalidMon($view_month)) $view_month=$this_month;
if (invalidYear($view_year)) $view_year=$this_year;
$view_month_i=0;
for ($x=0; $x<12; $x++){
	if (strtolower($view_month)==strtolower($possibleMonths[$x])){
		$view_month_i=$x;
		break;
	}
}
$month_days=explode(' ','31 28 31 30 31 30 31 31 30 31 30 31');
$isLeapYear=isLeapYear(mktime(0,0,0,1,1,$view_year));
if ($isLeapYear) $month_days[1]++;
//echo ''.$view_month.' of '.$view_year.'<Br>';
echo '<p></p>';
echo '<p align="center" class="title">'.$view_month.'</p>';
echo '<p align="center"><table border="1">';
echo '<tr>';
for ($x=0; $x<7; $x++) echo '<td align="center" class="bold" width="14%">'.$daysOfWeek[$x].'</td>';
echo '</tr>';
$view_month_array=getdate(mktime(0,0,0,$view_month_i+1,1,$view_year));
$y=1-$view_month_array['wday'];
mysql_connect('localhost','circlek1_cki','cki') or die('<span class="error">Error: Unable to connect to server</span>');
mysql_select_db('circlek1_events') or die('<span class="error">Error: Unable to connect to database</span>');
for ($x=0; $x<42; $x++){
	if ($x%7==0)
		echo '<tr valign="top">';
	echo '<td height="100" width="14%">';
	if ($y>0&&$y<=$month_days[$view_month_i]){
		echo '<p class="bold">'.$y.'</p>';
		$currentDayStart=mktime(0,0,0,$view_month_i+1,$y,$view_year);
		$currentDayEnd=getDayEnd($currentDayStart);
		$query='SELECT * FROM `eventlist` WHERE `date`>=\''.$currentDayStart.'\' AND `date`<=\''.$currentDayEnd.'\'';
		//echo $query.'<br>';
		$data=mysql_query($query);
		$len=mysql_num_rows($data);
		if ($len>0)
			echo '<table class="mini">';
		while ($info=mysql_fetch_row($data)){
			$e_name=$info[1];
			$id=$info[0];
			echo '<tr><td align="left" valign="top" class="mini"><a href="eventeditor.php?id='.$id.'">'.$e_name.'</a></td></tr>';
		}
		if ($len>0)
			echo '</table>';	
	}
	else{
		echo '&nbsp';
	}
	$y++;
	echo '</td>';
	if ($x%7==6)
		echo '</tr>';
//	if ($y>$month_days[$view_month_i]&&$x>=34) break;
}
mysql_close();
echo '</table></p>';
if ($view_month_i>0){
	$prev_month=$possibleMonths[$view_month_i-1];
	$prev_year=$view_year;
}
else{
	$prev_month=$possibleMonths[11];
	$prev_year=$view_year--;
}
if ($view_month_i<11){
	$next_month=$possibleMonths[$view_month_i+1];
	$next_year=$view_year;
}
else{
	$prev_month=$possibleMonths[0];
	$next_year=$view_year++;
}
echo '<p align="center"><a href="eventediting.php?month='.$prev_month.'&year='.$prev_year.$url_append.'">[Previous Month]</a> <a href="eventediting.php?month='.$this_month.'&year='.$this_year.$url_append.'">[This Month]</a> <a href="eventediting.php?month='.$next_month.'&year='.$next_year.$url_append.'">[Next Month]</a></p>';
echo '<p><a href="../leah.php">Back to Admin Tools</a></p>';
?>
</body>
</html>