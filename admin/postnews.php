<html>
<head>
<title>Post News</title>
<link rel="stylesheet" type="text/css" href="../ucsdcki.css">
</head>
<body>
<p class="title">Post News</p>
<?php
	require('../utils.php');
	$your_ip_address=GetHostByName($_SERVER['REMOTE_ADDR']);
	amanda_session_garbage_collect();
	$session_existence=amanda_session_exists($your_ip_address);
	if ($session_existence){
		$username=amanda_session_get_username($your_ip_address);
		$password=amanda_session_get_password($your_ip_address);
		$secLevel=getInfo($username,$password,'secLevel');
		if ($secLevel>650)
			$editable=true;
		else
			$editable=false;
	}
	if (!$editable)
		die('<span class="error">You should not be here.</span>');
?>
	<p align="center">Be careful, there is no News Editor.</p>
	<p align="center"><form action="postnewsbackend.php" method="post">
	<textarea name="news" cols="55" rows="10"></textarea><br><br>
	<input type="hidden" name="verification" value="snarfblat"><input type="submit" value="Post"></form></p>
	<p align="center"><a href="javascript:window.close()">[Close Window]</a></p>
</body>
</html>