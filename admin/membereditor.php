<html>
<head>
<title>Member Editor</title>
<link rel="stylesheet" type="text/css" href="../ucsdcki.css">
</head>
<body>
<p class="title">Member Editor</p>
<?php
	require('../utils.php');
	$your_ip_address=GetHostByName($_SERVER['REMOTE_ADDR']);
	amanda_session_garbage_collect();
	$session_existence=amanda_session_exists($your_ip_address);
	if ($session_existence){
		$username=amanda_session_get_username($your_ip_address);
		$password=amanda_session_get_password($your_ip_address);
		$secLevel=getInfo($username,$password,'secLevel');
		if ($secLevel>650)
			$editable=true;
		else
			$editable=false;
	}
	if (!$editable)
		die('<span class="error">You should not be here.</span>');

	echo '<p align="center">You may change the security level and MRP location of any member with a lower security level than you.</p>';
	if ($secLevel==899)
		echo '<p align="center">You have been identified as the Secretary.  You may view MRP locations for every member, but may only change the security levels of those with lower security levels than you (and may not change your own).</p>';
	echo '<p align="center"><a href="javascript:window.location.reload()">[Refresh]</a><br><a href="../leah.php">Back to Admin Tools</a></p>';
	mysql_connect('localhost','circlek1_cki','cki') or die('<span class="error">Error: Unable to connect to server</span>');
	mysql_select_db('circlek1_members') or die('<span class="error">Error: Unable to connect to database</span>');
	$query='SELECT `username`,`fname`,`mname`,`lname`,`mrp` FROM `profiles` ORDER BY `lname`,`fname`';
	$data=mysql_query($query);
	$pos_sec_lvls=explode(' ','0 150 250 350 450 550 625 675 750 850 950');
	$pos_sec_lvls_min=explode(' ','0 101 201 301 401 501 601 651 701 801 901');
	$pos_sec_lvls_max=explode(' ','100 200 300 400 500 600 650 700 800 900 1000');
	$pos_sec_lvls_desc=explode(',','Banned User,Guest,Confirmed Nonmember,Unconfirmed Member/Nonmember,Confirmed Member,Non-UCSD Circle K Person of Importance,Committee Member,Beta Tester,Board Member,Executive Board Member,Website Administrator');
	echo '<p><form action="membereditorbackend.php" method="post"><table class="matt" border="1">';
	echo '<tr><td class="bold" align="center">Username</td><td class="bold" align="center">Name</td><td class="bold" align="center">Security Level</td><td class="bold" align="center">MRP</td><td class="bold" align="center">Edit?</td><td class="bold" align="center">Confirm</td></tr>';
	for ($y=0; $info=mysql_fetch_row($data); $y++){
		$buf='<tr>';
		$tdata=mysql_query('SELECT `secLevel` FROM `memberlist` WHERE `username`=\''.$info[0].'\'');
		$tinfo=mysql_fetch_row($tdata);
		$tSecLevel=$tinfo[0];
		$buf.='<td align="center">'.$info[0].'</td>';
		$temporary_name=$info[1].' '.$info[2].' '.$info[3];
		$buf.='<td align="center">'.$temporary_name.'<input type="hidden" name="names['.$info[0].']" value="'.$temporary_name.'"></td>';
		$buf.='<td align="center">';
		$zomg='<select name="secLevel['.$info[0].']">';
		for ($x=0; $x<sizeOf($pos_sec_lvls); $x++){
			if ($pos_sec_lvls[$x]>$secLevel) break;
//			$flag=false;
			$num=3;
			for ($z=0; $z<sizeOf($pos_sec_lvls); $z++){
				if ($tSecLevel>=$pos_sec_lvls_min[$z]&&$tSecLevel<=$pos_sec_lvls_max[$z]){
//					$flag=true;
					$num=$z;
					break;
				}
			}
			$zomg.='<option value="';
			if ($tSecLevel>=$pos_sec_lvls_min[$x]&&$tSecLevel<=$pos_sec_lvls_max[$x]) $zomg.=$tSecLevel.'" selected="selected"';
			else $zomg.=$pos_sec_lvls[$x].'"';
			$zomg.='>'.$pos_sec_lvls_desc[$x].'</option>'.chr(13);
		}
		$zomg.='</select>';
		if ($secLevel==899&&($tSecLevel>=$secLevel||$tSecLevel>=850))
			$zomg='<input type="hidden" name="secLevel['.$info[0].']" value="'.$tSecLevel.'">(Uneditable)';
		$buf.=$zomg;
		$buf.='</td>';
		$buf.='<td align="center">';
		$buf.='<input type="text" name="mrp['.$info[0].']" value="'.$info[4].'">';
		$buf.='</td>';
		$buf.='<td align="center"><input type="checkbox" name="confl1['.$y.']"></td>';
		$buf.='<td align="center"><input type="checkbox" name="conf2['.$y.']"></td>';
		$buf.='</tr>';
		//echo '<tr><td>'.$secLevel.' '.$tSecLevel.'</td></tr>';
		if ($secLevel>$tSecLevel||$username==$info[0]||$secLevel==899) //899 belongs to the secretary and the secretary ALONE.
			echo $buf;
	}
	echo '<input type="hidden" name="length" value="'.$y.'">';
	echo '</table><input type="hidden" name="verification" value="snarfblat"><input type="submit" align="center" class="center" value="Edit"></form></span></p>';
?>
	<p><a href="../leah.php">Back to Admin Tools</a></p>
</body>
</html>