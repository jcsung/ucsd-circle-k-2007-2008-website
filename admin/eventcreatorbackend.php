<html>
<head>
<title>Event Creator Backend</title>
<link rel="stylesheet" type="text/css" href="../ucsdcki.css">
</head>
<body>
<p class="title">Creating Event...</p>
<?php
	$valid=true;
	$msg='<p>You did not enter the event ';
	require('../utils.php');
	$your_ip_address=GetHostByName($_SERVER['REMOTE_ADDR']);
	amanda_session_garbage_collect();
	$session_existence=amanda_session_exists($your_ip_address);
	if ($session_existence){
		$username=amanda_session_get_username($your_ip_address);
		$password=amanda_session_get_password($your_ip_address);
		$secLevel=getInfo($username,$password,'secLevel');
		if ($secLevel>650)
			$editable=true;
		else
			$editable=false;
	}
	if (!$editable||$_POST['verification']!='snarfblat')
		die('<span class="error">You should not be here.</span>');
	mysql_connect('localhost','circlek1_cki','cki') or die('<span class="error">Error: Unable to connect to server</span>');
	mysql_select_db('circlek1_events') or die('<span class="error">Error: Unable to connect to database</span>');
	$query='SELECT * FROM `eventlist` ORDER BY `serial` DESC';
	$data=mysql_query($query);
	$info=mysql_fetch_row($data);
	$serial=$info[0];
	$serial++;
	//echo $serial.'<br>';
	if (serial==1000000)
		die('<span class="error">Error: Maximum serial number reached.  Event cannot currently be created.</span>');
	while (strlen($serial)<6)
		$serial='0'.$serial;
	$_POST['serial']=$serial;
	if ($_POST['signupcap']<0) $_POST['signupcap']='2147483647';
	$temp=explode(chr(13),$_POST['info']);
	$_POST['info']='';
	for ($x=0; $x<sizeOf($temp); $x++){
		$_POST['info'].=$temp[$x];
		if ($x<sizeOf($temp)-1) $_POST['info'].=' <br> ';
	}
	$temp=explode(chr(13),$_POST['notes']);
	$_POST['notes']='';
	for ($x=0; $x<sizeOf($temp); $x++){
		$_POST['notes'].=$temp[$x];
		if ($x<sizeOf($temp)-1) $_POST['notes'].=' <br> ';
	}
	$types=$_POST['types'];
	if (!is_array($types)){
		$valid=false;
		$msg.=' types.';
	}
	else{
		$keys=array_keys($types);
		$_POST['type']='';
		for ($x=0; $x<sizeOf($keys); $x++){
			//$keys[$x]=stripslashes($keys[$x]);
			if ($types[$keys[$x]]=='on'){
				$temp=explode('\'',stripslashes($keys[$x]));
				for ($y=0; $y<sizeOf($temp); $y++)
					$_POST['type'].=$temp[$y];
				$_POST['type'].=',';
			}
		}
		$_POST['type']=substr($_POST['type'],0,strlen($_POST['type'])-1);
	}
	$_POST['date']=mktime(0,0,0,$_POST['date_m'],$_POST['date_d'],$_POST['date_y']);
	$temp=explode(' ','start stop');
	$temp2=explode(' ','hour min meridian');
	$temp3=explode(' ',': &nbsp;');
	$_POST['time']='';
	for ($x=0; $x<2; $x++){
		for ($y=0; $y<3; $y++){
			$_POST['time'].=$_POST['time_'.$temp[$x].'_'.$temp2[$y].''].$temp3[$y];
		}
		if ($x==0) $_POST['time'].=' TO ';
	}
	$chairs=$_POST['chairs'];
	if(!is_array($chairs)){
		if ($valid) $msg.='chairs.';
		$valid=false;		
	}
	else{
		$keys=array_keys($chairs);
		$temp='';
		$temp2=sizeOf($keys);
		for ($x=0; $x<$temp2; $x++){
			$cur=explode('\'',stripslashes($keys[$x]));
			for ($y=0; $y<sizeOf($cur); $y++)
				$temp.=$cur[$y];
			if ($temp2>2&&($x<$temp2-1||!(trim($_POST['chair_add']==='')))) $temp.=',';
			$temp.=' ';
			if ($temp2>1&&$x==$temp2-2&&trim($_POST['chair_add'])==='') $temp.='and ';
		}
		if (!(trim($_POST['chair_add'])==='')) $temp.=$_POST['chair_add'];
		$_POST['chair']=$temp;
	}
	//print_r($_POST); echo '<br>';

	$keys=explode(' ','serial name type date time location info chair notes signupcap isFirstday allowsignups deleted pic redirect');
	$check=explode(' ','no yes yes no no no yes yes no yes no yes no no no');
	$desc=explode(',','placeholder,name,type(s),placeholder,placeholder,placeholder,description,chair(s),placeholder,signup cap,placeholder,placeholder,placeholder,placeholder,placeholder');
	for ($x=0; $x<sizeOf($keys); $x++){
		if(trim($_POST[$keys[$x]])===''&&$check[$x]=='yes'){
			$valid=false;
			$msg.=$desc[$x].'.';
			break;
		}
	}
	$msg.=' Please go back (preserving event input data) and fix this.</p>';
	if ($valid){
			$hack_check='';
			$query='INSERT INTO `eventlist` (';
			for ($x=0; $x<sizeOf($keys); $x++){
				$query.='`'.$keys[$x].'`';
				if ($x<sizeOf($keys)-1) $query.=',';
			}
			$query.=') VALUES(';
			for ($x=0; $x<sizeOf($keys); $x++){
				$hack_check.=$_POST[$keys[$x]];
				$query.='\''.$_POST[$keys[$x]].'\'';
				if ($x<sizeOf($keys)-1) $query.=',';
			}	
			$query.=')';
			//echo $query.'<br>';
			//if (strpos($hack_check,'ALTER TABLE')===false&&strpos($hack_check,'ALTER DATABASE')===false&&strpos($hack_check,'CREATE TABLE')===false&&strpos($hack_check,'CREATE DATABASE')===false&&strpos($hack_check,'DROP TABLE')===false&&strpos($hack_check,'DROP DATABASE')===false&&strpos($hack_check,'RENAME TABLE')===false&&(strpos($hack_check,'DELETE')===false&&strpos($hack_check,'FROM')===false)&&
			mysql_query($query);
			$success=mysql_affected_rows();
			$query='CREATE TABLE `'.$serial.'` (`username` text NOT NULL,`type` enum(\'Member Signup\',\'Guest Signup\',\'Member Waitlist\',\'Guest Waitlist\') NOT NULL default \'Guest Signup\',`time` text NOT NULL,`comment` longtext NOT NULL,`driving` enum(\'No\',\'Yes\') NOT NULL default \'No\',`stalk` enum(\'No\',\'Yes\') NOT NULL default \'No\',`phone` text NOT NULL) ENGINE=MyISAM;';
			mysql_query($query);
			if ($success>0){
				echo '<p>The event has been created successfully.  Click <a href="../events/view.php?id='.$serial.'" target="_blank">here</a> to view it.</p>';
			}
			else{
				echo '<p>The event could not be created.  Go back (preserving the event input data) to try again.</p>';
			}
	}
	else
		echo $msg;

	mysql_select_db('circlek1_members') or die('<span class="error">Error: Unable to connect to database</span>');
	$query='SELECT `fname`,`lname` FROM `profiles` WHERE `username`=\''.$username.'\'';
	$data=mysql_query($query);
	$info=mysql_fetch_row($data);
	$name=$info[0].' '.$info[1].' ('.$username.')';
	mysql_select_db('circlek1_logs') or die('<span class="error">Error: Unable to connect to database</span>');
	$query='INSERT INTO `admin` (`username`,`action`,`detail`,`time`,`ip`) VALUES(\'';
	$query.=$name.'\',\'';
	$query.='CREATED EVENT'.'\',\'';
	$query.='<a href="../events/view.php?id='.$serial.'">'.$serial.'</a>\',\'';
	$query.=currentTime().'\',\'';
	$query.=$your_ip_address.'\')';
	mysql_query($query);

	echo '<p><a href="javascript:history.back(1)">Back to Event Creator (Preserving event input Data)</a></p>';
	echo '<p><a href="eventcreator.php">Back to Event Creator (Without preserving event input data)</a></p>';
	echo '<p><a href="../leah.php">Back to Admin Tools</a></p>';
	mysql_close();
?>
</body>
</html>