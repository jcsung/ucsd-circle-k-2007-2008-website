<html>
<head>
<title>Edit Next Meeting Frame</title>
<link rel="stylesheet" type="text/css" href="../ucsdcki.css">
</head>
<body>
<p class="title">Next Meeting Location Editor</p>
<p><form action="editnextmeetingbackend.php" method="post">
<?php
require('../utils.php');
$your_ip_address=GetHostByName($_SERVER['REMOTE_ADDR']);
amanda_session_garbage_collect();
$session_existence=amanda_session_exists($your_ip_address);
if ($session_existence){
	$username=amanda_session_get_username($your_ip_address);
	$password=amanda_session_get_password($your_ip_address);
	$secLevel=getInfo($username,$password,'secLevel');
	if ($secLevel>650)
		$editable=true;
	else
		$editable=false;
}
if (!$editable)
	die('<span class="error">You should not be here.</span>');

$months=explode(' ','January February March April May June July August September October November December');

$which=explode(' ','premeeting meeting');
$which_name=explode(',','Pre-meeting Dinner,Meeting');

mysql_connect('localhost','circlek1_cki','cki') or die('<span class="error">Error: Unable to connect to server</span>');
mysql_select_db('circlek1_misc') or die('<span class="error">Error: Unable to connect to database</span>');
$query='SELECT * FROM `havenextmeeting`';
$data=mysql_query($query);
$info=mysql_fetch_assoc($data);
if ($info['break']=='No') $have_next_mtg='Yes';
else $have_next_mtg='No';
$old=array();
for ($x=0; $x<sizeOf($which); $x++){
	$query='SELECT * FROM `meetinglocation` WHERE `serial`=\''.$which[$x].'\'';
	$data=mysql_query($query);
	$info=mysql_fetch_row($data);
	$has[$which[$x]]=$info[5];
	$query2='SHOW COLUMNS FROM `meetinglocation`';
	$data2=mysql_query($query2);
	for ($y=0; $info2=mysql_fetch_row($data2); $y++){
		$old[$which[$x].'_'.$info2[0]]=$info[$y];
	}
}
mysql_close();
$old['month']=getMonth($old['meeting_date'],'num');
$old['day']=getDay($old['meeting_date']);
$old['year']=getYear($old['meeting_date']);
for ($x=0; $x<sizeOf($which); $x++){
	$temp=getHour($old[$which[$x].'_date'],'military');
	if ($temp==0){
		$old[$which[$x].'_hour']=12;
		$old[$which[$x].'_meridian']='AM';
	}
	else if ($temp==12){
		$old[$which[$x].'_meridian']='PM';
	}
	else if ($temp>12){
		$old[$which[$x].'_hour']=$temp-12;
		$old[$which[$x].'_meridian']='PM';
	}
	else{
		$old[$which[$x].'_meridian']='AM';
	}
	$old[$which[$x].'_minute']=getMinutes($old[$which[$x].'_date']);
}
//print_r($old);
$date='<select name="month">';
for ($y=0; $y<12; $y++){
	$date.='<option value="'.($y+1).'"';
	if ($old['month']==$y+1) $date.=' selected="selected"';
	$date.='>'.$months[$y].'</option>';
}
$date.='</select>&nbsp;&nbsp;<select name="day">';
for ($y=1; $y<=31; $y++){
	$date.='<option';
	if ($old['day']==$y) $date.=' selected="selected"';
	$date.='>'.$y.'</option>';
}
$date.='</select>&nbsp;&nbsp;<select name="year">';
for ($y=0; $y<=1; $y++){
	$date.='<option';
	if ($old['year']==$y+getYear(currentTime())) $date.=' selected="selected"';
	$date.='>'.($y+getYear(currentTime())).'</option>';
}
$date.='</select>';
$yesno=explode(' ','Yes No');
?>
<p>
<span class="subtitle">General</span>
<br>Is the next meeting happening? <select name="havenext"><?php for ($x=0; $x<sizeOf($yesno); $x++){ echo '<option'; if ($have_next_mtg==$yesno[$x]) echo ' selected="selected"'; echo '>'.$yesno[$x].'</option>';  } ?></select>
<br>If so, when is it?  <?php echo $date; ?>
<br>If not, why?  <select name="donthavenext">
<?php
$abc=explode(' ','Winter Spring Summer');
for ($x=0; $x<3; $x++) echo '<option value='.$abc[$x].'>It is '.$abc[$x].' Break</option>';
?>
<option value="Cancelled">The meeting's been cancelled *slap*</option>
<option>Other</option>
</select>
<br>If "other" was selected, please add a custom message: <input type="text" name="donthavenextother">
</p>
<?php
$merids=explode(' ','AM PM');
for ($x=0; $x<sizeOf($which); $x++){
	echo '<input type="hidden" name="'.$which[$x].'_type" value="'.$which_name[$x].'">';
	echo '<input type="hidden" name="'.$which[$x].'_month" value="'.$old['month'].'">';
	echo '<input type="hidden" name="'.$which[$x].'_day" value="'.$old['day'].'">';
	echo '<input type="hidden" name="'.$which[$x].'_year" value="'.$old['year'].'">';
	$time='<select name="'.$which[$x].'_hour">.';
	for ($y=1; $y<=12; $y++){
		$time.='<option';
		if ($y==$old[$which[$x].'_hour']) $time.=' selected="selected"';
		$time.='>'.$y.'</option>';
	}
	$time.='</select> : <select name="'.$which[$x].'_minute">.';
	for ($y=0; $y<=59; $y++){
		$time.='<option';
		if ($y==$old[$which[$x].'_minute']) $time.=' selected="selected"';
		$time.='>'.($y<10?'0':'').$y.'</option>';
	}
	$time.='</select> : <select name="'.$which[$x].'_meridian">.';
	for ($y=0; $y<2; $y++){
		$time.='<option';
		if ($merids[$y]==$old[$which[$x].'_meridian']) $time.=' selected="selected"';
		$time.='>'.$merids[$y].'</option>';
	}
	$time.='</select>';
	echo '<p>';
	echo '<span class="subtitle">'.$which_name[$x].'</span>';
	echo '<br>Is there a '.$which_name[$x].'? <select name="'.$which[$x].'_have">';
	for ($y=0; $y<sizeOf($yesno); $y++){ echo '<option'; if ($has[$which[$x]]==$yesno[$y]) echo ' selected="selected"'; echo '>'.$yesno[$y].'</option>';  }  
	echo '</select>';
	echo '<br>If not, you can skip to the next section.';
	echo '<br>What time will it be? '.$time;
	echo '<br>Where will it be? <input name="'.$which[$x].'_location" value="'.$old[$which[$x].'_location'].'" type="text">';
	echo '<br>Should a map to the location be displayed? <select name="'.$which[$x].'_hasMap"><option>Yes</option><option>No</option></select>';
	//echo '<br>Map URL (leave blank if you want AMANDA to automatically generate the map URL): <input type=text onClick=select() name="'.$which[$x].'_map" value="'.$old[$which[$x].'_map'].'">';
	echo '<br>You may enter a note here about the next meeting: <br><textarea cols="55" rows="4" name="'.$which[$x].'_note">'.$old[$which[$x].'_note'].'</textarea>';
	echo '</p>';
	echo '<input type="hidden" name="which['.$x.']" value="'.$which[$x].'">';
}
echo '<input type="hidden" name="verification" value="snarfblat">';
?>

<p><input type="submit" value="Change"></p>
</form></p>
</body>
</html>