<html>
<head>
<title>Event Creator Backend</title>
<link rel="stylesheet" type="text/css" href="../ucsdcki.css">
</head>
<body>
<p class="title">Editing Event...</p>
<?php
	$dels=0;
	$valid=true;
	$msg='<p>You did not enter the event ';
	require('../utils.php');
	$your_ip_address=GetHostByName($_SERVER['REMOTE_ADDR']);
	amanda_session_garbage_collect();
	$session_existence=amanda_session_exists($your_ip_address);
	if ($session_existence){
		$username=amanda_session_get_username($your_ip_address);
		$password=amanda_session_get_password($your_ip_address);
		$secLevel=getInfo($username,$password,'secLevel');
		if ($secLevel>650)
			$editable=true;
		else
			$editable=false;
	}
	if (!$editable||$_POST['verification']!='snarfblat')
		die('<span class="error">You should not be here.</span>');
	mysql_connect('localhost','circlek1_cki','cki') or die('<span class="error">Error: Unable to connect to server</span>');
	mysql_select_db('circlek1_events') or die('<span class="error">Error: Unable to connect to database</span>');
	$serial=trim($_POST['serial']);
	while (strlen($serial)<6) 
		$serial='0'.$serial;
	//echo $serial.'<br>';
	$_POST['serial']=$serial;
	if ($_POST['signupcap']<0) $_POST['signupcap']='2147483647';
	$temp=explode(chr(13),$_POST['info']);
	$_POST['info']='';
	for ($x=0; $x<sizeOf($temp); $x++){
		$_POST['info'].=$temp[$x];
		if ($x<sizeOf($temp)-1) $_POST['info'].=' <br> ';
	}
	$temp=explode(chr(13),$_POST['notes']);
	$_POST['notes']='';
	for ($x=0; $x<sizeOf($temp); $x++){
		$_POST['notes'].=$temp[$x];
		if ($x<sizeOf($temp)-1) $_POST['notes'].=' <br> ';
	}
	$types=$_POST['types'];
	if (!is_array($types)){
		$valid=false;
		$msg.=' types.';
	}
	else{
		$keys=array_keys($types);
		$_POST['type']='';
		for ($x=0; $x<sizeOf($keys); $x++){
			//$keys[$x]=$keys[$x];
			if ($types[$keys[$x]]=='on'){
				$temp=explode('\'',stripslashes($keys[$x]));
				for ($y=0; $y<sizeOf($temp); $y++)
					$_POST['type'].=$temp[$y];
				$_POST['type'].=',';
			}
		}
		$_POST['type']=substr($_POST['type'],0,strlen($_POST['type'])-1);
	}
	$_POST['date']=mktime(0,0,0,$_POST['date_m'],$_POST['date_d'],$_POST['date_y']);
	
//	print_r($_POST); echo '<br>';
	$event_name=$_POST['name'];
	$keys=explode(' ','serial name type date time location info chair notes signupcap isFirstday allowsignups deleted pic redirect');
	$check=explode(' ','no yes yes no no no yes yes no yes no yes no no no');
	$desc=explode(',','placeholder,name,type(s),placeholder,placeholder,placeholder,description,chair(s),placeholder,signup cap,placeholder,placeholder,placeholder,placeholder,placeholder');
	for ($x=0; $x<sizeOf($keys); $x++){
		if(trim($_POST[$keys[$x]])===''&&$check[$x]=='yes'){
			$valid=false;
			$msg.=$desc[$x].'.';
			break;
		}
	}
	$msg.=' Please go back and fix this.</p>';
	if ($valid){
			$hack_check='';
			$query='UPDATE `eventlist` SET ';
			for ($x=0; $x<sizeOf($keys); $x++){
				$hack_check.=$_POST[$keys[$x]];
				$query.='`'.$keys[$x].'`='.'\''.$_POST[$keys[$x]].'\'';
				if ($x<sizeOf($keys)-1) $query.=',';
			}	
			$query.=' WHERE `serial`=\''.$serial.'\'';
			//echo $query.'<br>';
			//if (strpos($hack_check,'ALTER TABLE')===false&&strpos($hack_check,'ALTER DATABASE')===false&&strpos($hack_check,'CREATE TABLE')===false&&strpos($hack_check,'CREATE DATABASE')===false&&strpos($hack_check,'DROP TABLE')===false&&strpos($hack_check,'DROP DATABASE')===false&&strpos($hack_check,'RENAME TABLE')===false&&(strpos($hack_check,'DELETE')===false&&strpos($hack_check,'FROM')===false)&&
			mysql_query($query);
			$dels+=mysql_affected_rows();
			//echo mysql_affected_rows().' rows affected.<br>';
	}
	else
		echo $msg;
	$max=$_POST['length'];
	for ($x=0; $x<$max; $x++){
		$query='UPDATE `'.$serial.'` SET `comment`=\''.$_POST['comment'.$x].'\',`driving`=\''.$_POST['driving'.$x].'\' WHERE `username`=\''.$_POST['name'.$x].'\' AND `time`=\''.$_POST['time'.$x].'\'';
		//echo $query.'<br>';
		mysql_query($query);
		$dels+=mysql_affected_rows();
	}

	
	$id=$serial;
	//begin email notification code
		mysql_select_db('circlek1_events') or die('<span class="error">Error: Unable to connect to database</span>');
		$query='SELECT * FROM `eventlist` WHERE `serial`=\''.$id.'\'';
		//echo $query.'<br>';
		$data=mysql_query($query);
		$info=mysql_fetch_row($data);
		$event_name=$info[1];
		$query='SELECT * FROM `'.$id.'` WHERE `stalk`=\'Yes\'';
		$data=mysql_query($query);
		if ($action='SIGNED UP FOR') $action='signup';
		else $action='waitlist';
		while($info=mysql_fetch_row($data)){
			mysql_select_db('circlek1_members') or die('<span class="error">Error: Unable to connect to database</span>');
			$temp_name=$info[0];
			$temp_query='SELECT * FROM `profiles` WHERE `username`=\''.$temp_name.'\'';
			$temp_data=mysql_query($temp_query);
			$temp_info=mysql_fetch_row($temp_data);
			$to=$temp_info[5];
			$subj='[UCSD Circle K] Event Stalking: '.$event_name;
			$msg='Hello '.$temp_info[2].','.chr(13).'An administrator has edited event "'.$event_name.'."'.chr(13).chr(13).'--UCSD Circle K'.chr(13).chr(13).'You are receiving this notification email because you are stalking the event "'.$event_name.'."  To stop receiving these emails, edit your signup for this event, and set "Stalk Event?" to "No."';
			//echo $to.'<br>'.$subj.'<br>'.$msg.'<br>';
			if ($del>0)
				mail($to,$subj,$msg,'From: UCSD Circle K <circlek@ucsd.edu>');
			mysql_select_db('circlek1_events') or die('<span class="error">Error: Unable to connect to database</span>');
		}

		mysql_select_db('circlek1_events') or die('<span class="error">Error: Unable to connect to database</span>');		
		$query='SELECT * FROM `'.$id.'` WHERE `type`=\'Member Signup\' OR `type`=\'Member Waitlist\'';
		$data=mysql_query($query);
		for ($x=0; $info=mysql_fetch_row($data); $x++){
			$toemail[$x]=$info[0];
			//echo $toemail[$x].'<br>';
		}
		mysql_select_db('circlek1_members') or die('<span class="error">Error: Unable to connect to database</span>');
		for ($x=0; $x<sizeOf($toemail); $x++){
			$query='SELECT * FROM `options` WHERE `username`=\''.$toemail[$x].'\'';
			$data=mysql_query($query);
			$info=mysql_fetch_row($data);
			if ($info[6]=='Yes'){
				$temp_query='SELECT * FROM `profiles` WHERE `username`=\''.$toemail[$x].'\'';
				$temp_data=mysql_query($temp_query);
				$temp_info=mysql_fetch_row($temp_data);
				$to=$temp_info[5];
				$subj='[UCSD Circle K] Event Editing Notification: '.$event_name;
				$msg='Hello '.$temp_info[2].','.chr(13).'An administrator has edited event "'.$event_name.'."'.chr(13).chr(13).'--UCSD Circle K'.chr(13).chr(13).'You are receiving this notification email because you set "Email me if event information is changed on my signed-up events" to "Yes."  To change this, log in and go to "Edit Preferences" in the login page.';
				//echo $to.'<br>'.$subj.'<br>'.$msg.'<br>';
				if ($dels>0)
					mail($to,$subj,$msg,'From: UCSD Circle K <circlek@ucsd.edu>');
			}
		}
	//end notification code

	$sql_fields=explode(' ','username type time comment driving stalk phone');
	$sql_fields2=explode(' ','name time');
	
	//begin admin signup removal
	mysql_select_db('circlek1_events') or die('<span class="error">Error: Unable to connect to database</span>');
	for ($x=0; $x<$max; $x++){
		if ($_POST['delete'.$x]=='on'&&$_POST['deleteconf'.$x]=='on'){
			$query='DELETE FROM `'.$serial.'` WHERE `username`=\''.$_POST['name'.$x].'\' AND `time`=\''.$_POST['time'.$x].'\' ';
			//echo $query.'<br>';
			mysql_query($query);
		}
	}
	//end admin signup removal

	//begin waitlist updating
	mysql_select_db('circlek1_events') or die('<span class="error">Error: Unable to connect to database</span>');
	$currentcap=$_POST['signupcap'];
	$query='SELECT * FROM `'.$serial.'` WHERE `type`=\'Member Signup\' OR `type`=\'Guest Signup\' ORDER BY `time` ASC';
	$data=mysql_query($query);
	$cur_num_signups=mysql_num_rows($data);
	if ($cur_num_signups<$currentcap){

		$query='SELECT * FROM `'.$serial.'` WHERE `type`=\'Member Waitlist\' OR `type`=\'Guest Waitlist\' ORDER BY `time` ASC';
		$data=mysql_query($query);
		while ($info=mysql_fetch_row($data)){
			$o_info=$info;
			if ($info[1]=='Member Waitlist') $info[1]='Member Signup';
			else $info[1]='Guest Signup';
			$tq='UPDATE `'.$serial.'` SET ';
			for ($x=0; $x<sizeOf($sql_fields); $x++){
				$tq.='`'.$sql_fields[$x].'`=\''.addslashes($info[$x]).'\'';
				if ($x<sizeOf($sql_fields)-1) $tq.=',';
			}
			$tq.=' WHERE ';
			for ($x=0; $x<sizeOf($sql_fields); $x++){
				$tq.='`'.$sql_fields[$x].'`=\''.addslashes($o_info[$x]).'\'';
				if ($x<sizeOf($sql_fields)-1) $tq.=' AND ';
			}
			//echo $tq.'<br>';
			mysql_query($tq);
			//echo '<b>'.mysql_affected_rows().' rows affected</b><br>';
			if ($o_info[1]=='Member Waitlist'){
				mysql_select_db('circlek1_members') or die('<span class="error">Error: Unable to connect to database</span>');
				$t_q='SELECT `email`,`fname`,`lname` FROM `profiles` WHERE `username`=\''.$info[0].'\'';
				$t_d=mysql_query($t_q);
				$t_i=mysql_fetch_row($t_d);
				$to=$t_i[0];
				//echo '<p><h1>hi</h1></p>';
				$subj='[UCSD Circle K]You are now signed up for '.$event_name.'!';
				$msg='Hello '.$t_i[1].' '.$t_i[2].','.chr(13).'Either an unsignup has occured or the event cap has been changed for "'.$event_name.'," so you are now signed up for it!  Hurray!  See you there!'.chr(13).chr(13).'--UCSD Circle K';
				mail($to,$subj,$msg,'From: UCSD Circle K <circlek@ucsd.edu>');		
				mysql_select_db('circlek1_events') or die('<span class="error">Error: Unable to connect to database</span>');
			}
			$cur_num_signups++;
			if ($cur_num_signups==$currentcap) break;
		}

	}
	else if ($cur_num_signups>$currentcap){
		$query='SELECT * FROM `'.$serial.'` WHERE `type`=\'Member Signup\' OR `type`=\'Guest Signup\' ORDER BY `time` DESC';
		$data=mysql_query($query);
		while ($info=mysql_fetch_row($data)){
			$o_info=$info;
			if ($info[1]=='Member Signup') $info[1]='Member Waitlist';
			else $info[1]='Guest Waitlist';
			$tq='UPDATE `'.$serial.'` SET ';
			for ($x=0; $x<sizeOf($sql_fields); $x++){
				$tq.='`'.$sql_fields[$x].'`=\''.addslashes($info[$x]).'\'';
				if ($x<sizeOf($sql_fields)-1) $tq.=',';
			}
			$tq.=' WHERE ';
			for ($x=0; $x<sizeOf($sql_fields); $x++){
				$tq.='`'.$sql_fields[$x].'`=\''.addslashes($o_info[$x]).'\'';
				if ($x<sizeOf($sql_fields)-1) $tq.=' AND ';
			}
			//echo $tq.'<br>';
			mysql_query($tq);
			//echo '<b>'.mysql_affected_rows().' rows affected</b><br>';
			if ($o_info[1]=='Member Signup'){
				mysql_select_db('circlek1_members') or die('<span class="error">Error: Unable to connect to database</span>');
				$t_q='SELECT `email`,`fname`,`lname` FROM `profiles` WHERE `username`=\''.$info[0].'\'';
				$t_d=mysql_query($t_q);
				$t_i=mysql_fetch_row($t_d);
				$to=$t_i[0];
				//echo '<p><h1>hi</h1></p>';
				$subj='[UCSD Circle K]You have been put on the waitlist for '.$event_name;
				$msg='Hello '.$t_i[1].' '.$t_i[2].','.chr(13).'Unfortunately, due to extenuating circumstances, the cap for "'.$event_name.'" has been decreased.  As a result of this, you have been pushed onto the waitlist.  We are sorry this happened, but we cannot find enough rides for everyone.  Please check the event signups or your email periodically, as someone may unsign up and pull you back up onto the signup list.'.chr(13).chr(13).'--UCSD Circle K';
				mail($to,$subj,$msg,'From: UCSD Circle K <circlek@ucsd.edu>');		
				mysql_select_db('circlek1_events') or die('<span class="error">Error: Unable to connect to database</span>');
			}
			$cur_num_signups--;
			if ($cur_num_signups==$currentcap) break;
		}
	}
	//end waitlist updating

	mysql_select_db('circlek1_members') or die('<span class="error">Error: Unable to connect to database</span>');
	$query='SELECT `fname`,`lname` FROM `profiles` WHERE `username`=\''.$username.'\'';
	$data=mysql_query($query);
	$info=mysql_fetch_row($data);
	$name=$info[0].' '.$info[1].' ('.$username.')';
	mysql_select_db('circlek1_logs') or die('<span class="error">Error: Unable to connect to database</span>');
	$query='INSERT INTO `admin` (`username`,`action`,`detail`,`time`,`ip`) VALUES(\'';
	$query.=$name.'\',\'';
	$query.='EDITED EVENT'.'\',\'';
	$query.='<a href="../events/view.php?id='.$serial.'">'.$serial.'</a>\',\'';
	$query.=currentTime().'\',\'';
	$query.=$your_ip_address.'\')';
	if ($dels>0)
		mysql_query($query);

	echo '<p>Event Edited.</p>';
	echo '<p><a href="javascript:history.back(1)">Back</a></p>';
	echo '<p><a href="../events/view.php?id='.$serial.'">View Event</a></p>';
	echo '<p><a href="eventediting.php">Back to Event Editor</a></p>';
	echo '<p><a href="../leah.php">Back to Admin Tools</a></p>';
	mysql_close();
?>
</body>
</html>