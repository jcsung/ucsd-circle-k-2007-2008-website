<html>
<head>
<title>Log Viewer</title>
<link rel="stylesheet" type="text/css" href="../ucsdcki.css">
</head>
<body>
<?php
	require('../utils.php');
	$your_ip_address=GetHostByName($_SERVER['REMOTE_ADDR']);
	amanda_session_garbage_collect();
	$session_existence=amanda_session_exists($your_ip_address);
	if ($session_existence){
		$username=amanda_session_get_username($your_ip_address);
		$password=amanda_session_get_password($your_ip_address);
		$secLevel=getInfo($username,$password,'secLevel');
		if ($secLevel>650)
			$editable=true;
		else
			$editable=false;
	}
	if (!$editable)
		die('<span class="error">You should not be here.</span>');
	$which=explode(',','Signup Log,Login Log,Contact Page Log');
	if ($secLevel>900) array_push($which,'Admin Log');
	$toc='<ol>';
	for ($x=0; $x<sizeOf($which); $x++){
		$temp=explode(' ',strtolower($which[$x]));
		$toc.='<li><a href="#'.$temp[0].'">'.$which[$x].'</a></li>';
	}
	$toc.='<li><a href="javascript:window.location.reload()">Refresh</a></li>';
	$toc.='<li><a href="../leah.php">Back to Admin Tools</a></li>';
	$toc.='</ol>';
mysql_connect('localhost','circlek1_cki','cki') or die('<span class="error">Error: Unable to connect to server</span>');
mysql_select_db('circlek1_logs') or die('<span class="error">Error: Unable to connect to database</span>');
?>
<p class="title">Log Viewer<a name="top"></a></p>
<p><?php echo $toc; ?></p>
<p><h1 class="subtitle"><a name="signup"></a>Signup Log</h1><table width="100%" class="matt" border="1">
<?php
$query='SELECT * FROM `signup` ORDER BY `time` DESC';
$data=mysql_query($query);
for ($y=0; $info=mysql_fetch_row($data); $y++){
	if ($y%10==0) echo '<tr><td colspan="7" align="center"><table class="matt" width="100%"><tr><td width="25%" target="left"><a href="#top">Back to Top</a></td><td width="50%" align="center"><a href="../leah.php">Back to Admin Tools</a></td><td width="25%" align="right"><a href="javascript:window.location.reload()">Refresh</a></td></tr></table></td></tr>';
	echo '<tr>';
	for ($x=0; $x<sizeOf($info); $x++){
		if ($x==3)
			echo '<td>ON</td>';
		else if ($x==4){
			echo '<td>FROM</td>';
			if(!$info[4]) $info[4]='<span class="italic">Unlogged</span>';
		}
		echo '<td>';
		echo ($x==3?strtoupper(date('D m/j/y h:i:s A',$info[3])):$info[$x]);
		echo '</td>';
	}	
	echo '</tr>';
}
?>
<tr><td colspan="7" align="center"><table class="matt" width="100%"><tr><td width="25%" target="left"><a href="#top">Back to Top</a></td><td width="50%" align="center"><a href="../leah.php">Back to Admin Tools</a></td><td width="25%" align="right"><a href="javascript:window.location.reload()">Refresh</a></td></tr></table></td></tr></table></p>

<p><h1 class="subtitle"><a name="login"></a>Login Log</h1><table width="100%" class="matt" border="1">
<?php
$query='SELECT * FROM `login` ORDER BY `time` DESC';
$data=mysql_query($query);
for ($y=0; $info=mysql_fetch_row($data); $y++){
	if ($y%10==0) echo '<tr><td colspan="6" align="center"><table class="matt" width="100%"><tr><td width="25%" target="left"><a href="#top">Back to Top</a></td><td width="50%" align="center"><a href="../leah.php">Back to Admin Tools</a></td><td width="25%" align="right"><a href="javascript:window.location.reload()">Refresh</a></td></tr></table></td></tr>';
	echo '<tr>';
	for ($x=0; $x<sizeOf($info); $x++){
		if ($x==2) echo '<td>ON</td>';
		else if ($x==3){
			echo '<td>FROM</td>';
			if (!$info[3]) $info[3]='<span class="italic">Unlogged</span>';
		}
		echo '<td>';
		echo ($x==2?strtoupper(date('D m/j/y h:i:s A',$info[$x])):$info[$x]);
		echo '</td>';
	}	
	echo '</tr>';
}
?>
<tr><td colspan="6" align="center"><table class="matt" width="100%"><tr><td width="25%" target="left"><a href="#top">Back to Top</a></td><td width="50%" align="center"><a href="../leah.php">Back to Admin Tools</a></td><td width="25%" align="right"><a href="javascript:window.location.reload()">Refresh</a></td></tr></table></td></tr></table></p>

<p><h1 class="subtitle"><a name="contact"></a>Contact Page Log</h1><table width="100%" class="matt" border="1">
<?php
$query='SELECT * FROM `contact` ORDER BY `date` DESC';
$data=mysql_query($query);
for ($y=0; $info=mysql_fetch_row($data); $y++){
	if ($y%10==0) echo '<tr><td colspan="7" align="center"><table class="matt" width="100%"><tr><td width="25%" target="left"><a href="#top">Back to Top</a></td><td width="50%" align="center"><a href="../leah.php">Back to Admin Tools</a></td><td width="25%" align="right"><a href="javascript:window.location.reload()">Refresh</a></td></tr></table></td></tr>';
	echo '<tr>';
	for ($x=0; $x<sizeOf($info); $x++){
		if ($x==0) echo '<td>ON</td>';
		else if ($x==2) echo '<td>CONTACTED</td>';
		else if ($x==3){
			echo '<td>FROM</td>';
			if(!$info[3]) $info[3]='<span class="italic">Unlogged</span>';
		}
		echo '<td>';
		echo ($x==0?strtoupper(date('D m/j/y h:i:s A',$info[$x])):$info[$x]);
		echo '</td>';
	}	
	echo '</tr>';
}
?>
<tr><td colspan="7" align="center"><table class="matt" width="100%"><tr><td width="25%" target="left"><a href="#top">Back to Top</a></td><td width="50%" align="center"><a href="../leah.php">Back to Admin Tools</a></td><td width="25%" align="right"><a href="javascript:window.location.reload()">Refresh</a></td></tr></table></td></tr></table></p>
<?php
if ($secLevel>900){
?>

<p><h1 class="subtitle"><a name="admin"></a>Admin Log</h1><table width="100%" class="matt" border="1">
<?php
$query='SELECT * FROM `admin` ORDER BY `time` DESC';
$data=mysql_query($query);
for ($y=0; $info=mysql_fetch_row($data); $y++){
	if ($y%10==0) echo '<tr><td colspan="7" align="center"><table class="matt" width="100%"><tr><td width="25%" target="left"><a href="#top">Back to Top</a></td><td width="50%" align="center"><a href="../leah.php">Back to Admin Tools</a></td><td width="25%" align="right"><a href="javascript:window.location.reload()">Refresh</a></td></tr></table></td></tr>';
	echo '<tr>';
	for ($x=0; $x<sizeOf($info); $x++){
		if ($x==3) echo '<td>ON</td>';
		else if ($x==4){
			echo '<td>FROM</td>';
			if (!$info[4]) $info[4]='<span class="italic">Unlogged</span>';
		}
		echo '<td>';
		echo ($x==3?strtoupper(date('D m/j/y h:i:s A',$info[$x])):$info[$x]);
		echo '</td>';
	}	
	echo '</tr>';
}
?>
<tr><td colspan="7" align="center"><table class="matt" width="100%"><tr><td width="25%" target="left"><a href="#top">Back to Top</a></td><td width="50%" align="center"><a href="../leah.php">Back to Admin Tools</a></td><td width="25%" align="right"><a href="javascript:window.location.reload()">Refresh</a></td></tr></table></td></tr></table></p>

<?php
}
mysql_close();
?>
</body>
</html>