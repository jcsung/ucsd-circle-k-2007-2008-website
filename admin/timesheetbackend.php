<html>
<head>
<title>Generate Timesheet</title>
<link rel="stylesheet" type="text/css" href="../ucsdcki.css">
<style>
body{
	background-color: white;
	color: black;
	background-image: none;
}
.title{
	color: black;
}
</style>
</head>
<body>
<?php
require('../utils.php');
$your_ip_address=GetHostByName($_SERVER['REMOTE_ADDR']);
$id=trim($_GET['id']);
amanda_session_garbage_collect();
$session_existence=amanda_session_exists($your_ip_address);
if ($session_existence){
	$username=amanda_session_get_username($your_ip_address);
	$password=amanda_session_get_password($your_ip_address);
	$loginity=validateLogin($username,$password);
	if ($loginity){
		$type='member';
		$secLevel=getInfo($username,$password,'secLevel');
		if ($secLevel>700){
			$editable=true;
		}
		else{
			$editable=false;
		}
	}	
}
if (!$editable) die('<span class="error">You should not be here.  <a href="javascript:history.back(1)">Back</a></span>');
mysql_connect('localhost','circlek1_cki','cki') or die('<span class="error">Error: Unable to connect to server</span>');
mysql_select_db('circlek1_events') or die('<span class="error">Error: Unable to connect to database</span>');
$query='SELECT * FROM `eventlist` WHERE `serial`=\''.$id.'\'';
$data=mysql_query($query);
$info=mysql_fetch_row($data);
echo '<p align="center"><span class="title">'.$info[1].' Timesheet</span><br>'.date('l, F j, Y',$info[3]).'</p>';

$query='SELECT * FROM `'.$id.'` WHERE (`type`=\'Member Signup\' OR `type`=\'Guest Signup\') ORDER BY `time` ASC';
//echo $query.'<br>';
$data=mysql_query($query);
echo '<p><table class="matt" border="1" width="100%"><tr><td colspan="4" class="subtitle" align="center">Signups</td></tr><tr><td width="25%" class="bold" align="center">Name</td><td width="25%" class="bold" align="center">Phone</td><td width="25%" class="bold" align="center">Time In?</td><td width="25%" class="bold" align="center">Time Out?</td></tr>';
for ($lcv=1; $info=mysql_fetch_row($data); $lcv++){
	$temp_username=$info[0];
	$temp_type=$info[1];
	$temp_name=$temp_username;
	$temp_phone=$info[6];
	if ($temp_type=='Member Signup'){
		if (strtolower(trim($temp_username))==strtolower(trim($username))) $is_signed_up=true;
//		echo '"'.strtolower(trim($temp_username)).'" =?= "'.strtolower(trim($username)).'"<br>';
		mysql_select_db('circlek1_members') or die('<span class="error">Error: Database could not be found.</span>');
		$temp_query='SELECT * FROM `profiles` WHERE `username`=\''.$temp_username.'\'';
		$temp_data=mysql_query($temp_query);
		$temp_info=mysql_fetch_row($temp_data);
		$temp_name=$temp_info[2].' '.$temp_info[4];
		$temp_phone=$temp_info[6];
		mysql_select_db('circlek1_events') or die('<span class="error">Error: Database could not be found.</span>');
	}
	echo '<tr><td valign="top">'.$temp_name.'<td>'.$temp_phone.'</td><td>&nbsp;</td><td>&nbsp;</td></tr>';
}
if ($_POST['waitlisttoo']=='on'){
	$query='SELECT * FROM `'.$id.'` WHERE (`type`=\'Member Waitlist\' OR `type`=\'Guest Waitlist\') ORDER BY `time` ASC';
	//echo $query.'<br>';
	$data=mysql_query($query);
	if (mysql_num_rows($data)>0)
		echo '<tr><td colspan="4" class="subtitle" align="center">Waitlists</td></tr>';
	for ($lcv=1; $info=mysql_fetch_row($data); $lcv++){
		$temp_username=$info[0];
		$temp_type=$info[1];
		$temp_name=$temp_username;
		$temp_phone=$info[6];
		if ($temp_type=='Member Waitlist'){
			if (strtolower(trim($temp_username))==strtolower(trim($username))) $is_signed_up=true;
	//		echo '"'.strtolower(trim($temp_username)).'" =?= "'.strtolower(trim($username)).'"<br>';
			mysql_select_db('circlek1_members') or die('<span class="error">Error: Database could not be found.</span>');
			$temp_query='SELECT * FROM `profiles` WHERE `username`=\''.$temp_username.'\'';
			$temp_data=mysql_query($temp_query);
			$temp_info=mysql_fetch_row($temp_data);
			$temp_name=$temp_info[2].' '.$temp_info[4];
			$temp_phone=$temp_info[6];
			mysql_select_db('circlek1_events') or die('<span class="error">Error: Database could not be found.</span>');
		}
		echo '<tr><td valign="top">'.$temp_name.'<td>'.$temp_phone.'</td><td>&nbsp;</td><td>&nbsp;</td></tr>';
	}

}
$max=$_POST['num']>5?$_POST['num']:5;
for ($x=0; $x<$max; $x++){
	echo '<tr>';
	for ($lcv=0; $lcv<4; $lcv++)
		echo '<td>&nbsp;</td>';
	echo '</tr>';
}

echo '</table></p>';
?>
</body>
</html>