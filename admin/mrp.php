<html>
<head>
<title>Anybody's MRP</title>
<link rel="stylesheet" type="text/css" href="../ucsdcki.css">
</head>
<body>
<?php
require('../utils.php');
$your_ip_address=GetHostByName($_SERVER['REMOTE_ADDR']);
amanda_session_garbage_collect();
$session_existence=amanda_session_exists($your_ip_address);
$editable=false;
if ($session_existence){
	$username=amanda_session_get_username($your_ip_address);
	$password=amanda_session_get_password($your_ip_address);
	$loginity=validateLogin($username,$password);
	if ($loginity){
		$secLevel=getInfo($username,$password,'secLevel');
		if ($secLevel>700) $editable=true;
	}
}
if (!$editable) die('<span class="error">504 Forbidden.</span>');
$person=htmlentities(trim($_GET['humanoid']));

if(0==1&&strtolower($username)!=strtolower($person)){
	echo '<span class="error">Error: Information does not match.</span>';
}
else{
	$file='../mrp/'.decrypt($_GET['n']);
	$temp=explode('.csv',decrypt($_GET['n']));
	$temp=explode('_',$temp[0]);
	$thepersonsname='';
	for ($x=1; $x<sizeOf($temp); $x++)
		$thepersonsname.=$temp[$x].' ';
	$thepersonsname.=$temp[0];
	if (!strstr($file,'.csv')) $file.='.csv';
	//echo $file.'<br>';
	//echo $file.'<br>';
	if (!file_exists($file)){
		echo '<p>Invalid MRP file.</p>';
	}
	else{
		$NUM_COLS=13;
		$rows=explode(chr(13),file_get_contents($file));
		$events='<table class="matt" align="center" border="1">';
		for ($x=0; $x<sizeOf($rows); $x++){
			$num_spaces=0;
			$cols=explode(',',$rows[$x]);
			$buffer='<tr>';
			for ($y=0; $y<$NUM_COLS; $y++){
				$buffer.='<td valign="top"';
				if ($x==0) $buffer.=' align="center" class="bold"';
				else if ($x==1) $buffer.=' align="right"';
				else if ($y==2) $buffer.=' align="right"';
				else if ($y>2) $buffer.=' align="center"';
				$buffer.='>';
				if (trim($cols[$y])){
					$buffer.=$cols[$y];
				}
				else{
					$buffer.='&nbsp;';
					$num_spaces++;
				}

				$buffer.='</td>';
			}
			$buffer.='</tr>';
			//if ($num_spaces==$NUM_COLS) break;
			if ($num_spaces!=$NUM_COLS)
				$events.=$buffer;
		}
		$events.='</table>';

		$NUM_ROWS=6;
		
		$summary='<table class="matt" align="center" border="1">';
		for ($x=0; $x<$NUM_ROWS; $x++){
			$temp='';
			$flag=false;
			for ($y=0; $y<strlen($rows[$x]); $y++){
				$charAt=substr($rows[$x],$y,1);
				if ($charAt=='"'){
					$flag=!$flag;
					continue;
				}
				if ($charAt==','&&$flag) $charAt="&#44;";
				$temp.=$charAt;
			}
			$rows[$x]=$temp;
			$cols=explode(',',$rows[$x]);
			$summary.='<tr>';
			for ($y=$NUM_COLS+1; $y<sizeOf($cols); $y++){
				if ($x>1&&$x<5&&($y==($NUM_COLS+1)+7||$y==($NUM_COLS+1)+5||$y==($NUM_COLS+1)+3)) continue;
				if ($x==$NUM_ROWS-1&&trim($cols[$y])){
					$temp='';
					$temp2=explode(' ',$cols[$y]);
					for ($z=0; $z<sizeOf($temp2); $z++)
						$temp.=$temp2[$z];
					//echo $temp.'<br>';
					$temp='<img src="../mrp/'.strtolower($temp).'.png" alt="" border="0">';
					/*
					for ($z=0; $z<strlen($cols[$y]); $z++){
						$temp.=substr($cols[$y],$z,1);
						if ($z<strlen($cols[$y])-1) $temp.='<br>';
					}
					*/
					$cols[$y]=$temp;
				}


				$flag=false;
				$summary.='<td valign="';
				if($x==5) $summary.='top';
				else $summary.='middle';
				$summary.='"';
				if ($x==0&&$y==($NUM_COLS+1)+1) $summary.=' colspan="2"';
				if ($x==1&&($y==($NUM_COLS+1)+7||$y==($NUM_COLS+1)+5||$y==($NUM_COLS+1)+3)) $summary.=' rowspan="4"';
				$summary.='>';
				$summary.=(!(trim($cols[$y])==='')?$cols[$y]:'&nbsp;');
				$summary.='</td>';
				if ($x==0&&$y==($NUM_COLS+1)+1) $y++;

			}
			$summary.='</tr>';
		}
		$summary.='</table>';
		
		$other_abbreviations='<table class="matt" width="100%" border="1">';
		$ab_o=explode(' ','DSI AD');
		$ab_oe=explode(',','District Service Inititaive,Administrative');
		for ($x=0; $x<sizeOf($ab_o); $x++){
			$other_abbreviations.='<tr><td align="center" valign="middle">'.$ab_o[$x].'</td><td align="center" valign="middle">'.$ab_oe[$x].'</td></tr>';
		}
		$other_abbreviations.='</table></p>';

		$ab=explode(' ','CE KF IN MD FR SE DV DE INT OT');
		$abe=explode(',','Chair Event,K-Family,Interclub,Membership Development and Education,Fundraising,Social Event,Divisional Event,District Event,International Event,Other');
		$abe2=explode('|',"If you chaired the event|Attendance at any Kiwanis Family event (Kiwanis, Key Club, KIWIN�S, Aktion, Builders Club, K-Kids, or Circle K) in which there are at least TWO members present from your Circle K club and at least two members present from the other Kiwanis Family Organization.|Attendance at any Circle K or Kiwanis Family Event in which there are at least FOUR members present from our Circle K club and at least four California members present from the other Kiwanis Family Organization|These are events which help in the retention, development, and education of new and old members.|An event where money is raised for the club or for a charitable entity.|Any type of fun event with other members from the Kiwanis family.|These are events that are put on by your LTG which your whole division is invited to participate in.|These are events that are put on by the District Board.|These are events that are put on by the International Board.|Anything that doesn't fall into the previously-mentioned categories.  This can include: ".$other_abbreviations);
		
		$abbrevs='<table class="matt" border="1" width="100%">';
		$abbrevs.='<tr><td colspan="4" align="center" class="bold" valign="middle">List of Abbreviations</td></tr>';
		for ($x=0; $x<sizeOf($ab); $x++){
			if ($x%2==0) $abbrevs.='<tr>';
			$abbrevs.='<td align="center" valign="middle" width="10%">'.$ab[$x].'</td><td align="center" valign="middle" width="40%"><p class="underline">'.$abe[$x].'</p><p>'.$abe2[$x].'</p></td>';
			if ($x%2==1) $abbrevs.='</tr>';
		}
		$abbrevs.='</table>';

		echo '<p class="title" align="center">'.$thepersonsname.'\'s<a name="top"> </a>MRP</p>'.chr(13);
		echo '<p align="center">Version 1.2</p>';
		echo '<p align="center">Last Updated on '.date('F j, Y',filemtime($file)).' at '.date('g:i A',filemtime($file)).'</p>'.chr(13);
		echo '<p><ol><li><a href="#overall">Overall MRP Summary</a></li><li><a href="#abbrevs">List of Abbreviations</a></li><li><a href="#hours">Service Hour Summary</a></li></ol></p>';
		echo '<p align="center" class="subtitle">Overall<a name="overall"> </a>MRP Summary</p><p align="center">'.$summary.'<br><a href="#top">Back to top</a><br></p>'.chr(13);
		echo '<p align="center"><a name="abbrevs"></a>'.$abbrevs.'</p><p align="center" class="subtitle"><a name="hours"></a>Hours Summary</p><p align="center">'.$events.'<br><a href="#top">Back to top</a><br></p>'.chr(13);
		
	}
	
}			
?>
</body>
</html>

