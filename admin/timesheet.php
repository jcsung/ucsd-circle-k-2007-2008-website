<html>
<head>
<title>Generate Timesheet</title>
<link rel="stylesheet" type="text/css" href="../ucsdcki.css">
</head>
<body>
<?php
require('../utils.php');
$your_ip_address=GetHostByName($_SERVER['REMOTE_ADDR']);
$id=trim($_GET['id']);
amanda_session_garbage_collect();
$session_existence=amanda_session_exists($your_ip_address);
if ($session_existence){
	$username=amanda_session_get_username($your_ip_address);
	$password=amanda_session_get_password($your_ip_address);
	$loginity=validateLogin($username,$password);
	if ($loginity){
		$type='member';
		$secLevel=getInfo($username,$password,'secLevel');
		if ($secLevel>700){
			$editable=true;
		}
		else{
			$editable=false;
		}
	}	
}
if (!$editable) die('<span class="error">You should not be here.  <a href="javascript:history.back(1)">Back</a></span>');
?>
<form action="timesheetbackend.php?id=<?php echo $_GET['id']; ?>" method="post">
<p class="title">Generate Timesheet<br>
<br>How many blank rows? <input type="text" name="num" value="10"><br>
<br><input name="waitlisttoo" type="checkbox"> Include Waitlists?<br>
<br><input type="submit" value="Generate">
</p>
</form>
</body>
</html>