<html>
<head>
<title>Upcoming Birthdays</title>
<link rel="stylesheet" type="text/css" href="../ucsdcki.css">
</head>
<body>
<p class="title">Upcoming Birthdays</p>
<?php
	require('../utils.php');
	$your_ip_address=GetHostByName($_SERVER['REMOTE_ADDR']);
	amanda_session_garbage_collect();
	$session_existence=amanda_session_exists($your_ip_address);
	if ($session_existence){
		$username=amanda_session_get_username($your_ip_address);
		$password=amanda_session_get_password($your_ip_address);
		$secLevel=getInfo($username,$password,'secLevel');
		if ($secLevel>650)
			$editable=true;
		else
			$editable=false;
	}
	if (!$editable)
		die('<span class="error">You should not be here.</span>');
	mysql_connect('localhost','circlek1_cki','cki') or die('<span class="error">Error: Unable to connect to server</span>');
	mysql_select_db('circlek1_members') or die('<span class="error">Error: Unable to connect to database</span>');
	$query='SELECT `fname`,`lname`,`dob` FROM `profiles`';
	$data=mysql_query($query);
	$temp=currentTime();
	$start=getDayBeginning($temp);
	$end=getDayEnd($start+86400*7);
	$names=array();
	$bday=array();
	$age=array();
	$cur_mon=getMonth($temp,'num');
	while ($info=mysql_fetch_row($data)){
		$mon=getMonth($info[2],'num');
		$year=getYear($temp);
		if ($mon>$cur_mon) $year++;
		$remapped_bday=mktime(0,0,0,$mon,getDay($info[2]),$year);
		if ($remapped_bday>=$start&&$remapped_bday<=$end){
			array_push($names,$info[0].' '.$info[1]);
			array_push($bday,$info[2]);
			array_push($age,$year-getYear($info[2]));
		}
	}
	for ($x=$start; $x<=$end; $x+=86400){
		echo '<p align="right"><span class="bold">'.date('F j, Y',$x).'</span>';
		$cur_mon=getMonth($x,'num');
		$cur_day=getDay($x);
		for ($y=0; $y<sizeOf($bday); $y++){
			$mon=getMonth($bday[$y],'num');
			$day=getDay($bday[$y]);
			if ($mon==$cur_mon&&$day==$cur_day) echo '<br>'.$names[$y].' ('.intval($age[$y]).')';
		}
		echo '</p>';
	}
	mysql_close();
?>
<p align="center"><A href="javascript:window.close()">[Close Window]</a></p>
</body>
</html>