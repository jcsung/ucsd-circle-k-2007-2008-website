<html>
<head>
<title>Event Creator</title>
<link rel="stylesheet" type="text/css" href="../ucsdcki.css">
</head>
<body>
<?php
	require('../utils.php');
	$your_ip_address=GetHostByName($_SERVER['REMOTE_ADDR']);
	amanda_session_garbage_collect();
	$session_existence=amanda_session_exists($your_ip_address);
	if ($session_existence){
		$username=amanda_session_get_username($your_ip_address);
		$password=amanda_session_get_password($your_ip_address);
		$secLevel=getInfo($username,$password,'secLevel');
		if ($secLevel>650)
			$editable=true;
		else
			$editable=false;
	}
	if (!$editable)
		die('<span class="error">You should not be here.</span>');
	$temp=explode(',','Service,Social,Fundraiser,Kiwanis Family,MD&E,Interclub,District,Divisional,International,Administrative');
	if ($secLevel>900) array_push($temp,'Testing Purposes');
	$types='<table class="matt">';
	for ($x=0; $x<sizeOf($temp); $x++) 
		$types.='<tr><td valign="middle" align="left"><input type="checkbox" name="types[\''.$temp[$x].'\']"></td><td align="left" valign="middle">'.$temp[$x].'</td></tr>';
	$types.='</table>';
	$temp=explode(' ','January February March April May June July August September October November December');
	$cur=getMonth(currentTime(),'num');
	$date='<select name="date_m">';
	for ($x=0; $x<sizeOf($temp); $x++){
		$date.='<option value="'.($x+1).'"';
		if ($x+1==$cur) $date.=' selected="selected"';
		$date.='>'.$temp[$x].'</option>';
	}
	$date.='</select>&nbsp;&nbsp;<select name="date_d">';
	for ($x=1; $x<=31; $x++)
		$date.='<option>'.$x.'</option>';
	$date.='</select>&nbsp;,&nbsp;&nbsp;<select name="date_y">';
	for ($x=0; $x<3; $x++)
		$date.='<option>'.(getYear(currentTime())+$x).'</option>';
	$date.='</select>';
	$cur=explode(' ','start stop');
	$temp=array();
	array_push($temp,12);
	for ($x=1; $x<12; $x++)
		array_push($temp,$x);
	$time='';
	for ($x=0; $x<2; $x++){
		$time.='<select name="time_'.$cur[$x].'_hour">';
		for ($y=0; $y<sizeOf($temp); $y++)
			$time.='<option>'.$temp[$y].'</option>';
		$time.='</select>&nbsp;:<select name="time_'.$cur[$x].'_min">';
		for ($y=0; $y<59; $y+=5)
			$time.='<option>'.($y<10?'0':'').$y.'</option>';
		$time.='</select>&nbsp;<select name="time_'.$cur[$x].'_meridian"><option>AM</option><option>PM</option></select>';
		if ($x==0) $time.='&nbsp;&nbsp;TO&nbsp;&nbsp;';
	}
	$chairs='<table class="matt" border="1"><tr><td align="left" valign="middle"><table class="matt">';
	mysql_connect('localhost','circlek1_cki','cki') or die('<span class="error">Error: Unable to connect to server</span>');
	mysql_select_db('circlek1_members') or die('<span class="error">Error: Unable to connect to database</span>');
	$query='SELECT `username` FROM `memberlist` WHERE `secLevel`>\'700\'';
	$data=mysql_query($query);
	for ($x=0; $temp2=mysql_fetch_row($data); $x++){
		$query='SELECT `fname`,`lname` FROM `profiles` WHERE `username`=\''.$temp2[0].'\'';
		$temp=mysql_query($query);
		$temp3=mysql_fetch_row($temp);
		$cur[$x]=$temp3[0].' '.$temp3[1];
	}
	for ($x=0; $x<sizeOf($cur); $x++){
		for ($y=$x+1; $y<sizeOf($cur); $y++){
			if (strcmp(strtolower($cur[$x]),strtolower($cur[$y]))>0){
				$temp=$cur[$x];
				$cur[$x]=$cur[$y];
				$cur[$y]=$temp;
			}
		}
	}
	for ($x=0; $x<sizeOf($cur); $x++)
		$chairs.='<tr><td valign="middle" align="left"><input type="checkbox" name="chairs[\''.$cur[$x].'\']"></td><td align="left" valign="middle">'.$cur[$x].'</td></tr>';
	mysql_close();
	$chairs.='</table></td><td align="center" valign="middle" class="bold">AND</td><td align="center" valign="middle"><textarea name="chair_add" rows="5" cols="20"></textarea></td></tr>';
	$chairs.='</table>';
	$allowsignups='<select name="allowsignups"><option>Yes</option><option>No</option></select>';
	$pic='<input type="text" name="pic" size="50">';
	$pic.='<br>URL should include "http://" in it.'; 
	$pic.='<br>Optimal picture size is 400x300 or some multiple thereof.';
	$redir='<input type="text" name="redirect" size="50">';
	$redir.='<br>Leave this blank if you do not want the event page to redirect.';
	$redir.='<br>If you do want this event page to redirect, please include http:// in the URL';
?>
<p class="title" align="center">Event Creator</p>
<table class="matt">
<form action="eventcreatorbackend.php" method="post">
<tr><td class="bold" align="left" valign="top">Name:</td><td align="left" valign="top"><input type="text" name="name" size="50"></td></tr>
<tr><td class="bold" align="left" valign="top">Type(s):</td><td align="left" valign="top"><?php echo $types; ?></td></tr>
<tr><td class="bold" align="left" valign="top">Description:</td><td align="left" valign="top"><textarea name="info" rows="4" cols="55"></textarea></td></tr>
<tr><td class="bold" align="left" valign="top">Date:</td><td align="left" valign="top"><?php echo $date; ?></td></tr>
<tr><td class="bold" align="left" valign="top">Time:</td><td align="left" valign="top"><?php echo $time; ?></td></tr>
<tr><td class="bold" align="left" valign="top">Location:</td><td align="left" valign="top"><input type="text" name="location" size="50"></td></tr>
<tr><td class="bold" align="left" valign="top">Chair(s):</td><td align="left" valign="top"><?php echo $chairs; ?></td></tr>
<tr><td class="bold" align="left" valign="top">Notes:</td><td align="left" valign="top"><textarea name="notes" rows="4" cols="55"></textarea></td></tr>
<tr><td class="bold" align="left" valign="top">Signup Cap:</td><td align="left" valign="top"><input type="text" name="signupcap" size="50" value="2147483647"> (For uncapped events, use 2147483647)</td></tr>
<tr><td class="bold" align="left" valign="top">Allow Signups?</td><td align="left" valign="top"><?php echo $allowsignups; ?></td></tr>
<tr><td class="bold" align="left" valign="top">Picture URL:</td><td align="left" valign="top"><?php echo $pic; ?></td></tr>
<tr><td class="bold" align="left" valign="top">Redirect?</td><td align="left" valign="top"><?php echo $redir; ?></td></tr>
<tr><td colspan="2"><input type="hidden" name="verification" value="snarfblat"><input type="submit" value="Create Event"></td></tr>
<input type="hidden" name="isFirstday" value="Yes">
<input type="hidden" name="deleted" value="No">
</form>
<tr><td colspan="2"><a href="../leah.php">Back to Admin Tools</a></td></tr>
</table>
</body>
</html>