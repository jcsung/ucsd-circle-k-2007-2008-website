<html>
<head>
<title>Edit Next Meeting Frame Backend</title>
<link rel="stylesheet" type="text/css" href="../ucsdcki.css">
</head>
<body>
<?php
require('../utils.php');
$your_ip_address=GetHostByName($_SERVER['REMOTE_ADDR']);
amanda_session_garbage_collect();
$session_existence=amanda_session_exists($your_ip_address);
if ($session_existence){
	$username=amanda_session_get_username($your_ip_address);
	$password=amanda_session_get_password($your_ip_address);
	$secLevel=getInfo($username,$password,'secLevel');
	if ($secLevel>650)
		$editable=true;
	else
		$editable=false;
	$editor=getInfo($username,$password,'fname').' '.getInfo($username,$password,'lname').' ('.$username.')';	
	$what='';
}
if (!$editable||$_POST['verification']!='snarfblat')
	die('<span class="error">You should not be here.</span>');
//print_r($_POST); echo '<br>';

if ($_POST['havenext']=='No'&&$_POST['donthavenext']=='Other'&&!trim($_POST['donthavenextother']))
	die('<span class="error">Error: You did not enter a custom "Other" reason for no next meeting.</span><br><a href="javascript:history.back(1)">Go back to fix this</a>');
$which=$_POST['which'];
for ($x=0; $x<sizeOf($which); $x++){
	$_POST[$which[$x].'_month']=$_POST['month'];
	//echo $_POST['day'].' '.$_post[$which[$x].'_day'].'<br>';
	$_POST[$which[$x].'_day']=$_POST['day'];
	//echo $_POST['day'].' '.$_post[$which[$x].'_day'].' '.$which[$x].'<br>';
	$_POST[$which[$x].'_year']=$_POST['year'];
	if (!trim($_POST[$which[$x].'_location'])){
		die('<span class="error">Error: You did not enter a location for the '.$_POST[$which[$x].'_type'].'</span><br><a href="javascript:history.back(1)">Go back to fix this</a>');
	}
	if (!trim($_POST[$which[$x].'_map'])){
		$temp='';
		$temp2=explode(' ',$_POST[$which[$x].'_location']);
		for ($y=0; $y<sizeOf($temp2); $y++){
			$temp.=$temp2[$y];
			if ($y<sizeOf($temp2)-1) $temp.='+';
		}
		$_POST[$which[$x].'_map']='http://campusmap.michaelkelly.org/map.cgi?to='.$temp;
	}
	if ($_POST[$which[$x].'_meridian']=='PM'&&$_POST[$which[$x].'_hour']!=12) $_POST[$which[$x].'_hour']+=12;
	if ($_POST[$which[$x].'_hour']==12&&$_POST[$which[$x].'_meridian']=='AM') $_POST[$which[$x].'_hour']=0;
	$_POST[$which[$x].'_date']=mktime($_POST[$which[$x].'_hour'],$_POST[$which[$x].'_minute'],0,$_POST[$which[$x].'_month'],$_POST[$which[$x].'_day'],$_POST[$which[$x].'_year']);
	//echo $_POST['day'].' '.$_post[$which[$x].'_day'].' '.$which[$x].'<br>';
}
//print_r($_POST); echo '<br>';
mysql_connect('localhost','circlek1_cki','cki') or die('<span class="error">Error: Unable to connect to server</span>');
mysql_select_db('circlek1_misc') or die('<span class="error">Error: Unable to connect to database</span>');
$query='DELETE FROM `havenextmeeting` WHERE 1';
$data=mysql_query($query);
//echo mysql_affected_rows().' rows affected <br>';
$what.='Next meeting occuring? ';
if ($_POST['havenext']=='Yes'){
	$break='No';
	$what.='Yes';
}
else{
	$break=$_POST['donthavenext'];
	$what.='No - '.$break.($break=='Other'?' ('.$_POST['donthavenextother'].')':'');
}
$what.='<br>';
$query='INSERT INTO `havenextmeeting` (`break`,`message`) VALUES (\''.$break.'\',\''.trim($_POST['donthavenextother']).'\')';
$data=mysql_query($query);
//echo mysql_affected_rows().' rows affected <br>';
$dels=0;
for ($x=0; $x<sizeOf($which); $x++){
	$_POST[$which[$x].'_serial']=$which[$x];
	$tq='UPDATE `meetinglocation` SET ';
	$query='SHOW COLUMNS FROM `meetinglocation`';
	$data=mysql_query($query);
	for ($y=0; $info=mysql_fetch_row($data); $y++){
		$tq.='`'.$info[0].'`=\'';
		$tq.=trim($_POST[$which[$x].'_'.$info[0]]).'\',';
		$what.=$info[0].': ';
		if ($info[0]!='date')
			$what.=$_POST[$which[$x].'_'.$info[0]];
		else 
			$what.=date('m/d/Y h:i A',$_POST[$which[$x].'_'.$info[0]]);
		$what.='<br>';
	}
	$tq=substr($tq,0,strlen($tq)-1);
	$tq.=' WHERE `serial`=\''.$which[$x].'\'';
	//echo $tq.'<br>';
	$td=mysql_query($tq);
	$dels+=mysql_affected_rows();
}
//echo $what.'<br>';
mysql_select_db('circlek1_logs') or die('<span class="error">Error: Unable to connect to database</span>');
$query='INSERT INTO `admin` (`username`,`action`,`detail`,`time`,`ip`) VALUES(\'';
$query.=$editor.'\',\'';
$query.='EDITED NEXT MEETING INFO'.'\',\'';
$query.=$what.'\',\'';
$query.=currentTime().'\',\'';
$query.=$your_ip_address.'\')';
if ($dels>0)
	mysql_query($query);
mysql_close();
echo '<script language="Javascript">parent.nextmeeting.location.href = \'../nextmeeting.php\';</script>';
?>

<p class="title">Next Meeting Location Editor</p>
<p>The Next Meeting Information has been changed.</p>
<p><a href="../leah.php">Back to Admin Tools</a></p>
</body>
</html>