<html>
<head>
<title>Picture of the Week Caption Editor</title>
<link rel="stylesheet" type="text/css" href="../ucsdcki.css">
</head>
<body>
<p class="title">PotW Caption Editor</p>
<?php
	require('../utils.php');
	$your_ip_address=GetHostByName($_SERVER['REMOTE_ADDR']);
	amanda_session_garbage_collect();
	$session_existence=amanda_session_exists($your_ip_address);
	if ($session_existence){
		$username=amanda_session_get_username($your_ip_address);
		$password=amanda_session_get_password($your_ip_address);
		$secLevel=getInfo($username,$password,'secLevel');
		if ($secLevel>650)
			$editable=true;
		else
			$editable=false;
	}
	if (!$editable)
		die('<span class="error">You should not be here.</span>');
	mysql_connect('localhost','circlek1_cki','cki') or die('<span class="error">Error: Unable to connect to server</span>');
	mysql_select_db('circlek1_misc') or die('<span class="error">Error: Unable to connect to database</span>');
	$query='SELECT * FROM `potw`';
	$data=mysql_query($query);
	$info=mysql_fetch_row($data);
	$date=$info[2];
	$posMon=explode(' ','January February March April May June July August September October November December');
	$mon=getMonth($date,'num');
	$day=getDay($date);
	$year=getYear($date);
	$posTypes=explode(' ','jpg JPG gif GIF png PNG bmp BMP tiff TIFF');
	echo '<p><form action="captionpotwbackend.php" method="post"><table class="matt">';
	echo '<tr><td class="bold" align="left" valign="top">Picture Name:</td><td align="left" valign="top">potw.<select name="type">';
	for ($x=0; $x<sizeOf($posTypes); $x++){
		echo '<option';
		if ($posTypes[$x]==$info[0]) echo ' selected="selected"';
		echo '>'.$posTypes[$x].'</option>';
	}
	echo '</select></td></tr>';
	echo '<tr><td class="bold" align="left" valign="top">Caption:</td><td align="left" valign="top"><input type="text" name="caption" length="50" value="'.$info[1].'"></td></tr>';
	echo '<tr><td class="bold" align="left" valign="top">Date of Event:</td><td align="left" valign="top">';
	echo '<select name="mon">';
	for ($x=1; $x<=12; $x++){
		echo '<option';
		if ($x==$mon) echo ' selected="selected"';
		echo ' value="'.$x.'">'.$posMon[$x-1].'</option>';
	}
	echo '</select>&nbsp;';
	echo '<select name="day">';
	for ($x=1; $x<=31; $x++){
		echo '<option';
		if ($x==$day) echo ' selected="selected"';
		echo '>'.$x.'</option>';
	}
	echo '</select>,&nbsp;';
	echo '<select name="year">';
	for ($x=$year-1; $x<$year+1; $x++){
		echo '<option';
		if ($x==$year) echo ' selected="selected"';
		echo '>'.$x.'</option>';
	}
	echo '</select>';
	echo '</td></tr>';
	echo '<tr><td class="bold" align="left" valign="top">Description:</td><td align="left" valign="top"><textarea name="explanation" cols="30" rows="10">'.$info[3].'</textarea></td></tr>';
	
	echo '</table><input type="hidden" name="verification" value="snarfblat"><div align="center"><input type="submit" value="Edit"></div></form></p>';
?>
	<p align="Center"><a href="javascript:window.close()">Close Window</a></p>
</body>
</html>