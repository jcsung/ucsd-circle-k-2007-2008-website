<html>
<head>
<title>Currently Open Sessions</title>
<link rel="stylesheet" type="text/css" href="../ucsdcki.css">
</head>
<body>
<p class="title">Currently Open Sessions</p>
<?php
	require('../utils.php');
	$your_ip_address=GetHostByName($_SERVER['REMOTE_ADDR']);
	amanda_session_garbage_collect();
	$session_existence=amanda_session_exists($your_ip_address);
	if ($session_existence){
		$username=amanda_session_get_username($your_ip_address);
		$password=amanda_session_get_password($your_ip_address);
		$secLevel=getInfo($username,$password,'secLevel');
		if ($secLevel>650)
			$editable=true;
		else
			$editable=false;
	}
	if (!$editable)
		die('<span class="error">You should not be here.</span>');
	mysql_connect('localhost','circlek1_cki','cki') or die('<span class="error">Error: Unable to connect to server</span>');
	mysql_select_db('circlek1_misc') or die('<span class="error">Error: Unable to connect to database</span>');
	$query='SELECT `ip`,`username`,`timestamp` FROM `sessions` ORDER BY `timestamp` DESC';
	$data=mysql_query($query);
	echo '<table class="matt" width="100%" border="1">';
	echo '<tr><td class="bold" align="center">IP Address</td><td class="bold" align="center">Username</td><td class="bold" align="center">Time</td></tr>';
	while ($info=mysql_fetch_row($data)){
		$info[2]=date('l, F j, Y g:i A',$info[2]);
		echo '<tr>';
		for ($x=0; $x<3; $x++)
			echo '<td align="center">'.$info[$x].'</td>';
		echo '</tr>';
	}
	echo '</table>';
	mysql_close();
?>
	<p align="center"><a href="javascript:window.location.reload()">Reload</a></p>
	<p align="center"><a href="javascript:window.close()">[Close Window]</a></p>
</body>
</html>