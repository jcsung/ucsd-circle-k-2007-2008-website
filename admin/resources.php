<html>
<head>
<title>Admin</title>
<link rel="stylesheet" type="text/css" href="../ucsdcki.css">
</head>
<body>
<?php
require('../utils.php');
$your_ip_address=GetHostByName($_SERVER['REMOTE_ADDR']);
amanda_session_garbage_collect();
$session_existence=amanda_session_exists($your_ip_address);
if ($session_existence){
	$username=amanda_session_get_username($your_ip_address);
	$password=amanda_session_get_password($your_ip_address);
	$secLevel=getInfo($username,$password,'secLevel');
	if ($secLevel>900)
		$editable=true;
	else
		$editable=false;
}
if (!$editable)
	die('<span class="error">You should not be here.</span>');
?>
<a name="top"></a><p class="title">Webmaster Stuff</p>
<p class="subtitle">Table of Contents</p>
<p>
<ol>
<li><a href="#secretary">Secretary</a></li>
<li><a href="#resources">Web Resources</a></li>
<li><a href="#security">Security Level Info</a></li>
<li><a href="#date">Date Info</a></li>
<li><a href="#encryption">Encryption</a></li>
<li><a href="javascript:window.close()">Close</a></li>
</ol>
</p>

<a name="secretary"></a><p class="subtitle">Secretary Identifier</p>
<p>
<?php
mysql_connect('localhost','circlek1_cki','cki') or die('<span class="error">Error: Unable to connect to server</span>');
mysql_select_db('circlek1_members') or die('<span class="error">Error: Unable to connect to database</span>');
if ($_GET['sec']==1){
	$secretaries=$_POST['sec'];
	$usernames=@array_keys($secretaries);
	for ($x=0; $x<sizeOf($usernames); $x++){
		$query='UPDATE `memberlist` SET `secLevel`=\'899\' WHERE `username`=\''.$usernames[$x].'\'';
		//echo $query.'<br>';
		mysql_query($query);
	}
}
$query='SELECT * FROM `memberlist` WHERE `secLevel`=\'899\'';
$data=mysql_query($query);
$rows=mysql_num_rows($data);
if ($rows==0) echo '<span class="error bold">Warning: There are currently no secretary accounts!</span><br>';
else echo '<span class="bold">There are currently '.$rows.' secretary accounts.</span><br>';
$query='SELECT `username`,`fname`,`mname`,`lname` FROM `profiles` ORDER BY `lname`,`fname`';
$data=mysql_query($query);
echo '<form action="resources.php?sec=1" method="post"><table class="matt" border="0">';
echo '<tr><td align="center" class="subtitle">Username</td><td align="center" class="subtitle">Name</td><td align="center" class="subtitle">Secretary?</td></tr>';
for ($x=0; $info=mysql_fetch_row($data); $x++){
	$cur_username=$info[0];
	$name='';
	for ($y=1; $y<4; $y++){
		$name.=$info[$y].' ';
	}
	$name=trim($name);
	$tquery='SELECT `secLevel` FROM `memberlist` WHERE `username`=\''.$cur_username.'\'';
	$tdata=mysql_query($tquery);
	$tinfo=mysql_fetch_row($tdata);
	$securityLevel=$tinfo[0];
	if ($securityLevel<=650) continue;
	$isornot='<input type="checkbox" name="sec['.$cur_username.']"';
	if ($securityLevel==899) $isornot.=' checked="checked"';
	$isornot.='>';
	echo '<tr><td>'.$cur_username.'</td><td>'.$name.'</td><td align="center">'.$isornot.'</td></tr>';		
}
echo '<tr><td colspan="2"><input type="submit" value="Set"></td></tr>';
echo '</table></form>';
mysql_close();
?>
<br><a href="#top">Back to Top</a>
</p>

<a name="resources"></a><p class="subtitle">Web Resources</p>
<p>
<ol>
<li><a href="http://www.w3schools.com/tags/default.asp" target="_blank">HTML 4.1 Reference</a></li>
<li><a href="http://www.w3schools.com/css/css_reference.asp" target="_blank">CSS Reference</a></li>
<li><a href="http://www.designmeltdown.com/" target="_blank">Design Meltdown</a></li>
<li><a href="http://pageresource.com/jscript" target="_blank">Javascript Tutorials</a></li>
<li><a href="http://www.php.net/manual/en/" target="_blank">PHP Manual</a></li>
<li><a href="http://www.mysql.org/doc/refman/4.1/en/index.html" target="_blank">MySQL 4.1 Manual</a></li>
<li><a href="https://mail.ucsdcirclek.org:2083/frontend/x3/index.html" target="_blank">Site Conrol Panel</a></li>
<li><a href="https://mail.ucsdcirclek.org:2083/frontend/x3/sql/PhpMyAdmin.html" target="_blank">MySQL Database Access</a></li>
</ol>
<br><a href="#top">Back to Top</a>
</p>
<a name="security"></a><p class="subtitle"">Security Info</p>
<p><table class="matt">
<tr><td align="right">1</td><td align="center">-</td><td align="right">100</td><td align="center">=</td><td align="left">Locked Account</td></tr>
<tr><td align="right">101</td><td align="center">-</td><td align="right">200</td><td align="center">=</td><td align="left">Guest</td></tr>
<tr><td align="right">201</td><td align="center">-</td><td align="right">300</td><td align="center">=</td><td align="left">Confirmed Nonmember</td></tr>
<tr><td align="right">301</td><td align="center">-</td><td align="right">400</td><td align="center">=</td><td align="left">Unconfirmed Member/Nonmember</td></tr>
<tr><td align="right">401</td><td align="center">-</td><td align="right">500</td><td align="center">=</td><td align="left">Confirmed Member</td></tr>
<tr><td align="right">501</td><td align="center">-</td><td align="right">600</td><td align="center">=</td><td align="left">Non-UCSD Circle K Person of Importance</td></tr>
<tr><td align="right">601</td><td align="center">-</td><td align="right">650</td><td align="center">=</td><td align="left">Committee Member</td></tr>
<tr><td align="right">651</td><td align="center">-</td><td align="right">700</td><td align="center">=</td><td align="left">Beta Tester</td></tr>
<tr><td align="right">701</td><td align="center">-</td><td align="right">800</td><td align="center">=</td><td align="left">Board Member</td></tr>
<tr><td align="right">801</td><td align="center">-</td><td align="right">900</td><td align="center">=</td><td align="left">Executive Board Member</td></tr>
<tr><td align="right">901</td><td align="center">-</td><td align="right">1000</td><td align="center">=</td><td align="left">Website Administrator</td></tr>
</table><br><a href="#top">Back to Top</a></p>
<a name="date"></a><p class="subtitle"">Date Info</p>
<p>
<?php
$posM=explode(' ',' January February March April May June July August September October November December');
if($_POST['submit']=='Get Date Info'){
	if (!trim($_POST['day'])){
		$_POST['mon']=getMonth(currentTime(),'num');
		$_POST['day']=getDay(currentTime());
		$_POST['year']=getYear(currentTime());
	}
	$date1=$posM[$_POST['mon']].' '.$_POST['day'].', '.$_POST['year'].' is '.mktime(0,0,0,$_POST['mon'],$_POST['day'],$_POST['year']).'<br>';
	if (!$_POST['timestamp']) $_POST['timestamp']=currentTime();
	$date2=$_POST['timestamp'].' is '.date('l, F j, Y H:i:s',$_POST['timestamp']).'<br>';
}
//print_r($_POST); echo '<br>';
echo ($_POST['mon']==0?'':$date1).($_POST['timestamp']?$date2:'');
?>
</p>
<p><form action="resources.php" method="post">Date: <select name="mon">
<?php
for ($x=0; $x<13; $x++)
	echo '<option value="'.$x.'">'.$posM[$x].'</option>';
?>
</select>&nbsp;<select name="day"><option>&nbsp;</option>
<?php
for ($x=1; $x<=31; $x++)
	echo '<option>'.$x.'</option>';
?>
</select>,&nbsp;<input name="year" type="text" size="4" maxlength="4" value="<?php echo getYear(currentTime()); ?>">
<br>
Timestamp: <input type="text" name="timestamp" size="10"><br>
<input type="submit" name="submit" value="Get Date Info">
</form>
<br><a href="#top">Back to Top</a></p>
<a name="encryption"></a><p class="subtitle">Encryption</p>
<p>
<?php
if (!(trim($_POST['encryption'])==='')) echo '"'.$_POST['encryption'].'" encrypted is "'.encrypt($_POST['encryption']).'"<br>';
?>
<form action="resources.php" method="post">
Input String: <input type="text" name="encryption"><br>
<input type="submit" value="Encrypt">
</form><br><a href="#top">Back to Top</a></p>


<p align="center"><a href="javascript:window.close()">[Close Window]</a></p>
</body>
</html>

