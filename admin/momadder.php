<html>
<head>
<title>Member of the Month Adder</title>
<link rel="stylesheet" type="text/css" href="../ucsdcki.css">
</head>
<body>
<p class="title">MOM Adder</p>
<?php
	require('../utils.php');
	$your_ip_address=GetHostByName($_SERVER['REMOTE_ADDR']);
	amanda_session_garbage_collect();
	$session_existence=amanda_session_exists($your_ip_address);
	if ($session_existence){
		$username=amanda_session_get_username($your_ip_address);
		$password=amanda_session_get_password($your_ip_address);
		$secLevel=getInfo($username,$password,'secLevel');
		if ($secLevel>650)
			$editable=true;
		else
			$editable=false;
	}
	if (!$editable)
		die('<span class="error">You should not be here.</span>');
	$temp=currentTime();
	$mon=getMonth($temp,'num');
	$year=getYear($temp);
	$posMonths=explode(' ','January February March April May June July August September October November December');
	$posTypes=explode(' ','jpg gif png bmp tiff');
?>
	<p align="center"><form action="momadderbackend.php" method="post">
	<table class="matt" width="100%">
	<tr><td class="bold">Name:</td><td><input type="text" name="name" length="50"></tr>
	<tr><td class="bold">When:</td><td><select name="mon">
<?php
	for ($x=1; $x<=12; $x++){
		echo '<option value="'.$x.'"';
		if ($x==$mon) echo ' selected="selected"';
		echo '>'.$posMonths[$x-1].'</option>';
	}
?>
	</select>&nbsp;<select name="year">
<?php
	for ($x=$year-1; $x<=$year+1; $x++){
		echo '<option';
		if ($x==$year) echo ' selected="selected"';
		echo '>'.$x.'</option>';
	}
?>	
	</select></td></tr>	
	<tr><td class="bold">File Type:</td><td><select name="pic">
<?php
	for ($x=0; $x<sizeOf($posTypes); $x++)
		echo '<option>'.$posTypes[$x].'</option><option>'.strtoupper($posTypes[$x]).'</option>';
?>
	</select></td></tr>
	<input type="hidden" name="verification" value="snarfblat"><tr><td colspan="2" align="center"><input type="submit" value="Post"></td></tr></form></table></p>
	<p align="center"><a href="javascript:window.close()">[Close Window]</a></p>
</body>
</html>