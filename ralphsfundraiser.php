<html>
<head>
<title>About Circle K</title>
<link rel="stylesheet" type="text/css" href="ucsdcki.css">
</head>
<body>
<p class="title">Ralphs Fundraiser</p>
<p>Everybody needs groceries.  It is inevitable.  Wouldn't it be nice if you could help support Circle K while 
doing something as simple as shopping for your own groceries?  It would be like hitting two bird with the 
proverbial one stone - helping one's self as well as Circle K - and hence by extension, others.   And wouldn't 
it be nice if you could do this with no extra work on your part, and no extra cost?</p>
<p>Well, it just so happens that there <span class="italic">is</span> a way to do this.  UCSD Circle K is one of 
the nonprofit organizations under Ralphs' Community Contribution program.  What happens is this: when you shop at 
Ralph's, and use your Ralph's card, they will automatically donate a part of the money you spent to us.  And 
it's for free - they won't charge you extra or anything!</p>
<p>So what do you have to do to participate in this?  Almost nothing!  Just a one-time registration of your 
Ralphs card with them so that they know to donate the money to us.  How do you do this?  Like this:</p>
<p>
<ol>
<li>Go to <a href="http://www.ralphs.com/ccprogram_agree.htm" target="_blank">
http://www.ralphs.com/ccprogram_agree.htm</a></li>
<li>Click the "I Agree" there.  (You should probably actually agree, too.)</li>
<li>Then simply fill out the form, and submit it!
<br>Notes:
<ul>
<li>For the "NPO Number," you must give them ours, which you can copy from here: 
<input value="90134" type="text" onClick="select()" readonly="readonly" size="5"></li>
<li>For the Card Holder's Name, you must fill out the name <span class="bold">of the person who originally obtained 
the card.  So, if the card was given to you by a parent, you must put your parent's name!</span>
</li>
</ul>
</li>
</ol>
</p>
<p>So you see, with almost no work, you can support us while doing something we all have to do - grocery shopping.  
So sign up now!</p>
<p align="center"><A href="welcome.php">Back to home</a></p>

</body>
</html>