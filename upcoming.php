<html>
<head>
<title>Upcoming Events</title>
<link rel="stylesheet" type="text/css" href="ucsdcki.css">
<script language="Javascript">
<!--
function custom(){
	window.open('customsearch.php','CustomSearch','width=500,height=600,resizable=yes,scrollbars=yes,location=no');
}
//-->
</script>
<style>
body{
	color: black;
	background-image: url(./images/background1.jpg);
}
a{
	text-decoration: none;
}

a:link{
	color: #660066;
}

a:visited{
	color: #660066;
}

a:active{
	color: #660066;
}

a:hover{
	color: #660066;
	text-decoration: overline underline;
}

.unlinkable{
	color: #660066;
	text-decoration: line-through;
}
</style>
</head>
<body>
<?php
require('utils.php');
function isCapped($num){
	return $num<2147483647;
}
$your_ip_address=GetHostByName($_SERVER['REMOTE_ADDR']);
amanda_session_garbage_collect();
$session_existence=amanda_session_exists($your_ip_address);
$secLevel=150;
if ($session_existence){
	$username=amanda_session_get_username($your_ip_address);
	$password=amanda_session_get_password($your_ip_address);
	$loginity=validateLogin($username,$password);
	if ($loginity)
		$secLevel=getInfo($username,$password,'secLevel');
}
$show=strtolower(trim($_GET['show']));
if ($show=='custom') $show='custom';
else if ($show=='my'&&$loginity) $show='my';
else if ($show=='month'||!$show) $show='month';
else $show='all';
echo '<p align="center" class="subtitle">Upcoming Events</p>';
echo '<p align="center"><table class="matt"><tr>';
echo '<td align="center" class="mini">';
if ($show!='all') echo '<a href="upcoming.php?show=all">';
echo 'All Events';
if ($show!='all') echo '</a>';
echo '</td>';
echo '<td align="center" class="mini">';
if ($show!='month') echo '<a href="upcoming.php?show=month">';
echo 'This Week';
if ($show!='month') echo '</a>';
echo '</td>';
if ($loginity){
	echo '<td align="center" class="mini">';
	if ($show!='my') echo '<a href="upcoming.php?show=my">';
	echo 'My Events';
	if ($show!='my') echo '</a>';
	echo '</td>';
}
else{
	$autosignup=false;
}
echo '<td align="center" class="mini">';
if ($show!='custom') echo '<a href="customsearch.php" target="home">';
echo 'Search';
if ($show!='custom') echo '</a>';
echo '</td>';
echo '</tr></table></p>';
mysql_connect('localhost','circlek1_cki','cki') or die('<span class="error">Error: Unable to connect to server</span>');
if ($loginity){
	mysql_select_db('circlek1_members') or die('<span class="error">Error: Unable to connect to database</span>');
	$query='SELECT `autoSignup` FROM `options` WHERE `username`=\''.$username.'\'';
	$data=mysql_query($query);
	$info=mysql_fetch_row($data);
	$autosignup=($info[0]=='on');
}
mysql_select_db('circlek1_events') or die('<span class="error">Error: Unable to connect to database</span>');
$today=getDayBeginning(currentTime());
$month_end=$today+604800;
/*
$num_days=explode(' ','0 31 28 31 30 31 30 31 31 30 31 30 31');
if (isLeapYear($today)) $num_days[2]++;
$month_end=mktime(23,59,59,getMonth($today,'num'),$num_days[getMonth($today,'num')],getYear($today));
*/
if ($show!='custom'){
	$query='SELECT * FROM `eventlist` WHERE `date`>=\''.$today.'\'';
	if ($secLevel<=650) $query.=' AND `deleted`=\'No\'';
	$query.=' ORDER BY `date` ASC';
}
else{
	$query=decrypt($_POST['query']);
}
//echo $query.'<br>';
$data=mysql_query($query);
$signedup=false;
echo '<p><table class="matt">';
$loopcountingvariable=1;
for ($index=1; $info=mysql_fetch_row($data); $index++){
	$event_is_deleted=$info[12];
	$is_waitlist=false;
	$signedup=false;
	$date=$info[3]; //INSERT INDEX HERE
	if ($show=='month'&&($date>$month_end)) break; 
	$serial=$info[0];
	$signups_allowed=$info[11]; //INSERT INDEX HERE
	$numerical_cap=$info[9]; //INSERT INDEX HERE
	if ($loginity){
		$temp_query='SELECT * FROM `'.$serial.'` WHERE `username`=\''.$username.'\'';
		$temp_data=mysql_query($temp_query);
		if(mysql_num_rows($temp_data)>0){
			$signedup=true;
			$temp_info=mysql_fetch_row($temp_data);
			if (strstr($temp_info[1],'Waitlist')) $is_waitlist=true;
		}
	}
	if ($show=='my'&&!$signedup) continue;
	echo '<tr><td valign="top">'.$loopcountingvariable.'.&nbsp;&nbsp;</td><td>';
	echo ' <span class="mini underline">'.date('D, M j',$info[3]).'<br></span>';
	$today=currentTime();
	if ($today>$info[3]+43200+8*3600){
		$signups_allowed='No';
		$waitlists_allowed=false;
	}
	else{
		$waitlists_allowed=true;
	}
	if ($secLevel>650&&$event_is_deleted=='Yes') echo '<strike>';
	echo $info[1];
	if ($secLevel>650&&$event_is_deleted=='Yes') echo '</strike>';
	echo '<br><span class="mini"><a href="./events/view.php?id='.$serial.'" target="home">[View]</a>&nbsp;&nbsp;' ;
	if ($signups_allowed=='No'){
		echo '<span class="unlinkable">[Sign up]</span>&nbsp;&nbsp;';
	}
	else{
		if ($waitlists_allowed){
			if ($signedup){
				echo '<a href="./events/editsignup.php?part=1&humanoid='.$username.'&id='.$serial.'&loggedin=yes&box=no" target="home">[Edit Signup]</a>&nbsp;&nbsp;';
				if (!$is_waitlist)
					echo '<a href="./events/unaction.php?action=unsignup&id='.$serial.'&loggedin=yes&humanoid='.$username.'" target="home">[Unsign up]</a>&nbsp;&nbsp;';
				else
					echo '<a href="./events/unaction.php?action=unwaitlist&id='.$serial.'&loggedin=yes&humanoid='.$username.'" target="home">[Unwaitlist]</a>&nbsp;&nbsp;';
			}
			else{
				if($loginity) $nefzi='yes';
				else $nefzi='no';
				if (!isCapped($numerical_cap)){
					if ($autosignup) echo '<a href="./events/action.php?autosignup=on&action=signup&id='.$serial.'&loggedin='.$nefzi.'&box=no" target="home">[Sign up]</a>&nbsp;&nbsp;';
					else echo '<a href="./events/view.php?id='.$serial.'#signup" target="home">[Sign up]</a>&nbsp;&nbsp;';
				}
				else{
					$temp_query='SELECT * FROM `'.$serial.'`';
					$temp_data=mysql_query($temp_query);
					$num_signups=mysql_num_rows($temp_data);
					if ($num_signups>=$numerical_cap){
						echo '<span class="unlinkable">[Sign up]</span>&nbsp;&nbsp;';
						if ($autosignup) echo '<a href="./events/action.php?autosignup=on&action=waitlist&id='.$serial.'&loggedin='.$nefzi.'&box=no" target="home">[Waitlist]</a>&nbsp;&nbsp;';
						else echo '<a href="./events/view.php?id='.$serial.'#signup" target="home">[Waitlist]</a>&nbsp;&nbsp;';
					}
					else{
						if ($autosignup) echo '<a href="./events/action.php?action=signupid='.$serial.'&loggedin='.$nefzi.'&box=no" target="home">[Sign up]</a>&nbsp;&nbsp;';
						else echo '<a href="./events/view.php?id='.$serial.'#signup" target="home">[Signup]</a>&nbsp;&nbsp;';
					}
				}
			}
		}
	}
	echo '</span>';
	echo '</td></tr>';
	$loopcountingvariable++;
}
echo '</table></p>';
mysql_close();
?>
</body>
</html>

