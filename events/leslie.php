<html>
<head>
<title>Leslie's Testing-Phase Log Viewer</title>
<link rel="stylesheet" type="text/css" href="../ucsdcki.css">
</head>
<body>
<?php
require('../utils.php');
$your_ip_address=GetHostByName($_SERVER['REMOTE_ADDR']);
amanda_session_garbage_collect();
$session_existence=amanda_session_exists($your_ip_address);
if ($session_existence){
	$username=amanda_session_get_username($your_ip_address);
	$password=amanda_session_get_password($your_ip_address);
	$loginity=validateLogin($username,$password);
}
if ($loginity){
	if ($username!='apple'&&$username!='upessimist'){
		die('You are not Leslie.<br><A href="javascript:history.back(1)">Back</a>');
	}
}
else{
	die('<span class="error">You are not Leslie.</span>');
}
?>
<h1><a name="top">Leslie's Testing-Phase All-Log Viewer</a></h1><br><br>
<h2>Table of Contents</h2>
<ol>
<li><a href="#admin">Admin Log</a></li>
<li><a href="#contact">Contact Log</a></li>
<li><a href="#login">Login Log</a></li>
<li><a href="#signup">Signup Log</a></li>
<li><a href="javascript:window.location.reload()">Refresh</a></li>
<li><a href="../leah.php">Back to Admin Tools</a></li>
</ol><br>
<?php
mysql_connect('localhost','circlek1_cki','cki') or die('Error: Unable to connect to remote host');
?>
<h2><a name="admin">ADMIN LOG</a></h2><br>
<?php
mysql_select_db('circlek1_logs') or die('Error: Unable to connect to database');
$query='SELECT * FROM `admin` ORDER BY `time` ASC';
$data=mysql_query($query);
$between=explode(' ','  ON FROM');
while ($info=mysql_fetch_row($data)){
	for ($x=0; $x<sizeOf($info); $x++){
		if ($x==3) $info[$x]=date('M d, Y h:i A',$info[$x]);
		echo $info[$x].' '.$between[$x].' ';
	}
	echo '<br>';
}
?>
<a href="#top">Back to top</a><br>
<h2><a name="contact">CONTACT LOG</a></h2><br>
<?php
mysql_select_db('circlek1_logs') or die('Error: Unable to connect to database');
$query='SELECT * FROM `contact` ORDER BY `date` ASC';
$data=mysql_query($query);
$between=explode(',','ON,SENT A MESSAGE TO,FROM');
while ($info=mysql_fetch_row($data)){
	echo 'ON '.date('M d, Y h:i A',$info[0]).' '.$info[1].' SENT A MESSAGE TO '.$info[2].' FROM '.$info[3];
	echo '<br>';
}
?>
<a href="#top">Back to top</a><br>
<h2><a name="login">LOGIN LOG</a></h2><br>
<?php
$query='SELECT * FROM `login` ORDER BY `time` ASC';
$data=mysql_query($query);
$between=explode(' ',' AT FROM');
while ($info=mysql_fetch_row($data)){
	for ($x=0; $x<sizeOf($info); $x++){
		if ($x==2) $info[$x]=date('M d, Y h:i A',$info[$x]);
		echo $info[$x].' '.$between[$x].' ';
	}
	echo '<br>';
}
?>
<a href="#top">Back to top</a><br>
<h2><a name="signup">SIGNUP LOG</a></h2><br>
<?php
$query='SELECT * FROM `signup` ORDER BY `time` ASC';
$data=mysql_query($query);
$between=explode(' ','  AT FROM');
while ($info=mysql_fetch_row($data)){
	for ($x=0; $x<sizeOf($info); $x++){
		if ($x==3) $info[$x]=date('M d, Y h:i A',$info[$x]);
		echo $info[$x].' '.$between[$x].' ';
	}
	echo '<br>';
}
?>
<a href="#top">Back to top</a><br>
<?php
mysql_close();
?>
</body>
</html>