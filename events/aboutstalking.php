<html>
<head>
<title>What's "Stalk Event?"</title>
<link rel="stylesheet" type="text/css" href="../ucsdcki.css">
</head>
<body>
<p class="subtitle">What does "Stalk Event" do?</p>
<p>If you set "Stalk Event" to "Yes," every time the event changes, you will receive an 
email detailing what changes have been made.  <span class="bold">This will occur even if you have 
disabled event email notifications in your account options</span></p>
<p align="center"><a href="javascript:window.close()">Close Window</a></p>
</body>
</html>