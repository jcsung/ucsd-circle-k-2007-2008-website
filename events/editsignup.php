<html>
<head>
<title>Edit Sign up/Waitlist</title>
<link rel="stylesheet" type="text/css" href="../ucsdcki.css">
</head>
<body>
<?php
require('../utils.php');
$your_ip_address=GetHostByName($_SERVER['REMOTE_ADDR']);
$id=trim($_GET['id']);
amanda_session_garbage_collect();
$session_existence=amanda_session_exists($your_ip_address);
if ($session_existence){
	$username=amanda_session_get_username($your_ip_address);
	$password=amanda_session_get_password($your_ip_address);
	$loginity=validateLogin($username,$password);
	if ($loginity){
		$login_check='yes';
	}
	else{
		$login_check='no';
	}
}
mysql_connect('localhost','circlek1_cki','cki') or die('<span class="error">Error: Unable to connect to server</span>');
mysql_select_db('circlek1_events') or die('<span class="error">Error: Unable to connect to database</span>');
$query='SELECT * FROM `eventlist` WHERE `serial`=\''.$id.'\'';
$data=mysql_query($query);
if (mysql_num_rows($data)==0){
	echo '<span class="error">Error: Specified event could not be found.</span>';
	mysql_close();
}
else{
	$info=mysql_fetch_row($data);
	$makeshift_name=$info[1];
	//print_r($_GET);echo '<br>';print_r($_POST);

	$id=htmlentities(trim($_GET['id']));

	$part=htmlentities(trim($_GET['part']));

	if ($part<1||$part>2||!$part){
		die('<span class="error">Error: Invalid Parameter!</span>');
	}

	$loggedin=strtolower(trim($_GET['loggedin']));
	$match=strtolower(trim($_GET['humanoid']));
	
	if ($loggedin!='yes'&&$loggedin!='no'){
		echo '<span class="error">Error: Inadequate information to proceed.</span>';
	}
	else{/*
		if (htmlentities(strtolower(trim($username)))!=htmlentities($match)){
			die('<span class="error">Error: Information does not match.</span>');
		}*/
		if ($loggedin!='yes'){
			die('<span class="error">Error: Not logged in.</span>');
		}
		else{

			if ($part==1){
				$choices=explode(' ','Yes No');
				$query='SELECT `name` FROM `eventlist` WHERE `serial`=\''.$id.'\'';
				$data=mysql_query($query);
				$info=mysql_fetch_row($data);
				$event_name=$info[0];
				echo '<form action="editsignup.php?humanoid='.$username.'&part=2&id='.$id.'&loggedin=yes&box=no" method="post">';
				$query='SELECT * FROM `'.$id.'` WHERE `username`=\''.$username.'\' AND (`type`=\'Member Signup\' OR `type`=\'Member Waitlist\')';
				$data=mysql_query($query);
				$info=mysql_fetch_row($data);
				$comment=$info[3];
				if ($comment=='&nbsp;')	$comment=' ';
				$comment_array=explode('<br>',$comment);
				$newcomment='';
				for ($x=0; $x<sizeOf($comment_array); $x++){
					$newcomment.=$comment_array[$x];
					if ($x<sizeOf($comment_array)-1) $newcomment.=chr(13);
				}
				$comment=$newcomment;
				$driving=$info[4];
				$stalk=$info[5];
				echo '<p class="subtitle" align="center">Editing Signup Information For Event "'.$event_name.'"</p>';
				echo '<p align="center"><table class="matt">';
				echo '<tr><td colspan="2">Please change your signup information below:</td></tr>';
				echo '<tr><td class="bold" align="top">Comment: </td><td><textarea name="comments" rows="4" cols="55">'.$comment.'</textarea></td></tr>';
				echo '<tr><td class="bold" align="top">Driving? </td><td><select name="driving">';
				for ($x=0; $x<2; $x++){
					echo '<option';
					if ($choices[$x]==$driving) echo ' selected="selected"';
					echo '>'.$choices[$x].'</option>';
				}
				echo '</select></td></tr>';
				echo '<tr><td class="bold" align="top">Stalk Event? </td><td><select name="stalk">';
				for ($x=0; $x<2; $x++){
					echo '<option';
					if ($choices[$x]==$stalk) echo ' selected="selected="';
					echo '>'.$choices[$x].'</option>';
				}
				echo '</select></td></tr>';
				echo '<tr><td colspan="2" align="center"><input type="submit" name="submit" value="Edit Signup"></td></tr>';
				echo '</table></p>';
				echo '</form>';
			}
			else{
				mysql_select_db('circlek1_events') or die('<span class="error">Error: Unable to connect to database</span>');
				$comment=htmlentities($_POST['comments']);
				$newcomment='';
				for ($x=0; $x<strlen($comment); $x++){
					$temp=substr($comment,$x,1);
					if ($temp==chr(13)) $newcomment.='<br>';
					else $newcomment.=$temp;
				}
				$comment=$newcomment;
				$comment=trim($comment);
				//echo $comment.'<br>';
				$driving=$_POST['driving'];
				$stalk=$_POST['stalk'];
				$query='UPDATE `'.$id.'` SET `comment`=\''.$comment.'\' , `driving`=\''.$driving.'\' , `stalk`=\''.$stalk.'\' WHERE (`username`=\''.$username.'\' AND (`type`=\'Member Signup\' OR `type`=\'Member Waitlist\'))';
				//echo $query.'<br>';
				$data=mysql_query($query);
				//echo mysql_affected_rows().' rows affected<br>';

				mysql_select_db('circlek1_events') or die('<span class="error">Error: Unable to connect to database</span>');
				$query='SELECT * FROM `eventlist` WHERE `serial`=\''.$id.'\'';
				//echo $query.'<br>';
				$data=mysql_query($query);
				$info=mysql_fetch_row($data);
				$event_name=$info[1];
				$query='SELECT * FROM `'.$id.'` WHERE `stalk`=\'Yes\'';
				$data=mysql_query($query);
				if ($action='UNSIGNED UP FROM') $action='unsignup';
				else $action='unwaitlist';
				while($info=mysql_fetch_row($data)){
					mysql_select_db('circlek1_members') or die('<span class="error">Error: Unable to connect to database</span>');
					$temp_name=$info[0];
					$temp_query='SELECT * FROM `profiles` WHERE `username`=\''.$temp_name.'\'';
					$temp_data=mysql_query($temp_query);
					$temp_info=mysql_fetch_row($temp_data);
					$to=$temp_info[5];
					$subj='[UCSD Circle K] Event Stalking: '.$event_name;
					$msg='Hello '.$temp_info[2].','.chr(13).'There has been a signup editing  for the event "'.$event_name.'."'.chr(13).chr(13).'--UCSD Circle K'.chr(13).chr(13).'You are receiving this notification email because you are stalking the event "'.$event_name.'."  To stop receiving these emails, edit your signup for this event, and set "Stalk Event?" to "No."';
					//echo $to.'<br>'.$subj.'<br>'.$msg.'<br>';
					mail($to,$subj,$msg,'From: UCSD Circle K <circlek@ucsd.edu>');
					mysql_select_db('circlek1_events') or die('<span class="error">Error: Unable to connect to database</span>');
				}

				mysql_select_db('circlek1_events') or die('<span class="error">Error: Unable to connect to database</span>');		
				$query='SELECT * FROM `'.$id.'` WHERE `type`=\'Member Signup\' OR `type`=\'Member Waitlist\'';
				$data=mysql_query($query);
				for ($x=0; $info=mysql_fetch_row($data); $x++){
					$toemail[$x]=$info[0];
					//echo $toemail[$x].'<br>';
				}
				mysql_select_db('circlek1_members') or die('<span class="error">Error: Unable to connect to database</span>');
				for ($x=0; $x<sizeOf($toemail); $x++){
					$query='SELECT * FROM `options` WHERE `username`=\''.$toemail[$x].'\'';
					$data=mysql_query($query);
					$info=mysql_fetch_row($data);
					if ($info[6]=='Yes'){
						$temp_query='SELECT * FROM `profiles` WHERE `username`=\''.$toemail[$x].'\'';
						$temp_data=mysql_query($temp_query);
						$temp_info=mysql_fetch_row($temp_data);
						$to=$temp_info[5];
						$subj='[UCSD Circle K] Event Signup Editing Notification: '.$event_name;
						$msg='Hello '.$temp_info[2].','.chr(13).'There has been a signup editing for the event "'.$event_name.'."'.chr(13).chr(13).'--UCSD Circle K'.chr(13).chr(13).'You are receiving this notification email because you set "Email me if signups change on my signed-up events" to "Yes."  To change this, log in and go to "Edit Preferences" in the login page.';
						//echo $to.'<br>'.$subj.'<br>'.$msg.'<br>';
						mail($to,$subj,$msg,'From: UCSD Circle K <circlek@ucsd.edu>');
					}
				}
				
				mysql_select_db('circlek1_members') or die('<span class="error">Error: Unable to connect to database</span>');
				$name=$username;
				if ($loggedin=='no') $name='GUEST ('.$name.')';
				else{
					$teh_query='SELECT * FROM `profiles` WHERE `username`=\''.$name.'\'';
					$teh_data=mysql_query($teh_query);
					$teh_info=mysql_fetch_row($teh_data);
					$name=$teh_info[2].' '.$teh_info[4].' ('.$name.')';
				}
				mysql_select_db('circlek1_logs') or die('<span class="error">Error: Unable to connect to database</span>');
				$teh_query='INSERT INTO `signup` (`username`,`action`,`event`,`time`,`ip`) VALUES (\''.$name.'\',\'EDITED SIGNUP FOR\',\'<a href="../events/view.php?id='.$id.'">'.$makeshift_name.'</a>\',\''.currentTime().'\',\''.$your_ip_address.'\')';
				mysql_query($teh_query);



				echo '<meta http-equiv="refresh" content="0;url=view.php?id='.$id.'">';
			}
		}


		
		mysql_close();
	}
}


?>
</body>
</html>