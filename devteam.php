<html>
<head>
<title>The AMANDA Dev Team</title>
<link rel="stylesheet" type="text/css" href="ucsdcki.css">
</head>
<body>
<p><h1>The AMANDA Dev Team</h1></p>
<p>It is the work of these heroic indivduals that you have the AMANDA you see before you.  Honor them 
as you would honor..um..honorable people, or something.</p>
<p><h2>2007-2008 Circle K Year</h2>
Thanks to the official members of the Dev Team:
<table border="1" class="matt">
<tr><td>Jeffrey Sung</td><td>Developer, Programmer, and Website Chair</td></tr>
<tr><td>Eugenia Leong</td><td>Computer Science Advisor and Flash Advisor</td></tr>
<tr><td>Jeff Jou</td><td>Developer and Flash Advisor</td></tr>
<tr><td>Lily Chang</td><td>Unbiased Perspective and Content Advisor</td></tr>
<tr><td>Jehan Kanga</td><td>Photographer and Content Advisor</td></tr>
<tr><td>Dante !</td><td>Content Advisor</td></tr>
<tr><td>Peter Lai</td><td>Content Advisor</td></tr>
<tr><td>Alan Nam</td><td>Content Advisor</td></tr>
</table>
Thanks also to the honorary Dev Team members:
<table border="1" class="matt">
<tr><td>Spencer Shepard of UC Davis</td><td>Alpha and Beta Tester and Systems Advisor</td></tr>
<tr><td>Matt Chandrangsu</td><td>Quality Assurance and Beta Tester</td></tr>
<tr><td>Eileen Zhang</td><td>Graphic Artist and Layout Advisor</td></tr>
<tr><td>Leslie Lu</td><td>Head Alpha and Beta Tester</td></tr>
<tr><td>Angela Yen</td><td>Content and Layout Advisor</td></tr>
<tr><td>Amanda Ramond</td><td>Content and Layout Advisor</td></tr>
<tr><td>Zenas Hau</td><td>Artistic and Layout Advisor</td></tr>
<tr><td>Melissa Kong from GMU Circle K</td><td>Honorary Dev Team Member and Moral Support</td></tr>
</table>
<br>Special thanks to Annie Chow for content and aesthetic advice.
<br>Special thanks to Christopher Casebeer for his invaluable coding and conceptual advice.
<br>Finally, a big thanks to everyone in Circle K during the 2007-2008 year for their continued support 
</p>
<p><h2>2006-2007 Circle K Year</h2>
Note: This year's team was an unofficial team: the people listed here are those who contributed the most.
<table class="matt" border="1">
<tr><td>Jeffrey Sung</td><td> Developer and Programmer</td></tr>
<tr><td>Alan Nam</td><td> Website Chair and Content Advisor</td></tr>
<tr><td>Spencer Shepard of UC Davis</td><td> Alpha and Beta Tester</td></tr>
<tr><td>Amanda Ramond</td><td> Voice of Common Sense, Bugfinder, and Content Advisor</td></tr>
<tr><td>Vidhi Doshi</td><td>Club President and Content Advisor</td></tr>
</table>
<br>Special thanks to Matt Chandrangsu for finding a profile changing error in version 0.5.3 and all manner of advice and testing.
<br>Special thanks to Annie Chow for finding a bug with driver listings in Version 1.6.
<br>Finally, a big thanks to everyone in Circle K during the 2006-2007 year for their continued support 
for AMANDA.
</p>
<p><a href=".">Back to Index</a></p>
</body>
</html>