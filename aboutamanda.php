<html>
<head>
<title>About AMANDA</title>
<link rel="stylesheet" type="text/css" href="ucsdcki.css">
</head>
<body>
<p class="title">About AMANDA</p>
<p><span class="bold">What is AMANDA?</span><br>
AMANDA stands for Automated Member-Accessible Network Database Accounts.  But what is she?  it?  ...he? 
AMANDA is more than an acronym.  AMANDA is a paradigm.  An ideal.  An energy field that permeates 
the entire universe, created by all living things.  Oh wait, that's the Force.  Anyway - AMANDA is the 
UCSD Circle K website's content-management system.  The automated event signup system.  The backend of a 
sophisticated website.  It is...AMANDA.
</p>
<p><span class="bold">The Parts of AMANDA</span><br>
AMANDA is made up of many parts.  They are listed in historical order of their creation:<table class="matt">
<tr><td valign="top" class="bold">LEAH:</td><td>Login-Enabled Administration Hub (The Administrative Control Panel.)</td></tr>
<tr><td valign="top" class="bold">ANNIE:</td><td>Another Napless Night-Induced Epiphany (Powers the MRP display system)</td></tr>
<tr><td valign="top" class="bold">VIDHI:</td><td>Virtual InterDomain-Hopping Interface (Powers the Links system)</td></tr>
<tr><td valign="top" class="bold">SHARON:</td><td>Service Hour Addition and Recognition ONline (A never-finished web-driven dynamic MRP system that was rendered obsolete by District Secretary Andrew's MRP spreadsheet)</td></tr>
<tr><td valign="top" class="bold">HANAN:</td><td>Hours And Numbers Addition and eNumeration (The MRP database created for SHARON)</td></tr>
<tr><td valign="top" class="bold">JEANNIE:</td><td>Jeff's Excellent And New Nocturnally-Implemented Epiphany (Powered the AMANDA v. 1.x Event Signup System.)</td></tr>
<tr><td valign="top" class="bold">JACK:</td><td>Jeff's Awesome Coding Knowledge (Powers the chatterbox)</td></tr>
<tr><td valign="top" class="bold">NATASHA:</td><td>Now-Automated Temporal Account Simple & Harmonic Algorithm (Powers the calendar and the Upcoming Events page)</td></tr>
<tr><td valign="top" class="bold">TRISHA:</td><td>Triply Re-parsing Intelligent Sensitive non-Heuteristic Algorithm (The chatterbox's parsing algorithm.  Algorithm has been substatially improved and no longer needs to parse text multiple times.)</td></tr>
<tr><td valign="top" class="bold">JER:</td><td>J<span class="mini">EANNIE</span> Expansive  Revision (An overhaul of the Event Signup System.)</td></tr>
<tr><td valign="top" class="bold">CAROLINE:</td><td>CSV-Accessing Reader Of Logged INformation Extension (A new MRP system that reads MRP data from CSV files instead of displaying a gif)</td></tr>
</table>
</p>

</body>
</html>