<html>
<head>
<title>About Circle K</title>
<link rel="stylesheet" type="text/css" href="ucsdcki.css">
<style>
.filled{
	list-style-type: disc;
}
.empty{
	list-style-type: circle;
}
</style>
</head>
<body>
<p class="title">About Circle K</p>
<p></p>
<p><h2 class="subtitle">Overview</h2></p>
<p>Circle K International is the world�s largest collegiate service organization boasting over 12,000 members worldwide, in over 500 campuses, over 7 countries and over a million service hours each year. In fact service is only one goal of Circle K in addition to training its members as leaders and building lasting friendships. Thus our three tenets are Service, Leadership, and Fellowship. </p>
<p>Circle K at UCSD is sponsored by the Kiwanis Club of La Jolla.</p>
<p></p>
<p><h2 class="subtitle">Pledge</h2></p>
<p><table class="matt" width="100%"><tr><td>I pledge to uphold the objects of Circle K International, </td></tr>
<tr><td>To foster compassion and goodwill toward others through service and leadership</td></tr>
<tr><td>To develop my abilities and the abilities of all people</td></tr>
<tr><td>And to dedicate myself to the realization of humankind's potential.</td></tr></table></p>
<p></p> 
<p><h2 class="subtitle">Objects of Circle K</h2></p>
<p>The objects of Circle K International shall be: <ul>
<li class="filled">To emphasize the advantages of the democratic way of life;</li> 
<li class="filled">To provide the opportunity for leadership training in service; </li>
<li class="filled">To serve on the campus and in the community; </li>
<li class="filled">To cooperate with the administrative officers of the educational institutions of which the clubs are a part; </li>
<li class="filled">To encourage participation in group activities; </li>
<li class="filled">To promote good fellowship and high scholarship; </li>
<li class="filled">To develop aggressive citizenship and the spirit of service for improvement of all human relationships; </li>
<li class="filled">To afford useful training in the social graces and personality development; and </li>
<li class="filled">To encourage and promote the following ideals: <ul>
<li class="empty">To give primacy to the human and spiritual rather than to the material values of life; </li>
<li class="empty">To encourage the daily living of the Golden Rule in all human relationships; </li>
<li class="empty">To promote the adoption and the application of high social, business and professional standards; </li>
<li class="empty">To develop, by precept and example, a more intelligent, aggressive, and serviceable citizenship; </li>
<li class="empty">To provide through Circle K clubs a practical means to form enduring friendships, to render altruistic service, and to build better communities; and </li>
<li class="empty">To cooperate in creating and maintaining that sound public opinion and high idealism which makes possible the increase of righteousness, justice, patriotism and goodwill. </li>
</ul></li></ul></p>


<p></p>

<p><table class="matt"><tr><td width="100%"><h2 class="Subtitle">Structure</h2></td></tr>
<tr><td><span class="bold">Club</span> - This level is comprised of members within a particular school or university. Here, a board of officers runs the operations of the club. A President, Administrative Vice President, Service Vice President, Treasurer, and Secretary make up the Executive Board. Our club is the mighty Circle K International at University of California San Diego.</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td><span class="bold">Division</span> - Each club is part of a division, which is comprised of other clubs within a close demographic area. Each division has an elected Lieutenant Governor (LTG) whose job is to bring clubs of the division together and assist them with their individual needs. Our club falls under the Paradise Division, which comprises of Grossmont College, San Diego State University, and University of Hawaii- Manoa. </td></tr>
<tr><td>&nbsp;</td></tr>

<tr><td><span class="bold">District</span> - Each club in a division then falls under a district which is usually bounded by state lines. Our district is the California-Nevada-Hawaii District and is comprised of 8 divisions. During the annual district convention, delegates from all clubs in the district elect a Governor, Secretary, Treasurer, and Publications Editor to service on the District Executive Board. A board of district chairs is then appointed in order to carry out specific district focuses and events throughout the year in which clubs from all over the district can participate and attend. </td></tr>
<tr><td>&nbsp;</td></tr>

<tr><td><span class="bold">Sub-region</span> - Every sub-region is a collective of several bordering districts based on size, demographic, and cultures. The California-Nevada-Hawaii district with the Rocky Mountain and Southwest districts, make up Sub-region B. Every year at International Convention the sub-region elects a Representative to be the voice and bridge to international. </td></tr>
<tr><td>&nbsp;</td></tr>

<tr><td><span class="bold">International</span> - Of all 30 districts in the organization, they together comprise Circle K International. At this level, there is an International President and Vice President as well as 9 elected Sub-regional Representatives who carry out in the best interests of Circle K international as a whole, and for each individual sub-region. </td></tr>
</table>

</body>
</html>