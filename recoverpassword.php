<html>
<head>
<title>Recovery Password</title>
<link rel="stylesheet" type="text/css" href="ucsdcki.css">
</head>
<body>
<p class="title">Recover Password</p>
<?php
require('utils.php');
if ($_POST['submit']!='Recover'){
?>
	<form action="recoverpassword.php" method="post">
	<p>We all forget.  Don't worry though, recovering your password is easy!</p>
	<p>Simply fill in your username, and we will email your password to the email your account is registered to.</p>
	<p>Username? <input type="text" name="username" maxlength="30"> <input type="submit" name="submit" value="Recover"></p>
	</form>
<?php
}
else{
	//print_r($_POST); echo '<br>'; 
	$username=htmlentities($_POST['username']);
	mysql_connect('localhost','circlek1_cki','cki') or die('<span class="error">Error: Unable to connect to server</span>');
	mysql_select_db('circlek1_members') or die('<span class="error">Error: Unable to connect to database</span>');
	$query='SELECT * FROM `profiles` WHERE `username`=\''.$username.'\'';
	$data=mysql_query($query);
	if (mysql_num_rows($data)<1)
		die('<span class="error">Error: We don\'t have an account with that username.  <a href="recoverpassword.php">Go back and fix this</a></span>');
	$info=mysql_fetch_row($data);
	$name=$info[2].' '.$info[4];
	$to=$info[5];
	$subj='Your AMANDA Password';
	$pass=decrypt($info[1]);
	$msg='Hello, '.$name.','.chr(13).chr(13).'You are receiving this email because someone (presumably you) has asked AMANDA to recover your password.  This has been done.'.chr(13).'Your password: '.$pass.chr(13).'Hopefully this helps!'.chr(13).chr(13).'Sincerely,'.chr(13).'UCSD Circle K';
	$from='From: UCSD Circle K <circlek@ucsd.edu>';
	$bool=mail($to,$subj,$msg,$from);
	if ($bool)
		echo '<p>Your password has been successfully emailed to you.</p>';
	else 
		echo '<p>For some reason, your password could not be emailed to you.  You will have to contact an administrator to have him/her recover your password for you.</p>';
	mysql_close();
	
}
?>
</body>
</html>