<?php
$titles=explode('|','FTC?  Federal Trade Commission?|DLSSP: What is it?|DCON: TEH AWESOME|I know ICON, but can you?');
$pos=explode(' ','FTC DLSSP DCON ICON');
$pos_2=explode(',','Fall Training Conference,District Large Scale Service Project: South,District Convention,International Convention');
$when=explode('|','November 2-4, 2007|February 23-24, 2008|March 28-30, 2008|This has not been announced yet.');
$where=explode('|','in Norcal - Sonora, California (somewhat close to Sacramento)|somewhere San Diegoish|in San Jose, California!|Somewhere.');
$why=explode('|','<table class="matt" border="1"><tr><td align="center"><img src="./images/ftcad1.jpg" border="0"><br>Road trip!  A long one too - notice how late we arrived.</td></tr><tr><td align="center"><img src="./images/ftcad2.jpg" border="0"><br>Participate in an awesome campfire skit!</td></tr><tr><td align="center"><img src="./images/ftcad3.jpg" border="0"><br>Learn useful skills and information in workshops!</td></tr><tr><td align="center"><img src="./images/ftcad4.jpg" border="0"><br>And of course, hang out with people from all over California, Nevada, and Hawaii!</td></tr></table>|<table class="matt" border="1"><tr><td align="center"><img src="./images/dlsspad2.jpg" border="0"><br>Paint.</td></tr><tr><td align="center"><img src="./images/dlsspad3.jpg" border="0"><br>Clean.</td></tr><tr><td align="center"><img src="./images/dlsspad4.jpg" border="0"><br>Build.</td></tr><tr><td align="center"><img src="./images/dlsspad1.jpg" border="0"><br>Make a difference.</td></tr><tr><td align="center"><img src="./images/dlsspad5.jpg" border="0"><br>Look, it\'s our governor!</td></tr></table>|<table class="matt" border="1"><tr><td align="center"><img src="./images/dconad1.jpg" border="0"><br>Represent UCSD!</td></tr><tr><td align="center"><img src="./images/dconad2.jpg" border="0"><br>Do some service!</td></tr><tr><td align="center"><img src="./images/dconad3.jpg" border="0"><br>Apply for (and possibly obtain) Circle K scholarships!</td></tr><tr><td align="center"><img src="./images/dconad4.jpg" border="0"><br>Have your hours of service  recognized!</td></tr><tr><td align="center"><img src="./images/dconad5.jpg" border="0"><br>Most of all, have fun!</td></tr></table>|<table class="matt" border="1"><tr><td align="center"><img src="./images/iconad1.jpg" border="0"><br>Represent Cal-Nev-Ha in an exotic locale!</td></tr><tr><td align="center"><img src="./images/iconad2.jpg" border="0"><br>This picture is awesome</td></tr><tr><td align="center"><img src="./images/iconad3.jpg" border="0"><br>Watch Cal-Nev-Ha dominate the awards (we do every year)!</td></tr><tr><td align="center"><img src="./images/iconad4.jpg" border="0"><br>Fraternize with fellow Circle Kers from all over the world!</td></tr></table>');
$what_extended=explode('|',"Fall Training Conference is a three-day conference that happens every fall.  At this conference, Circle K clubs from all over our California-Nevada-Hawaii (Cal-Nev-Ha) district come together.  At FTC there are workshops where one can learn useful things, campfire skits performed by each club (you can help participate in ours!), and even a dance on the last day!  So come and have a great time!|DLSSP is, as its name says, a large-scale service project.  Basically, it is a service project attended by the entire district.  So help make a difference!  Attend DLSSP!|Disrict Convention.  The mere mention of this is enough to send old Circle Kers into deep reveries as they reminisce about the good times they have had.  District convention is the last event of the Circle K year.  Like FTC, there will be workshops.  However, these workshops are far more interesting and fun - for example, last dcon there was a Hawaiian dance workshop!  Also, at dcon the awards are given out to various clubs or club officers for exemplary service or performance.  If there is just one district event you can go to, be sure to make it DCON.  It is an amazing experience unmatched by almost any other in Circle K!  And this year, it's the weekend of Spring Break!  So don't buy your plane tickets back to UCSD just yet!|International Convention is the biggest international Circle K event there is.  At this convention, members of Circle K clubs from all over the world meet to mingle, do some service, and have fun.  And it's not going to just be in San Jose or Sonora.  This year, it will be in CITY, STATE!  How awesome is that?  See the world!  Meet people from other places!  Come to International Convention!");
$linx=explode('|','./events/view.php?id=000231|./events/view.php?id=000377|./events/view.php?id=000379|#');

$loc=-1;
$query=trim($_GET['show']);
//echo $query.'<br>';
for ($x=0; $x<sizeOf($pos); $x++){
	if (strcmp(strtolower($pos[$x]),strtolower($query))==0){
		$loc=$x;
		break;
	}
}
if ($loc==-1) $title="Error: Event Not Found";
else $title='About '.$pos_2[$loc];
$slk=$pos[$loc];
$blh=$pos_2[$loc];
?>
<html>
<head>
<title><?php echo $title; ?></title>
<link rel="stylesheet" type="text/css" href="ucsdcki.css">
<style>
.filled{
	list-style-type: disc;
}
.empty{
	list-style-type: circle;
}
</style>
</head>
<body>
<?php
if ($loc==-1) die('<span class="error">Error: Event specified could not be found.</span>');
?>
<p class="title"><?php echo $titles[$loc]; ?></p>
<p><table class="matt" width="100%">
<?php
echo '<tr><td width="20%" valign="top" class="bold">Who is this page for?</td><td class>This page is for YOU!  Here you can find out information about '.$slk.'</td></tr>';
echo '<tr><td width="20%" valign="top" class="bold">What is '.$slk.'?</td><td>'.$slk.' stands for '.$blh.'.  '.$blh.' is a Circle K '.($loc!=3?' district':'n international').' event.  '.$what_extended[$loc].'</td></tr>'; 
echo '<tr><td width="20%" valign="top" class="bold">When is it?</td><td>'.$when[$loc].'</td></tr>'; 
echo '<tr><td width="20%" valign="top" class="bold">Where is it?</td><td>This year, '.$slk.' will be '.$where[$loc].'</td></tr>'; 
echo '<tr><td width="20%" valign="top" class="bold">Why should I attend?</td><td>There are many reasons to attend '.$slk.':<br><br>'.$why[$loc].'</td></tr>'; 
echo '<tr><td colspan="2" align="center"><h1>Interested?  Click <a href="'.$linx[$loc].'">here</a> to sign up!</h1></tr>'; 
?>
<?php

?>
</table>
</p>
</body>
</html>
