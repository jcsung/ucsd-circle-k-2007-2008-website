<html>
<head>
<title>President's Message</title>
<link rel="stylesheet" type="text/css" href="ucsdcki.css">
<style>
</head>
<body>
<table><tr><td class="matt" width="100%" height="100%">
<img src="./images/alanicon.jpg" border="0" align="left">
Welcome, new and returning members.<br><br>

In case you've forgotten or simply didn't know, this is Circle K
International at UCSD, where doing service, becoming a community leader,
and making life-long friends are all just the tip of the iceberg. How do
you even begin to explain the smiling faces of children you read to, the
"thank you's" from those you help feed, the laughter from the community
you help build? Or explain to friends back home, that the people you met
aren't just fellow members, but family members? Or the mornings you wake
up and wish you still were in bed, but then going to sleep that night,
glad you went out and touched a life? It's hard for me to explain my
experiences, so I simply do them.<br><br>

My name is Alan Nam, and I'm proud to be your president for this year. I
may be loud, crack an inappropriate joke or two, or even look like a lost
12 year-old boy when I'm standing around not doing anything, but I'm
always willing (and wanting!) to listen if you have problems, need
questions answered, or even if you just had a bad day and need to vent. If
you're a new, or prospective, member and I haven't had a chance to meet
you personally, please stop and say "hi". I hope to make a lot of new
friends this year, because I know I'll at least be changing lives.<br><br>

Proud for service,<br><br>
Alan Y. Nam<br><br>
UCSD Circle K International<br><br>
</td></tr></table>
</body>
</html>