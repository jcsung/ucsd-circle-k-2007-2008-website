<html>
<head>
<title>AMANDA Terms of Service</title>
<link rel="stylesheet" type="text/css" href="ucsdcki.css">
</head>
<body>
<p align="center" class="title">AMANDA Terms of Service</p>
<p>By registering for an AMANDA account, you agree to the following:</p>
<p><ol>
<p class="subtitle">General Rules and Information</p>
<li>Your personal information supplied during the registration process is real and up-to-date.  This is 
required because we need to know who you are so we can confirm you (see #2) and then be able to contact you 
if neccessary.</li>
<li>Upon registering, your account status is set to "Unconfirmed Member."  Your account status will be 
changed by the UCSD Circle K board from there to either "Confirmed Nonmember" or "Confirmed Member," at  
some point after your registration.  Confirmed Member accounts have access to more of UCSD Circle K's 
online resources, including other confirmed members' profiles (see #4 for more on this).</li>
<li>The UCSD Circle K board has full access to the personal information entered into your account's 
profile, whether or not you have set the options to hide the hideable parts.  This is so that the board can get in 
contact with you with information about events.  Of course, UCSD Circle K board members will never sell, 
auction, give away, or otherwise redistribute your personal information.  However, the UCSD Circle K board 
cannot be held responsible for personal information being compromised by hacking of the database by others 
with malicious intent.</li>
<li>You can hide some of the personal information you enter into AMANDA.  Any of this information that 
you do not hide, via your Account Options, can be viewed by any other "Confirmed Member" account-holder.  
This is to allow free exchange of member contact information to foster an atmosphere of fellowship.  
However, UCSD Circle K, cannot be held responsible for the actions of other members with "Confirmed 
Member" status, including, but not limited to, redistribution of your information, or any other actions 
that may violate these terms of service.  However, if they do so, their accounts and IPs will be 
banned without possibility of being unbanned.</li>
<li>If/When your account status is changed to "Confirmed Member," you agree to not redistribute the 
personal information of other Confirmed Members or Confirmed Nonmembers or otherwise abuse the trust 
afforded to you by UCSD Circle K and its members in the sharing of this information.</li>
<li>The changing of account status to "Confirmed Member" is entirely at the discretion of the UCSD Circle 
K board and can be reversed at any time.</li>
<li>UCSD Circle K reserves the right to ban AMANDA accounts if it is deemed neccessary and proper.</li>
<p class="subtitle">Login, Chatterbox, and Signup Rules and Information</p>
<li>Logins are logged.  An excessive and unreasonable number of incorrect attempted logins in a 
short period of time will be taken as an attempt to break into someone else's account, and 
may lead to a ban.  If you have forgotten your password, you can easily recover it through the "Recover 
Password" link in the Login frame.</li>
<li>Posting in the chatterbox is logged, using a similar format as Flooble chatterboxes - IP address and 
time is logged.  These can be viewed for any post by clicking on the poster's name.  The last part of 
eadh IP address is hidden from public view, for obvious security reasons.  However, it is available to 
the UCSD Circle K board, and they reserve the right to use such information to prevent abuse of the 
chatterbox.</li>
<li>Do not spam or impersonate others in the chatterbox.  Please note that posting with a name like 
"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" or posting a comment 
of that sort is considered spamming.</li>
<li>Event signups/waitlists, edits, and unsignups/unwaitlists are logged.</li>
<li>In any place where the user can post text, images, or any other media, abusive, obscene, vulgar, slanderous, hateful, threatening, 
sexually-oriented or any other material that may violate any applicable laws will not be tolerated.  In 
addition, you agree that the UCSD Circle K board can edit or remove </li>
<p class="subtitle">Finally</p>
<li>Anyone found in violation of any of these terms of service can be banned by the UCSD Circle K board.</li>
</ol></p>
<p align="center"><a href="javascript:window.close()">[Close Window]</p>
</body>
</html>