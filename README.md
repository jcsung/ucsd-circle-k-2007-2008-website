# UCSD Circle K 2007 - 2008 Website

Description
===========
The 2007-2008 UCSD Circle K Website was my first attempt at web development.  
Even back then, I recognized I was carrying out a lot of bad practices.  
API keys and Database passwords are in the files.  The front and back ends 
are not separated, so it is impossible to change the front end without 
refactoring the back end, and vice versa.  The variable naming is atrocious.  

But everyone had to start somewhere, and this is how I started.  Minus pages 
that had personal information or Facebook App API keys, pictures with people 
whose permission I did not get, and files that I do not know the copyright 
status of, this is the 2007-2008 UCSD Circle K site.

The site included the Automated Member-Accessible Networked Database Accounts, 
a PHP/MySQL event signup system that handled online event signups, with 
event subscriptions, a highly customized administrative tool suite for board 
members, simplistic analytics tools, and a semi-automated MRP-display system.  
There was also a Facebook App that displayed the week's upcoming events.  
(The code for that was not included here).

The "Where is PCU?" Flash app (sort of a very specific version of Street View 
before Street View), was developed by @jsjou, and is included with his 
permission.

History
=======

The beginning: 2006-2007
------------------------
In my freshman year of college, a suitemate of mine joined the community 
service organization Circle K International.  By the next year, he  had 
become the Tech chair, in charge of the club website.  Leveraging existing 
services offered by UC San Diego's Academic Computing Services, he computerized 
the event signup system.  No longer would club officers need to stay 
behind in meeting rooms and copy down the list of names of event 
attendees.  Instead, members could fill out an online form, the contents 
of which woudl be emailed to him.  He would then manually update the 
event's webpage to include the member's sign up information.

He and I both recognized the inherent inefficiency of this system, 
especially when large-scale events with huge numbers of signups needed 
to occur, or during particularly busy midterms weeks.  So he bought a 
book to learn PHP and MySQL.  Academics, Circle K, and real life 
interfered, and the book sat unused.  At the time, I had no expertise in web 
development.  However, I had been interested since the end of high school, 
when five peers of mine had assisted our AP Computer Science teacher, the late 
Dave Wittry, in designing and developing an online scheduling system for the 
students using PHP/MS Access.

That semester, I had scheduled my classes to give myself free Fridays.  One 
late Thursday night, I found myself without anything to do.  Borrowing the 
book, I began learning basic PHP, and after an all-nighter, had a working 
proof-of-concept of what would become known as the Automated Member-Accessible 
Networked Database Accounts.

As I was not the Tech Chair, and was still learning and experimenting, this 
system was kept unstyled and mostly separate from the rest of the website.  
However, I recognized that this was poor UI/UX (even if I did not know this 
terminology back then), and also recognized that with my academic, work, and 
extracurricular responsibilities, it would take some time to optimize the 
UI/UX and create a robust and scalable application.  Thus I proposed a 3-year 
timeline: the first year (this one), a proof-of-concept year, where the system 
was kept entirely separate and function trumped form.  The second year would 
involve a naive integration of the system with the remainder of the website.  
I would spend the remainder of that year learning more about the MVC model 
(which I had recently read about), and in the final year, I would fully 
refactor the system to separate the front and back ends.  This was necessary 
to ensure the website's front end could be redesigned every year (as was 
tradition) without requiring a massive rewrite every year of the back end 
systems.

Finding Service: 2007-2008
--------------------------
The following year, I became the Tech Chair.  During the summer break, I 
rebuilt the system from the ground up, integrating all of the code into 
the the remainder of the website.  While even back then I recognized this 
as bad practice, this was done to release a functional website.  New features 
were added to the system, and a simple Facebook App displaying the week's 
upcoming events as well.  Afterwards, I began learning the Smarty templating 
engine and more about the MVC model in preparation for the third year of the 
plan.

As the year ended, the new executive board chose to go in a different 
direction with the website, and the 3-year plan was cancelled.  I worked 
with the new Tech Chair to help migrate over the back end code into her 
new front end, and my direct contributions to the Circle K tech stack ended.
