<html>
<head>
<title>Resources</title>
<link rel="stylesheet" type="text/css" href="ucsdcki.css">
</head>
<body>
<p class="title"><a name="top">Resources</a></p>
<p>Circle K is an international organization with clubs all over the world.  Here you can access the 
combined electronic resources of all of Circle K International.</p>
<!--<p class="bold">Warning: This page has not been really updated since May 2007, and many of the links seem to be broken.  
The links will be updated once classes start fully and the Circle K clubs are able to get their websites 
up and running again.</p>//-->
<p>
<table class="matt" border="0" width="100%">
<tr>
<td align="center" valign="middle" width="20%"><a href="#us"><img src="./images/us_nemo.png" border="0" alt="Us" width="64.5" height="50"></a></td>
<td align="center" valign="middle" width="20%"><a href="#division"><img src="./images/division_nemo.png" border="0" alt="Division" width="110" height="90"></a></td>
<td align="center" valign="middle" width="20%"><a href="#district"><img src="./images/district_nemo.png" border="0" alt="District" width="120" height="120"></a></td>
<td align="center" valign="middle" width="20%"><a href="#subregion"><img src="./images/subregion_nemo.png" border="0" alt="Sub-Region" width="120" height="120"></a></td>
<td align="center" valign="middle" width="20%"><a href="#international"><img src="./images/international_nemo.png" border="0" alt="International" width="120" height="120"></a></td>
</tr>
<tr>
<td align="center" valign="middle" width="20%"><a href="#us">Us</a></td>
<td align="center" valign="middle" width="20%"><a href="#division">Division</a></td>
<td align="center" valign="middle" width="20%"><a href="#district">District</a></td>
<td align="center" valign="middle" width="20%"><a href="#subregion">Sub-Region</a></td>
<td align="center" valign="middle" width="20%"><a href="#international">International</a></td>

</tr>

</tr></table>
</p>
<p><table class="matt" width="600" border="0" cellspacing="10" cellpadding="0">
<tr>
<td align="center" valign="top"><img src="./images/ucsd_logo.gif" alt="" border="0"></td>
<td>
<p><h2 class="subtitle"><a name="us">Us: UCSD</a></h2></p>
<p>
<ul type="square">
<li><a href="http://groups.yahoo.com/group/ucsdcirclek/" target="_blank">Our Yahoo! Group</a><ul><li>Join this to receive our emails informing you of meeting locations, project times, and anything else we have to tell you!</li></ul></li>
<li><a href="http://www.ljkiwanis.org/" target="_blank">Our Sponsoring Kiwanis' Website</a><ul><li>This is the website of our sponsoring Kiwanis club, containing all of their online resources!</li></ul></li>
<li><a href="#top">Back to top</a></li>
</ul>
</p>
</td>
</tr>
<tr><td align="center" colspan="2">&nbsp;</td></tr>
<tr>
<td align="center" valign="top"><img src="./images/paradise_logo.gif" alt="" border="0"></td>
<td>
<p><h2 class="subtitle"><a name="division">The Division: Paradise</a></h2></p>
<p>
<ul type="square">
<!--<li>California State University, San Marcos</li>//-->
<li><a href="http://www.myspace.com/grossmontcirclek" target="_blank">Grossmont College Circle K</a></li>
<!--<li>Hawaii Pacific University</li>//-->
<li><a href="http://www-rohan.sdsu.edu/~circlek/" target="_blank">San Diego State University Circle K</a></li>
<li><a href="http://www2.hawaii.edu/~cki/" target="_blank">University of Hawaii, Manoa Circle K</a></li>
<li><a href="#top">Back to top</a></li>
</ul>
</p>
</td>
</tr>
<tr><td align="center" colspan="2">&nbsp;</td></tr>
<tr>
<td align="center" valign="top"><img src="./images/calnevha_logo.gif" alt="" border="0"></td>
<td>
<p><h2 class="subtitle"><a name="district">The District: Cal-Nev-Ha</a></h2></p>
<p>
<ul type="square">
<li><a href="http://www.cnhcirclek.org/" target="_blank">Circle K Cal-Nev-Ha District Website</a><ul><li>This is the website for our district, California-Nevada-Hawaii (aka Cal-Nev-Ha).  Here you will find many, many resources available to you, including the district newsletter, the <a href="http://cnhcirclek.org/resources/publish.html">Sunburst</a>.  So why don't you check out this site?</li></ul></li>
<!--<li>California State University, Sacramento</li>//-->
<li><a href="http://ucdaviscki.org/" target="_blank">University of California, Davis Circle K</a></li>
<li><a href="http://geocities.com/UOPCircleK/" target="_blank">University of the Pacific Circle K</a></li>
<li><a href="http://www.unrcirclek.bravehost.com/" target="_blank">University of the Nevada, Reno Circle K</a></li>
<!--<li>Allan Hancock College</li>//-->
<li><a href="http://www.csufcirclek.org/" target="_blank">California Sate University, Fresno Circle K</a></li>
<li><a href="http://www.calpoly.edu/~cirkclub/" target="_blank">California Polytechnic State University, San Luis Obispo Circle K</a></li>
<li><a href="http://orgs.sa.ucsb.edu/cki/" target="_blank">University of California, Santa Barbra Circle K</a></li>
<!--<li>California State University, Stanislaus</li>//-->
<!--<li>California State University, San Bernardino</li>//-->
<li><a href="http://www.ucrcirclek.org/" target="_blank">University of California, Riverside Circle K</a></li>
<li><a href="http://circlek.unlv.edu/" target="_blank">University of Nevada, Las Vegas Circle K</a></li>
<!--<li>University of Redlands</li>//-->
<li><a href="http://www.csupomona.edu/~cki/" target="_blank">California Polytechnic State University, Pomona Circle K</a></li>
<!--<li>The Claremont Colleges</li>//-->
<li><a href="http://www.circlekpcc.org/" target="_blank">Pasadena City College Circle K</a></li>
<!--<li>Mt. San Antonio College</li>//-->
<li><a href="http://profile.myspace.com/index.cfm?fuseaction=user.viewprofile&friendid=126584233" target="_blank">DeAnza College Circle K</a></li>
<!--<li>Foothill College</li>//-->
<!--<li><a href="http://www.humboldt.edu/~circlek" target="_blank">Humboldt State University Circle K</a></li>//-->
<!--<li>Ohlone College</li>//-->
<li><a href="http://www.circleksf.org/" target="_blank">San Francisco State University Circle K</a></li>
<li><a href="http://student.santarosa.edu/circlek/index.html" target="_blank">Santa Rosa Jr. College Circle K</a></li>
<!--<li>Sonoma State University</li>//-->
<!--<li>St. Mary's College</li>//-->
<li><a href="http://www.stanford.edu/group/circle-k/" target="_blank">Stanford University Circle K</a></li>
<li><a href="http://www.ocf.berkeley.edu/~circlek/" target="_blank">University of California, Berkeley Circle K</a></li>
<li><a href="http://www.geocities.com/ckicsuf/" target="_blank">California State University, Fullerton Circle K</a></li>
<!--<li>Fullerton College</li>//-->
<!--<li>Golden West College</li>//-->
<!--<li>Irvine Valley College</li>//-->
<!--<li>Orange Coast College</li>//-->
<!--<li>Santa Ana College</li>//-->
<li><a href="http://www.ucicirclek.org/" target="_blank">University of California, Irvine Circle K</a></li>
<!--<li>Whittier College</li>//-->
<li><a href="http://www.csudh.edu/circle_k/" target="_blank">California State University, Dominguez Hills Circle K</a></li>
<!--<li>El Camino College</li>//-->
<!--<li>Long Beach City College</li>//-->
<li><a href="http://www.beachcki.org/" target="_blank">Long Beach State University Circle K</a></li>
<li><a href="http://circlek.bol.ucla.edu/" target="_blank">University of California, Los Angeles Circle K</a></li>
<li><a href="http://www-scf.usc.edu/~circlek/" target="_blank">University of Southern California Circle K</a></li>
<li><a href="#top">Back to top</a></li>
</ul>
</p>
</td>
</tr>
<tr><td align="center" colspan="2">&nbsp;</td></tr>
<tr>
<td align="center" valign="top"><img src="./images/b.gif" alt="" border="0"></td>
<td>
<p><h2 class="subtitle"><a name="subregion">Sub-Region: Sub-Region B</a></h2></p>
<p>
<ul type="square">
<li>Coming soon.</li>
<li><a href="#top">Back to top</a></li>
</ul>
</p>
</td>
</tr>
<tr><td align="center" colspan="2">&nbsp;</td></tr>
<tr>
<td align="center" valign="top"><img src="./images/world_logo.gif" alt="" border="0"></td>
<td>
<p><h2 class="subtitle"><a name="international">The World: Circle K International</a></h2></p>
<p>
<ul type="square">
<li><a href="http://www.circlek.org/" target="_blank">Circle K International Website</a><ul><li>This is the international website for Circle K.  It contains tons of resources for Circle Kers ALL over the world - from information on scholarships to job opportunities, internships, and more!  This site is definitely worth checking out (you'll need to register for an account though).</li></ul></li>
<li><a href="#top">Back to top</a></li>
</ul>
</p>
</td>
</tr>
</table></p>
</body>
</html>