<html>
<head>
<title>Processing your registration...</title>
<link rel="stylesheet" type="text/css" href="ucsdcki.css">
</head>
<body>
<?php
require('utils.php');
$tos=$_POST['terms'];
if ($tos!='on'){
	$message='<span class="error">';
	$message.='You did not agree to the terms of service.<br>';
	$message.='You must do this to get an AMANDA account.<br>';
	$message.='<a href="javascript:history.back(1)">Go back and fix this.</a>';
	$message.='</span>';
	die($message);
}
$_POST['username']=htmlentities(trim($_POST['username']));
$_POST['fname']=htmlentities(trim($_POST['fname']));
$_POST['lname']=htmlentities(trim($_POST['lname']));
$_POST['password']=htmlentities(trim($_POST['password']));
$_POST['confpassword']=htmlentities(trim($_POST['confpassword']));
$profile=explode(' ','username password fname mname lname email phone secQues secQuesAns college year sex dob aim icq msn yahoo website interests music tv movies books mrp');
$options=explode(' ','username hideEmail hidePhone hideSex hideDOB notifyIfEventInfoChanged notifyIfSignupsChanged autoSignup hideIM');
$mlist=explode(' ','username secLevel facebook');
$autosignup=explode(' ','username comment driving stalk');
$autosignup_defaults=explode('|',$_POST['username'].'||No|No');
$_POST['mrp']='notfound';
$_POST['secLevel']=350;
$_POST['phone']='';
$_POST['autoSignup']='off';
for ($x=0; $x<10; $x++){
	$_POST['phone'].=$_POST['phone'.$x];
	if ($x==2||$x==5) $_POST['phone'].='-';
}
for ($x=18; $x<=22; $x++){
	$temp='';
	$temp2=htmlentities($_POST[$profile[$x]]);
	for ($y=0; $y<strlen($temp2); $y++){
		$temp3=substr($temp2,$y,1);
		if ($temp3!=chr(13)) $temp.=$temp3;
		else $temp.='<br>';
	}
	$_POST[$profile[$x]]=trim($temp);
	
}
if ($_POST['phone']=='--') $_POST['phone']=null;
$_POST['dob']=mktime(0,0,0,$_POST['dobm'],$_POST['dobd'],$_POST['doby']);
//print_r($_POST); echo '<br>';

$req=explode(' ','username password confpassword fname lname email phone');
$req_exp=explode(',','username,password,password confirmation,first name,last name,Email,phone number');
for ($x=0; $x<sizeOf($req); $x++){
	if (!$_POST[$req[$x]])
		die('<span class="error">Error: You did not fill in your '.$req_exp[$x].', which is required information.  <br><a href="javascript:history.back(1)">Please go back and fix this</a></span>');
}
if (strtolower(htmlentities(trim($_POST['password'])))!=strtolower(htmlentities(trim($_POST['confpassword']))))
	die('<span class="error">Error: Your password and password confirmation do not match.  <br><a href="javascript:history.back(1)">Please go back and fix this</a></span>');
if (strlen($_POST['phone'])!=12) 
	die('<span class="error">Error: You did not enter a complete phone number.  <br><a href="javascript:history.back(1)">Please go back and fix this</a></span>');

$_POST['password']=encrypt($_POST['password']);

mysql_connect('localhost','circlek1_cki','cki') or die('<span class="error">Error: Unable to connect to server</span>');
mysql_select_db('circlek1_members') or die('<span class="error">Error: Unable to connect to database</span>');
$query='SELECT * FROM `memberlist` WHERE `username`=\''.$_POST['username'].'\'';
//echo $query.'<br>';
$data=mysql_query($query);
if (mysql_num_rows($data)>0)
	die('<span class="error">Unfortunately, that username is already taken.  <br><a href="javascript:history.back(1)">Please go back and choose a different one</a></span>');

$result=0;

$query='INSERT INTO `memberlist` (';
for ($x=0; $x<sizeOf($mlist); $x++){
	$query.='`'.$mlist[$x].'`';
	if ($x<sizeOf($mlist)-1) $query.=',';
}
$query.=') VALUES(';
for ($x=0; $x<sizeOf($mlist); $x++){
	$query.='\''.$_POST[$mlist[$x]].'\'';
	if ($x<sizeOf($mlist)-1) $query.=',';
}
$query.=')';
//echo $query.'<br>';
mysql_query($query);
$result+=mysql_affected_rows();

$query='INSERT INTO `profiles` (';
for ($x=0; $x<sizeOf($profile); $x++){
	$query.='`'.$profile[$x].'`';
	if ($x<sizeOf($profile)-1) $query.=',';
}
$query.=') VALUES(';
for ($x=0; $x<sizeOf($profile); $x++){
	$query.='\''.$_POST[$profile[$x]].'\'';
	if ($x<sizeOf($profile)-1) $query.=',';
}
$query.=')';
//echo $query.'<br>';
mysql_query($query);
$result+=mysql_affected_rows();

$query='INSERT INTO `options` (';
for ($x=0; $x<sizeOf($options); $x++){
	$query.='`'.$options[$x].'`';
	if ($x<sizeOf($options)-1) $query.=',';
}
$query.=') VALUES(';
for ($x=0; $x<sizeOf($options); $x++){
	$query.='\''.$_POST[$options[$x]].'\'';
	if ($x<sizeOf($options)-1) $query.=',';
}
$query.=')';
//echo $query.'<br>';
mysql_query($query);
$result+=mysql_affected_rows();

$query='INSERT INTO `autosignup` (';
for ($x=0; $x<sizeOf($autosignup); $x++){
	$query.='`'.$autosignup[$x].'`';
	if ($x<sizeOf($autosignup)-1) $query.=',';
}
$query.=') VALUES(';
for ($x=0; $x<sizeOf($autosignup); $x++){
	$query.='\''.$autosignup_defaults[$x].'\'';
	if ($x<sizeOf($autosignup)-1) $query.=',';
}
$query.=')';
//echo $query.'<br>';
mysql_query($query);
$result+=mysql_affected_rows();

//echo $result.' rows affected<br>';
if ($result==4){
	$msg='Your registration proceeded smoothly.  You are now free to log in!';
}
else{
	$msg='Your registration did not proceed smoothly.  But try logging in anyway.  If it does not work, please contact us.';
}

echo '<p>'.$msg.'</p>';
?>
</body>
</html>