<html>
<head>
<title>Families</title>
<link rel="stylesheet" type="text/css" href="ucsdcki.css">
</head>
<body>
<p class="Center"><img src="./images/families.png" border="0" alt="Families"></p>
<p><table class="matt" width="100%" border="0">
<tr><td width="25%" valign="top" align="center">
<div class="subtitle">Alan and Eileen's Family</div>
<!-- Family 1 //-->
Alan  Nam*<br>
Angela Cheung<br>
Bernardo  Arevalo<br>
Brenda  Tran<br>
David  Seto <br>
Eileen  Zhang*<br>
Elaine  Lugo<br>
Lena  Trinh<br>
Mary  Nasief<br>
Patricia Bae<br>
Sarah  Cook<br>
Shelley Yijy  Phoun<br>
Zenas  Hau<br>
</td>
<td width="25%" valign="top" align="center">
<div class="subtitle">Angela and Stefanie's Family</div>
<!-- Family 2 //-->
Andrea  Qualm<br>
Angela  Yen*<br>
Beiqun Zhao<br>
Brian  Wong<br>
Briana  Ly<br>
David  Tsang <br>
Hira  Alam<br>
Jeremiah  Boguiren<br>
Lisa  Cuneo<br>
Nicholas Ramsell<br>
Sheila  Sepehri<br>
Stefanie  Yan*<br>
Vicki  Lai<br>
</td>
<td width="25%" valign="top" align="center">
<div class="subtitle">The Epic Penguins</div>
<!-- Family 3 //-->
Alan   Lam<br>
Daphne  Wong*<br>
Edith  Urieta<br>
Elizabeth  Aong<br>
Eugenia  Leong<br>
Helen  Ly<br>
Jane  Fang<br>
Lauren Bettencourt<br>
Matthew  Louis*<br>
Power  Dang<br>
Stephanie  Wong <br>
Teresa  Chu<br>
Vaibhav Konanur<br>
</td>
<td width="25%" valign="top" align="center">
<div class="subtitle">HP</div>
<!-- Family 4 //-->
Alisha  Johnson<br>
Carolina  Moreno<br>
David  Wong<br>
Honyin  Chiu*<br>
Katherine  DeLoach<br>
Lilly  Zhang <br>
Michael  Greenberg<br>
Peter  Lai*<br>
Shannon Nies<br>
Steven Van<br>
Theresa  Nguyen<br>
Vidhi  Doshi<br>
Yuumi  Miyazawa <br>
</td>
</tr>
</table></p><p>
<table class="matt" width="100%" border="0">
<tr>
<td width="33.3333%" valign="top" align="center">
<div class="subtitle">Sexy Pocky Beasts</div>
<!-- Family 5 //-->
Amanda  Ramond*<br>
Angela  Phan<br>
Christina  Deedas<br>
Dante !*<br>
Dave  Ortiz-Suslow<br>
Jared  Ng <br>
Jennifer  Cao<br>
Jennifer  Kiang <br>
Julie  Chao<br>
Mandy  Ng<br>
Mariam  Asrdzhyan<br>
Sanghyun  Han<br>
Shivani  Patel<br>
Wendy  Chao<br>
</td>
<td width="33.3333%" valign="top" align="center">
<div class="subtitle">Family Left <br>(out of New Member Install)</div>
<!-- Family 6 //-->
Alex  Goetz<br>
Allyson  Chee<br>
Hannah  Walsh<br>
Jeffrey  Jou<br>
Jeffrey  Sung*<br>
Kennie  Park<br>
Kristine  Rodda<br>
Leslie  Lu*<br>
Lisa Fan<br>
Lori Yonemura<br>
Maria   Reyes<br>
Maxine Inocencio<br>
Sara Luu<br>
</td>
<td width="33.3333%" valign="top" align="center">
<div class="subtitle">HDizzle</div>
<!-- Family 7 //-->
Amy Bougoukalos<br>
Brittany  Singleton<br>
Cami  Tam<br>
Drew  Vo*<br>
Honeylyn  Suyat*<br>
Irene  Hwang<br>
Kendrick  Ton <br>
Kimberly  Turk<br>
Raquel  Fierro<br>
Ryan  Taylor<br>
Sean Chou<br>
Yun-wei  Chang<br>
</td>
</tr>
</table></p>
<p class="center">* Family head</p>
<p class="Center">Last Updated: January 31, 2008</p>
</body>
</html>
